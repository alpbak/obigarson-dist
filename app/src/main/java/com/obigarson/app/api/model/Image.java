package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {

    public Integer id;
    public String venueImage;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.venueImage);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.venueImage = in.readString();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}