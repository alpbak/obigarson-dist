package com.obigarson.app.api;


import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Locale;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class Serve {

    //public static final String API_BASE_URL = "https://api.obigarson.com/api/";
    //public static final String API_BASE_URL = "http://api.obigarson.com:8000/api/";
    public static final String API_BASE_URL = "https://api.obigarson.com/api/";

    private static OkHttpClient httpClient = new OkHttpClient();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);
    }

    public static <S> S createService(Class<S> serviceClass, final String authToken) {
        final String devLang = Locale.getDefault().getLanguage();

        Log.d("LANGUAGE: ", devLang);


        if (authToken != null) {
            Log.d("authToken: ", authToken);
            httpClient.interceptors().clear();
            httpClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", "Token "+authToken)
                            //.addHeader("HTTP_X_LANGUAGE", devLang)
                            //.header("HTTP_X_LANGUAGE", devLang)
                            .header("LANG", devLang)
                            .addHeader("LANG", devLang)
                            .method(original.method(), original.body());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
