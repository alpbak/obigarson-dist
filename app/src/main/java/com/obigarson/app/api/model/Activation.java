package com.obigarson.app.api.model;

import com.google.gson.Gson;


public class Activation {
    public String code;
    public Credentials user;

    public Activation(Credentials user, String code) {
        this.user = user;
        this.code = code;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
