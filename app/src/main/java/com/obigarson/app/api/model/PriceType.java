package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PriceType implements Parcelable {

    public Integer id;
    public String paymentType;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.paymentType);
    }

    public PriceType() {
    }

    @Override
    public String toString() {
        return paymentType;
    }

    protected PriceType(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.paymentType = in.readString();
    }

    public static final Parcelable.Creator<PriceType> CREATOR = new Parcelable.Creator<PriceType>() {
        public PriceType createFromParcel(Parcel source) {
            return new PriceType(source);
        }

        public PriceType[] newArray(int size) {
            return new PriceType[size];
        }
    };
}
