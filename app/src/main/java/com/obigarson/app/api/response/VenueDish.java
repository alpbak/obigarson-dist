package com.obigarson.app.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.obigarson.app.api.model.OrderedDish;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 21/12/15.
 */
public class VenueDish implements Parcelable {
    public boolean     alcohol_inc;
    public String  categoryName;
    public List<OrderedDish> dishes;
    public int id;

    public VenueDish() {
    }

    public VenueDish(int id, boolean alcohol_inc, String categoryName, List<OrderedDish> dishes) {
        this.id = id;
        this.alcohol_inc = alcohol_inc;
        this.categoryName = categoryName;
        this.dishes = dishes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(alcohol_inc ? (byte) 1 : (byte) 0);
        dest.writeString(this.categoryName);
        dest.writeList(this.dishes);
        dest.writeInt(this.id);
    }

    protected VenueDish(Parcel in) {
        this.alcohol_inc = in.readByte() != 0;
        this.categoryName = in.readString();
        this.dishes = new ArrayList<OrderedDish>();
        in.readList(this.dishes, List.class.getClassLoader());
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<VenueDish> CREATOR = new Parcelable.Creator<VenueDish>() {
        public VenueDish createFromParcel(Parcel source) {
            return new VenueDish(source);
        }

        public VenueDish[] newArray(int size) {
            return new VenueDish[size];
        }
    };

    @Override
    public String toString() {
        return "VenueDish{" +
                "id=" + id +
                ", alcohol_inc=" + alcohol_inc +
                ", categoryName='" + categoryName + '\'' +
                ", dishes=" + dishes +
                '}';
    }
}
