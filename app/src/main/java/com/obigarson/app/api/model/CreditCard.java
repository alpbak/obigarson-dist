package com.obigarson.app.api.model;

/**
 * Created by mstf on 17/01/16.
 */
public class CreditCard {
    public String card_number;
    public String month;
    public String year;
    public String holdername;
    public String description;
    public String status;
    public String ccv;

    @Override
    public String toString() {
        return "CreditCard{" +
                "cardNumber='" + card_number + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", holdername='" + holdername + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", ccv='" + ccv + '\'' +
                '}';
    }
}
