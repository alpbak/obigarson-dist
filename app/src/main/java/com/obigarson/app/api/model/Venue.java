package com.obigarson.app.api.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Venue implements Parcelable {

    public Integer id;
    public String venueName;
    public Integer venueCode;
    public List<Props> venueProperty = new ArrayList<>();
    public Props venueCategory;
    public String venueAddress;
    public String openTime;
    public String closeTime;
    public String venueGeoposition;
    public String venueImage;
    public Integer tableQuantity;
    public PriceType priceType;
    public String phone_number;
    public String description;
    public Boolean venueStatus;
    public List<PriceType> packagePayment = new ArrayList<>();
    public List<PriceType> venuePayment = new ArrayList<>();
    public List<PriceType> fastfoodPayment = new ArrayList<>();
    public Props venue_County;
    public Props venueTown;
    public Props venueneighborhood;
    public Props venuedistrict;
    public String takeaway_start_time;
    public String takeaway_end_time;
    public boolean reservation_status;
    public boolean fastfoodStatus;
    public boolean getgoing_status;
    public boolean takeaway_status;
    public List<Image> images = new ArrayList<>();
    //public List<String> social_accounts = new ArrayList<>();
    public Boolean favorites;
    public Double distance;



    public Venue() {
    }

    @Override
    public String toString() {
        return "Venue{" +
                "distance=" + distance +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.venueName);
        dest.writeValue(this.venueCode);
        dest.writeTypedList(venueProperty);
        dest.writeParcelable(this.venueCategory, 0);
        dest.writeString(this.venueAddress);
        dest.writeString(this.openTime);
        dest.writeString(this.closeTime);
        dest.writeString(this.venueGeoposition);
        dest.writeString(this.venueImage);
        dest.writeValue(this.tableQuantity);
        dest.writeParcelable(this.priceType, 0);
        dest.writeString(this.phone_number);
        dest.writeString(this.description);
        dest.writeValue(this.venueStatus);
        dest.writeTypedList(packagePayment);
        dest.writeTypedList(venuePayment);
        dest.writeTypedList(fastfoodPayment);
        dest.writeParcelable(this.venue_County, 0);
        dest.writeParcelable(this.venueTown, 0);
        dest.writeParcelable(this.venueneighborhood, 0);
        dest.writeParcelable(this.venuedistrict, 0);
        dest.writeString(this.takeaway_start_time);
        dest.writeString(this.takeaway_end_time);
        dest.writeByte(reservation_status ? (byte) 1 : (byte) 0);
        dest.writeByte(fastfoodStatus ? (byte) 1 : (byte) 0);
        dest.writeByte(getgoing_status ? (byte) 1 : (byte) 0);
        dest.writeByte(takeaway_status ? (byte) 1 : (byte) 0);
        dest.writeTypedList(images);
        //dest.writeStringList(this.social_accounts);
        dest.writeValue(this.favorites);
        dest.writeValue(this.distance);
    }

    protected Venue(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.venueName = in.readString();
        this.venueCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.venueProperty = in.createTypedArrayList(Props.CREATOR);
        this.venueCategory = in.readParcelable(Props.class.getClassLoader());
        this.venueAddress = in.readString();
        this.openTime = in.readString();
        this.closeTime = in.readString();
        this.venueGeoposition = in.readString();
        this.venueImage = in.readString();
        this.tableQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.priceType = in.readParcelable(PriceType.class.getClassLoader());
        this.phone_number = in.readString();
        this.description = in.readString();
        this.venueStatus = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.packagePayment = in.createTypedArrayList(PriceType.CREATOR);
        this.venuePayment = in.createTypedArrayList(PriceType.CREATOR);
        this.fastfoodPayment = in.createTypedArrayList(PriceType.CREATOR);
        this.venue_County = in.readParcelable(Props.class.getClassLoader());
        this.venueTown = in.readParcelable(Props.class.getClassLoader());
        this.venueneighborhood = in.readParcelable(Props.class.getClassLoader());
        this.venuedistrict = in.readParcelable(Props.class.getClassLoader());
        this.takeaway_start_time = in.readString();
        this.takeaway_end_time = in.readString();
        this.reservation_status = in.readByte() != 0;
        this.fastfoodStatus = in.readByte() != 0;
        this.getgoing_status = in.readByte() != 0;
        this.takeaway_status = in.readByte() != 0;
        this.images = in.createTypedArrayList(Image.CREATOR);
        //this.social_accounts = in.createStringArrayList();
        this.favorites = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.distance = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<Venue> CREATOR = new Creator<Venue>() {
        public Venue createFromParcel(Parcel source) {
            return new Venue(source);
        }

        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };
}