package com.obigarson.app.api.model;

import java.util.Comparator;

/**
 * Created by mstf on 23/12/15.
 */
public class OrderSort implements Comparator<Order> {

        @Override
        public int compare(Order lhs, Order rhs) {
        return lhs.id - rhs.id;
        }
}