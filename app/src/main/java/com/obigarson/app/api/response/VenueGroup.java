package com.obigarson.app.api.response;

import android.util.Log;

import com.obigarson.app.api.model.Venue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VenueGroup {
    public List<VenueCat> venues = new ArrayList<>();

    public void  setCats(List<Venue> list){

        Map<String, ArrayList<Venue>> cat = new HashMap<>();
        for (Venue venue:list ) {

            if(!cat.containsKey(venue.venueCategory.getName())){
                cat.put(venue.venueCategory.getName(),new ArrayList<Venue>());
            }
            cat.get(venue.venueCategory.getName()).add(venue);
        }
        for (String key : cat.keySet()){
            VenueCat temp = new VenueCat();
            temp.name = key;
            temp.venues = cat.get(key);
            venues.add(temp);
        }
    }
}
