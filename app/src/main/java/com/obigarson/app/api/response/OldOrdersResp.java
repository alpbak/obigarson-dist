package com.obigarson.app.api.response;

import com.obigarson.app.api.model.OldOrders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abak on 19/03/16.
 */

public class OldOrdersResp {
    public List<OldOrders> orders = new ArrayList<>();

    @Override
    public String toString() {
        return "ReservationResp{" +
                "favorites=" + orders +
                '}';
    }
}
