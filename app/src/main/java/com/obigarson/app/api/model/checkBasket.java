package com.obigarson.app.api.model;

import java.util.List;

/**
 * Created by alpaslanbak on 18.06.2016.
 */
public class checkBasket {

    //public List<BasketDetail> basketDetail;
    public List<checkBasketOrder> orders;

    public boolean isActive(){
        for (checkBasketOrder order:orders) {
            if(order.Status.equals("Sipariş Edildi")){
                return true;
            }
        }
        return  false;
    }
}
