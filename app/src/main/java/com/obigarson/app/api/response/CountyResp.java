package com.obigarson.app.api.response;


import com.obigarson.app.api.model.Props;

import java.util.ArrayList;
import java.util.List;

public class CountyResp {
    public List<Props> counties = new ArrayList<>();

    public CountyResp(List<Props> counties) {
        this.counties = counties;
    }

    @Override
    public String toString() {
        return "CountyResp{" +
                "counties=" + counties +
                '}';
    }
}
