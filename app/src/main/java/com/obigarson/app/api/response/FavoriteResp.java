package com.obigarson.app.api.response;


import com.obigarson.app.api.model.Favorite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class FavoriteResp {
    public List<Favorite> favorites = new ArrayList<>();
}
