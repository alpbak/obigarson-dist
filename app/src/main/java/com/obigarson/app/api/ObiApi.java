package com.obigarson.app.api;


import com.obigarson.app.api.model.BKMMessage;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.ForgotPassword;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Profile;
import com.obigarson.app.api.model.checkBasket;
import com.obigarson.app.api.response.AddressResp;
import com.obigarson.app.api.response.CountyResp;
import com.obigarson.app.api.response.CreditCardResp;
import com.obigarson.app.api.response.DistrictResp;
import com.obigarson.app.api.response.FavoriteResp;
import com.obigarson.app.api.response.FbLoginResp;
import com.obigarson.app.api.response.ForgetPasswordResp;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.api.response.MenuResp;
import com.obigarson.app.api.response.NeighBourResp;
import com.obigarson.app.api.response.OldOrdersResp;
import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.api.response.QrResp;
import com.obigarson.app.api.response.ReservationResp;
import com.obigarson.app.api.response.TownResp;
import com.obigarson.app.api.response.VenueResp;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface ObiApi {

    @FormUrlEncoded
    @POST("send_sms/")
    Call<Message> sendSms(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("activate_account/")
    Call<LoginResp> activateAccount(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("users/forgot_password/")
    Call<Message> forgotPassword(@Field("signed_body") String body);


    @FormUrlEncoded
    @POST("login/")
    Call<LoginResp> login(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("venue/list/")
    Call<VenueResp> getVenues(@Field("signed_body") String body);


    @GET("users/favorites/venues/")
    Call<FavoriteResp> getFavs();

    @FormUrlEncoded
    @POST("users/favorites/venues/")
    Call<FavoriteResp> addFavorite(@Field("signed_body") String body);

    @FormUrlEncoded
    @DELETE("users/favorites/venues/")
    Call<VenueResp> delFavorite(@Field("signed_body") String body);


    @GET("users/profile/")
    Call<Profile> getUserProfile();

    @GET("users/favorites/dishes/")
    Call<VenueResp> getUserFavDishes();

    @FormUrlEncoded
    @POST("users/favorites/dishes/")
    Call<VenueResp> addUserFavDishes(@Field("signed_body") String body);

    @FormUrlEncoded
    @DELETE("users/favorites/dishes/")
    Call<VenueResp> delUserFavDishes(@Field("signed_body") String body);

    @GET("users/address/")
    Call<AddressResp> getUserAddresses();

    @FormUrlEncoded
    @POST("users/address/")
    Call<AddressResp> addUserAddress(@Field("signed_body") String body);


    @DELETE("users/address/")
    Call<AddressResp> delUserAddress(@Query( value = "signed_body", encoded = true) String body);

    @GET("counties/")
    Call<CountyResp> getCounties();

    @GET("towns/")
    Call<TownResp> getTowns(@Query("id") int id);

    @GET("districts/")
    Call<DistrictResp> getDistricts(@Query("id") int id);

    @GET("neighborhoods/")
    Call<NeighBourResp> getNeighbours(@Query("id") int id);

    @GET("users/reservations/")
    Call<ReservationResp> getUserReserves();

    //@GET("users/reservations/")
    @GET("users/package/orders/")
    Call<OldOrdersResp> getUserOldOrders();

    @FormUrlEncoded
    @POST("users/reservations/")
    Call<ReservationResp> addUserReserve(@Field("venue") int venueCode,
                                         @Field("reservation_date") String reservation_date,
                                         @Field("telephone") String telephone,
                                         @Field("count") int count);

    @FormUrlEncoded
    @POST("users/reservations/cancel/")
    Call<ReservationResp> cancelUserReserve(@Field("reservation") int reserveId);


    @FormUrlEncoded
    @POST("venue/callwaiters/")
    Call<Message> callWaiter(@Field("table") long tableCode);

    @FormUrlEncoded
    @POST("fbconnect/")
    Call<FbLoginResp> fbConnect(@Field("signed_body") String body);


    @FormUrlEncoded
    @POST("users/creditcard/add/")
    Call<CreditCardResp> addCreditCard(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("users/creditcard/add/")
    Call<Message> updateOnSignalId(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("users/creditcard/")
    Call<CreditCardResp> getCreditCards(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("users/creditcard/delete/")
    Call<CreditCardResp> delCreditCard(@Field("signed_body") String body);



    //Orders
    @FormUrlEncoded
    @POST("venue/orders/")
    Call<Basket> addOrder(@Field("venue") long venueCode,
                              @Field("table") long tableCode,
                              @Field("dishes") int dishId,
                              @Field("quantity") int quantity);

    @FormUrlEncoded
    @POST("venue/apart/orders/")
    Call<Basket> addApartOrder(@Field("venue") long venueCode,
                                @Field("room") long roomCode,
                                @Field("dishes") int dishId,
                                @Field("quantity") int quantity);

    @FormUrlEncoded
    @POST("venue/fastfood/orders/")
    Call<Basket> addFastFoodOrder(@Field("venue") long venueCode,
                               @Field("dishes") int dishId,
                               @Field("quantity") int quantity);

    @GET("venue/apart/orders/basket/")
    Call<PackageResp> getApartBasket(@Query("venue") int venueCode,@Query("room") long roomCode);

    @FormUrlEncoded
    @POST("venue/apart/orders/payment/")
    Call<Message> orderApartPayment(@Field("venue") long venueCode,
                               @Field("room") long roomCode,
                               @Field("payment_type") int paymentType);

    //OnlineCreditCard
    @FormUrlEncoded
    @POST("venue/apart/orders/payment/")
    Call<Message> orderApartPaymentOnlineCard(@Field("venue") long venueCode,
                                              @Field("room") long roomCode,
                                              @Field("payment_type") int paymentType,
                                              @Field("creditcard") String creditcard
                                              );

    @FormUrlEncoded
    @POST("venue/orders/payment/")
    Call<Message> orderPaymentOnlineCard(@Field("venue") long venueCode,
                                         @Field("table") long tableCode,
                                         @Field("payment_type") int paymentType,
                                         @Field("creditcard") String creditcard
                                          );

    @FormUrlEncoded
    @POST("venue/fastfood/orders/payment/")
    Call<Message> fastFoodPaymentOnlineCard(@Field("venue") long venueCode,
                                         @Field("payment_type") int paymentType,
                                         @Field("creditcard") String creditcard
    );



    @FormUrlEncoded
    @POST("venue/package/orders/payment/")
    Call<Message> orderPackagePaymentOnlineCard(@Field("venue") int venueCode,
                                                @Field("address") int addressId,
                                                @Field("payment_type") int paymentType,
                                                @Field("creditcard") String creditcard
                                                );
    @FormUrlEncoded
    @POST("venue/package/orders/payment/")
    Call<Message> orderPackagePayment(@Field("venue") int venueCode,
                                      @Field("address") int addressId,
                                      @Field("payment_type") int paymentType);

    @FormUrlEncoded
    @POST("venue/orders/payment/")
    Call<Message> orderPayment(@Field("venue") long venueCode,
                               @Field("table") long tableCode,
                               @Field("payment_type") int paymentType);

    @FormUrlEncoded
    @POST("venue/orders/bkmexpress_payment/")
    Call<BKMMessage> BKMExpress(@Field("venue") long venueCode,
                                @Field("table") long tableCode,
                                @Field("payment_type") int paymentType);

    @FormUrlEncoded
    @POST("venue/fastfood/bkmexpress_payment/")
    Call<BKMMessage> BKMExpressFastFood(@Field("venue") long venueCode,
                                @Field("table") long tableCode,
                                @Field("payment_type") int paymentType);

    @FormUrlEncoded
    @POST("venue/apart/bkmexpress_payment/")
    Call<BKMMessage> BKMExpressApart(@Field("venue") long venueCode,
                                        @Field("room") long tableCode,
                                        @Field("payment_type") int paymentType);

    @FormUrlEncoded
    @POST("venue/dishes/")
    Call<MenuResp> getMenu(@Field("signed_body") String body);

    @FormUrlEncoded
    @POST("venue/orders/send/")
    Call<Message> sendOrder(@Field("venue") long venueCode,
                            @Field("table") long tableCode,
                            @Field("basket") int basketId);

    @FormUrlEncoded
    @POST("venue/orders/quantity/")
    Call<Message> updateOrder(@Field("venue") long venueCode,
                              @Field("table") long tableCode,
                              @Field("order") int orderId,
                              @Field("param") String param);

    @FormUrlEncoded
    @POST("venue/apart/orders/quantity/")
    Call<Message> updateApartOrder(@Field("venue") long venueCode,
                              @Field("order") int orderId,
                              @Field("param") String param);

    @GET("venue/orders/basket/")
    Call<Basket> getBasket(@Query("venue") long venue,@Query("table") long tableId);

    @GET("users/latest/orders/")
    Call<checkBasket> checkBasketEmpty();


    @FormUrlEncoded
    @POST("venue/orders/note/")
    Call<Message> orderNote(@Field("venue") long venueCode,
                               @Field("table") long tableCode,
                               @Field("note") String note);

    @FormUrlEncoded
    @POST("venue/package/orders/note/")
    Call<Message> packageNote(@Field("venue") long venueCode,
                              @Field("address") long address,
                              @Field("note") String note);

    @FormUrlEncoded
    @POST("venue/orders/tips/")
    Call<Message> orderTip(@Field("venue") long venueCode,
                           @Field("table") long tableCode,
                           @Field("price") double tipValue);

    //TakeAway Orders
    @FormUrlEncoded
    @POST("venue/packageservices/")
    Call<QrResp> checkPackage(@Field("venue") int venueCode,
                               @Field("address") int address);



    @FormUrlEncoded
    @POST("venue/package/orders/")
    Call<PackageResp> addPackageOrder(@Field("venue") int venueCode,
                                     @Field("address") int addressId,
                                     @Field("dishes") int dishId,
                                     @Field("quantity") int quantity);

    @FormUrlEncoded
    @POST("venue/package/orders/send/")
    Call<Message> sendPackageOrder(@Field("venue") int venueCode,
                                   @Field("address") int addressId,
                                   @Field("basket") int basketId);

    @FormUrlEncoded
    @POST("venue/package/orders/quantity/")
    Call<Message> updatePackageOrder(@Field("venue") long venueCode,
                                     @Field("address") int addressId,
                                     @Field("order") int orderId,
                                     @Field("param") String param);

    @FormUrlEncoded
    @POST("venue/fastfood/orders/quantity/")
    Call<Message> updateFastFoodOrder(@Field("venue") long venueCode,
                                     @Field("order") int orderId,
                                     @Field("param") String param);

    @GET("venue/package/orders/basket/")
    Call<PackageResp> getPackageBasket(@Query("venue") int venueCode,@Query("address") int addressId);

    @GET("venue/fastfood/orders/basket/")
    Call<PackageResp> getFastFoodBasket(@Query("venue") int venueCode);

    @GET("users/package/orders/")
    Call<Basket> getPackageHist();

    @GET("users/orders/")
    Call<Basket> getOrderHist();

    @GET("users/package/orders/detail/")
    Call<Basket> getPackageDetail(@Query("id") int orderId);

    @GET("users/orders/detail/")
    Call<Basket> getOrderDetail(@Query("id") int orderId);

    @GET("qr_code/")
    Call<QrResp> getQrCode(@Query("venue") Long venueCode,@Query("table") Long tableId);

}
