package com.obigarson.app.api.model;


public class Message {
    public String status;
    public String message;

    public Message() {
    }

    public Message(String message, String status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
