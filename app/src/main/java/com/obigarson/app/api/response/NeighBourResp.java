package com.obigarson.app.api.response;

import com.obigarson.app.api.model.Props;

import java.util.ArrayList;
import java.util.List;

public class NeighBourResp {
    public List<Props> neighborhoods = new ArrayList<>();
}
