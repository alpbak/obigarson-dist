package com.obigarson.app.api.model;

import com.google.gson.Gson;


public class Loc {
    public double longitude;
    public double latitude;

    public Loc() {
    }

    public Loc(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getJson(){
        return new Gson().toJson(this).toString();
    }
}
