package com.obigarson.app.api.response;

import com.google.gson.Gson;

/**
 * Created by abak on 23/03/16.
 */
public class ForgetPasswordResp {
    public String status;
    public String message;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String string() {
        return "ForgetPasswordResp{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
