package com.obigarson.app.api;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.obigarson.app.api.model.Credentials;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.util.ObiApp;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import retrofit.Call;
import retrofit.Response;


public  class Utils {

    private static final String SIGNED_BODY = "%s.%s";
    private static final String key = "u7n-%$j!o3rwjd46+dhji7r3+=vjzm-$)&x^rdl)=tnou51a1&";
    public static String searchText;



    public  static String encode(String data) throws Exception {
        final Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        final SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        final String resp = new String(Hex.encodeHex(sha256_HMAC.doFinal(data.getBytes("UTF-8"))));
        final String add  = String.format(SIGNED_BODY, resp, data);
          return add;
    }
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static JSONObject putJson(JSONObject json, String var, String value) throws  Exception{
        return json.put(var,value);
    }
    public static JSONObject putJson(JSONObject json, HashMap<Object,Object> vars) throws  Exception{
        for (Object obj : vars.keySet()){
            json.put((String)obj,(String)vars.get(obj));
        }
        return json;
    }




    public void saveUser(Context context,Credentials credentials){
        SharedPreferences sh_Pref = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh_Pref.edit();
        editor.putString("credentials", credentials.toString());
        editor.commit();
    }
    public static boolean reAuth(Context context){
        try{
            SharedPreferences sh_Pref = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            SharedPreferences sh_User = context.getSharedPreferences("USER", Context.MODE_PRIVATE);

            Call<LoginResp> res = ObiApp.api.login(sh_Pref.getString("credentials", ""));
            Response response = res.execute();
            if(response.isSuccess()){
                ObiApp.user = (LoginResp)response.body();
                saveUser(context);
            }else{
                sh_Pref.edit().clear().commit();
                sh_User.edit().clear().commit();
                return false;
            }
        }catch (Exception ex){
            return false;
        }
        return true;
    }

    public static String encodeURI(String value) throws Exception {
          return URLEncoder.encode(value,"UTF-8");
    }



    public static void saveUser(Context context){
        SharedPreferences sh_Pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh_Pref.edit();
        editor.putString("userResp", ObiApp.user.toString());
        editor.commit();
    }
    public static LoginResp getUser(Context context){
        SharedPreferences sh_Pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return   new Gson().fromJson(sh_Pref.getString("userResp",""),LoginResp.class);
    }


    public static String setSearchString(String gelen){
        searchText = gelen;
        return searchText;
    }
    public static String getSearchString(){
        return searchText;
    }
}
