package com.obigarson.app.api.response;


import android.util.Log;

import com.obigarson.app.api.model.Venue;

import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class VenueResp {
    public List<Venue> venues;

    @Override
    public String toString() {
        return "VenueResp{" +
                "venues=" + venues +
                '}';
    }
}
