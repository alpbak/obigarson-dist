package com.obigarson.app.api.model;

/**
 * Created by abak on 23/03/16.
 */
public class ForgotPassword {
    public String status;
    public String message;

    public ForgotPassword() {
    }

    public ForgotPassword(String message, String status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
