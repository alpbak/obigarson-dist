package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mstf on 03/12/15.
 */
public class DishCategory implements Parcelable {
    public String Name;
    public Boolean alcoholInc;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Name);
        dest.writeValue(this.alcoholInc);
    }

    public DishCategory() {
    }

    protected DishCategory(Parcel in) {
        this.Name = in.readString();
        this.alcoholInc = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<DishCategory> CREATOR = new Parcelable.Creator<DishCategory>() {
        public DishCategory createFromParcel(Parcel source) {
            return new DishCategory(source);
        }

        public DishCategory[] newArray(int size) {
            return new DishCategory[size];
        }
    };

    @Override
    public String toString() {
        return "DishCategory{" +
                "Name='" + Name + '\'' +
                ", alcoholInc=" + alcoholInc +
                '}';
    }
}
