package com.obigarson.app.api.response;


import com.obigarson.app.api.model.Reservation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class ReservationResp {
    public List<Reservation> reservations = new ArrayList<>();

    @Override
    public String toString() {
        return "ReservationResp{" +
                "favorites=" + reservations +
                '}';
    }
}
