package com.obigarson.app.api.model;

import java.util.ArrayList;
import java.util.List;

public class VenueDish {

    public Boolean alcoholInc;
    public String categoryName;
    public List<OrderedDish> dishes = new ArrayList<>();
    public Integer id;
}
