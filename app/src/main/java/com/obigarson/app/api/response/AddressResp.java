package com.obigarson.app.api.response;

import com.obigarson.app.api.model.Address;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class AddressResp {
    public List<Address> address = new ArrayList<>();

    @Override
    public String toString() {
        return "AddressResp{" +
                "address=" + address +
                '}';
    }
}
