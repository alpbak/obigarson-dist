package com.obigarson.app.api.model;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by abak on 19/03/16.
 */
public class OldOrders {
    public Integer id;
    public Venue venue;
    public String orderDate;
    public String CreateDate;
    public String UpdateDate;
    public String CancelDate;
    public String phoneNumber;
    public Integer reservationCount;
    public String note;
    public String Status;
    public List<Payment> payment = new ArrayList<>();

    /*
    public Reservation() {

    }

    public Reservation(Integer id, Venue venue, String reservationDate, String createDate, String updateDate, String cancelDate, String phoneNumber, Integer reservationCount, String note, String status) {
        this.id = id;
        this.venue = venue;
        this.reservationDate = reservationDate;
        CreateDate = createDate;
        UpdateDate = updateDate;
        CancelDate = cancelDate;
        this.phoneNumber = venue.phone_number;
        this.reservationCount = reservationCount;
        this.note = note;
        Status = status;
    }
*/
    @Override
    public String toString() {

       for(Payment str : payment) {
            System.out.println("*****************");
            System.out.println(str.payment_total);
        }

        return "Reservation{" +
                "id=" + id +
                ", venue=" + venue +
                ", reservationDate='" + orderDate + '\'' +
                ", payment='" + payment + '\'' +
                /*
                ", UpdateDate='" + UpdateDate + '\'' +
                ", CancelDate='" + CancelDate + '\'' +
                ", phoneNumber='" + venue.phone_number + '\'' +
                ", reservationCount=" + reservationCount +
                ", note='" + note + '\'' +
                */
                ", Status='" + Status + '\'' +

                '}';
    }
}

