package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mstf on 17/01/16.
 */
public class FbLogin implements Parcelable {
    public String telephone;
    public String birthDay;
    public String profile_image;
    public String genders;
    public int age;
    public String facebookID;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.telephone);
        dest.writeString(this.birthDay);
        dest.writeString(this.profile_image);
        dest.writeString(this.genders);
        dest.writeInt(this.age);
        dest.writeString(this.facebookID);
    }

    public FbLogin() {
    }

    protected FbLogin(Parcel in) {
        this.telephone = in.readString();
        this.birthDay = in.readString();
        this.profile_image = in.readString();
        this.genders = in.readString();
        this.age = in.readInt();
        this.facebookID = in.readString();
    }

    public static final Parcelable.Creator<FbLogin> CREATOR = new Parcelable.Creator<FbLogin>() {
        public FbLogin createFromParcel(Parcel source) {
            return new FbLogin(source);
        }

        public FbLogin[] newArray(int size) {
            return new FbLogin[size];
        }
    };
}
