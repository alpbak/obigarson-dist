package com.obigarson.app.api.response;

import com.obigarson.app.api.model.BasketDetail;
import com.obigarson.app.api.model.Order;

import java.util.List;

/**
 * Created by mstf on 22/12/15.
 */
public class PackageResp {
    public List<Order> orders;
    public List<BasketDetail> basketDetail;
    public double basketTotal;
}
