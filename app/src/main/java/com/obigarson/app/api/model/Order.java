package com.obigarson.app.api.model;

/**
 * Created by mstf on 03/12/15.
 */
public class Order  {
    public Integer id;
    public OrderedDish ordered_dishes;
    public int quantity;
    public Integer app_order_id;
    public Integer app_product_id;
    public String orderTags;
    public String unit_price;
    public String total_price;
    public String orderDate;
    public String Status;

    public Order() {
    }



    public Order(Integer id, OrderedDish ordered_dishes, int quantity, Integer app_order_id, Integer app_product_id, String orderTags, String unit_price, String total_price, String orderDate, String status) {
        this.id = id;
        this.ordered_dishes = ordered_dishes;
        this.quantity = quantity;
        this.app_order_id = app_order_id;
        this.app_product_id = app_product_id;
        this.orderTags = orderTags;
        this.unit_price = unit_price;
        this.total_price = total_price;
        this.orderDate = orderDate;
        Status = status;
    }

}
