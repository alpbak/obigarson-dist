package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mstf on 03/12/15.
 */
public class Credentials implements Parcelable {
    public String first_name;
    public String last_name;
    public String email;
    public String telephone;
    public String devicetoken;
    public String password;
    public String facebookid;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.email);
        dest.writeString(this.telephone);
        dest.writeString(this.devicetoken);
        dest.writeString(this.password);
        dest.writeString(this.facebookid);
    }

    public Credentials() {
    }

    protected Credentials(Parcel in) {
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.telephone = in.readString();
        this.devicetoken = in.readString();
        this.password = in.readString();
        this.facebookid = in.readString();
    }

    public static final Creator<Credentials> CREATOR = new Creator<Credentials>() {
        public Credentials createFromParcel(Parcel source) {
            return new Credentials(source);
        }

        public Credentials[] newArray(int size) {
            return new Credentials[size];
        }
    };
}
