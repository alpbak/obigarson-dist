package com.obigarson.app.api.response;


import com.obigarson.app.api.model.Props;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class TownResp {
    public List<Props> towns = new ArrayList<>();
}
