package com.obigarson.app.api.model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Address {

    public Integer id;
    public String address_title;
    public String address;
    public String telephone;
    public Props addressCounty;
    public Props addressCountyTown;
    public Props addressneighborhood;
    public Props addressdistrict;

    public String getJson(){
        JsonObject obj = new Gson().toJsonTree(this).getAsJsonObject();
        obj.addProperty("addressCounty",addressCounty.id);
        obj.addProperty("addressCountyTown",addressCountyTown.id);
        obj.addProperty("addressneighborhood",addressneighborhood.id);
        obj.addProperty("addressdistrict",addressdistrict.id);
        obj.addProperty("address",address);
        obj.addProperty("address_title",address_title);
        return obj.toString();
    }

    public Address() {
    }

    public Address(Integer id, String address_title, String address, String telephone, Props addressCounty, Props addressCountyTown, Props addressneighborhood, Props addressdistrict) {
        this.id = id;
        this.address_title = address_title;
        this.address = address;
        this.telephone = telephone;
        this.addressCounty = addressCounty;
        this.addressCountyTown = addressCountyTown;
        this.addressneighborhood = addressneighborhood;
        this.addressdistrict = addressdistrict;
    }

    @Override
    public String toString() {
        return address_title+ "-" + addressdistrict;
    }


}