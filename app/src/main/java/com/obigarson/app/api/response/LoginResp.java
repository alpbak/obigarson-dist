package com.obigarson.app.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.obigarson.app.api.model.Customer;

/**
 * Created by mstf on 03/12/15.
 */
public class LoginResp implements Parcelable {
    public String token;
    public Customer data;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

     public String string() {
        return "LoginResp{" +
                "data=" + data +
                ", token='" + token + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.token);
        dest.writeParcelable(this.data, flags);
    }

    public LoginResp() {
    }

    protected LoginResp(Parcel in) {
        this.token = in.readString();
        this.data = in.readParcelable(Customer.class.getClassLoader());
    }

    public static final Parcelable.Creator<LoginResp> CREATOR = new Parcelable.Creator<LoginResp>() {
        public LoginResp createFromParcel(Parcel source) {
            return new LoginResp(source);
        }

        public LoginResp[] newArray(int size) {
            return new LoginResp[size];
        }
    };
}
