package com.obigarson.app.api.model;

/**
 * Created by alpaslanbak on 4.05.2016.
 */
public class BKMMessage {
    public String html;

    public BKMMessage() {
    }

    public BKMMessage(String html) {
        this.html = html;
    }

    @Override
    public String toString() {
        return "Message{" +
                ", message='" + html + '\'' +
                '}';
    }
}
