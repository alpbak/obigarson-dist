package com.obigarson.app.api.model;

/**
 * Created by mstf on 03/12/15.
 */
public class BasketDetail {
    public Integer id;
    public Venue venue;
    public Customer customer;
    public Table table;
    public Integer appTableId;
    public Integer appTicketId;
    public String orderDate;
    public String Status;
}
