package com.obigarson.app.api.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Props implements Parcelable {


    public Integer id;

    public String Name;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The Name
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name
     * The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return Name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.Name);
    }

    public Props() {
    }

    public Props(Integer id, String name) {
        this.id = id;
        Name = name;
    }

    protected Props(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Name = in.readString();
    }

    public static final Parcelable.Creator<Props> CREATOR = new Parcelable.Creator<Props>() {
        public Props createFromParcel(Parcel source) {
            return new Props(source);
        }

        public Props[] newArray(int size) {
            return new Props[size];
        }
    };
}
