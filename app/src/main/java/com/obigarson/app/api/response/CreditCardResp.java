package com.obigarson.app.api.response;

import com.obigarson.app.api.model.CreditServer;

import java.util.List;

/**
 * Created by mstf on 17/01/16.
 */
public class CreditCardResp {
    public List<CreditServer> creditcards;

    @Override
    public String toString() {
        return "CreditCardResp{" +
                "creditcards=" + creditcards +
                '}';
    }
}
