package com.obigarson.app.api.response;

import com.obigarson.app.api.model.Venue;

import java.util.List;

/**
 * Created by mstf on 03/12/15.
 */
public class QrResp {
    public List<Venue> venue;
    public List<VenueDish> venueDishes;
    public boolean apartStatus;


    @Override
    public String toString() {
        return "QrResp{" +
                "venue=" + venue +
                "apart=" + apartStatus +
                ", venueDishes=" + venueDishes +
                '}';
    }
}
