package com.obigarson.app.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.obigarson.app.api.model.Venue;

import java.util.List;
import java.util.Objects;

/**
 * Created by mstf on 07/12/15.
 */
public class VenueCat implements Parcelable {
    public List<Venue> venues;
    public String name;

    public VenueCat() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueCat venueCat = (VenueCat) o;
        return Objects.equals(name, venueCat.name);
    }

    public VenueCat(String name, List<Venue> venues) {
        this.name = name;
        this.venues = venues;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(venues);
        dest.writeString(this.name);
    }

    protected VenueCat(Parcel in) {
        this.venues = in.createTypedArrayList(Venue.CREATOR);
        this.name = in.readString();
    }

    public static final Creator<VenueCat> CREATOR = new Creator<VenueCat>() {
        public VenueCat createFromParcel(Parcel source) {
            return new VenueCat(source);
        }

        public VenueCat[] newArray(int size) {
            return new VenueCat[size];
        }
    };
}
