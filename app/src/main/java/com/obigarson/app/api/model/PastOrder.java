package com.obigarson.app.api.model;

/**
 * Created by mstf on 27/01/16.
 */
public class PastOrder {
    public Integer id;
    public Venue venue;
    public Customer customer;
    public Table table;
    public Integer appTableId;
    public Integer appTicketId;
    public String orderDate;
    public String Status;


}
