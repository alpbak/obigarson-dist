package com.obigarson.app.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.obigarson.app.api.model.Customer;
import com.obigarson.app.api.model.FbLogin;

/**
 * Created by mstf on 03/12/15.
 */
public class FbLoginResp implements Parcelable {
    public String token;
    public Customer user_data;
    public FbLogin profil;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public FbLoginResp(FbLogin profil, String token, Customer user_data) {
        this.profil = profil;
        this.token = token;
        this.user_data = user_data;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.token);
        dest.writeParcelable(this.user_data, 0);
        dest.writeParcelable(this.profil, flags);
    }

    protected FbLoginResp(Parcel in) {
        this.token = in.readString();
        this.user_data = in.readParcelable(Customer.class.getClassLoader());
        this.profil = in.readParcelable(FbLogin.class.getClassLoader());
    }

    public static final Creator<FbLoginResp> CREATOR = new Creator<FbLoginResp>() {
        public FbLoginResp createFromParcel(Parcel source) {
            return new FbLoginResp(source);
        }

        public FbLoginResp[] newArray(int size) {
            return new FbLoginResp[size];
        }
    };

    public LoginResp getUser(){
        LoginResp resp = new LoginResp();
        resp.token = this.token;
        resp.data = this.user_data;
        return resp;
    }
}
