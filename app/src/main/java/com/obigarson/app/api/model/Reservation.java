package com.obigarson.app.api.model;

/**
 * Created by mstf on 03/12/15.
 */
public class Reservation {


    public Integer id;
    public Venue venue;
    public String reservationDate;
    public String CreateDate;
    public String UpdateDate;
    public String CancelDate;
    public String phoneNumber;
    public Integer reservationCount;
    public String note;
    public String Status;

    /*
    public Reservation() {

    }

    public Reservation(Integer id, Venue venue, String reservationDate, String createDate, String updateDate, String cancelDate, String phoneNumber, Integer reservationCount, String note, String status) {
        this.id = id;
        this.venue = venue;
        this.reservationDate = reservationDate;
        CreateDate = createDate;
        UpdateDate = updateDate;
        CancelDate = cancelDate;
        this.phoneNumber = venue.phone_number;
        this.reservationCount = reservationCount;
        this.note = note;
        Status = status;
    }
*/
    @Override
    public String toString() {

       return "Reservation{" +
                "id=" + id +
                ", venue=" + venue +
                ", reservationDate='" + reservationDate + '\'' +
                ", CreateDate='" + CreateDate + '\'' +
                ", UpdateDate='" + UpdateDate + '\'' +
                ", CancelDate='" + CancelDate + '\'' +
                ", phoneNumber='" + venue.phone_number + '\'' +
                ", reservationCount=" + reservationCount +
                ", note='" + note + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}
