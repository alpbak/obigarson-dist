package com.obigarson.app.api.model;

/**
 * Created by mstf on 17/01/16.
 */
public class CreditServer {
    public Integer id;
    public String customerLocale;
    public String customerKey;
    public String cardToken;
    public String cardNumber;
    public Integer cardExMonth;
    public Integer cardExYear;
    public String cardHolderName;
    public String cardAlias;
    public Boolean Status;

    @Override
    public String toString() {
        return "CreditServer{" +
                "cardToken='" + cardToken + '\'' +
                ", id=" + id +
                ", customerLocale='" + customerLocale + '\'' +
                ", customerKey='" + customerKey + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", cardExMonth=" + cardExMonth +
                ", cardExYear=" + cardExYear +
                ", cardHolderName='" + cardHolderName + '\'' +
                ", cardAlias='" + cardAlias + '\'' +
                ", Status=" + Status +
                '}';
    }
}
