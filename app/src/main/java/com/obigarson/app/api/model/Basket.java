package com.obigarson.app.api.model;

import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.util.ObiApp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


public class Basket {
    public double totalTips=0;
    public double basketTotal = 0;
    public List<BasketDetail> basketDetail;
    public List<Order> orders;



    public boolean isActive(){
        for (Order order:orders) {
            if(order.Status.equals("Sipariş Edildi")){
                return true;
            }
        }
        return  false;
    }

    public boolean isNewOrder(){
        for (Order order:orders) {
            if(order.Status.equals("Beklemede")){
                return true;
            }
        }
        return  false;
    }

    public int getId(){
        return basketDetail.get(0).id;
    }

    public int activeOrders(){
        int orderCount = 0 ;
        for (Order order:orders) {
            if(order.Status.equals("Beklemede")){
                orderCount++;
            }
        }
        return  orderCount;
    }
    public double getTotal(){
         double total = 0 ;
        for (Order order:orders) {
            total += Double.valueOf(order.total_price);
        }
        return  round(total+ ObiApp.tip,2);
    }

    public double getTotals(){
        double total = 0 ;
        for (Order order:orders) {
            total += Double.valueOf(order.total_price);
        }
        return  round(total+ ObiApp.tip,2);
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setBasket(PackageResp res){

        this.orders = res.orders;
        this.basketDetail = res.basketDetail ;
     }

    public List<PriceType> getVenuePayment(){
        return basketDetail.get(0).venue.venuePayment;
    }
    public List<PriceType> getPackagePayment(){
        return basketDetail.get(0).venue.packagePayment;
    }
    public List<PriceType> getFastFoodPayment(){
        return basketDetail.get(0).venue.fastfoodPayment;
    }
}
