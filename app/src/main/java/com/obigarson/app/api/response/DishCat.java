package com.obigarson.app.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.obigarson.app.api.model.DishCategory;
import com.obigarson.app.api.model.OrderedDish;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstf on 12/12/15.
 */
public class DishCat implements Parcelable {
    public DishCategory category;
    public List<OrderedDish> dishes;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.category, flags);
        dest.writeList(this.dishes);
    }

    public DishCat() {
    }

    protected DishCat(Parcel in) {
        this.category = in.readParcelable(DishCategory.class.getClassLoader());
        this.dishes = new ArrayList<OrderedDish>();
        in.readList(this.dishes, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<DishCat> CREATOR = new Parcelable.Creator<DishCat>() {
        public DishCat createFromParcel(Parcel source) {
            return new DishCat(source);
        }

        public DishCat[] newArray(int size) {
            return new DishCat[size];
        }
    };
}
