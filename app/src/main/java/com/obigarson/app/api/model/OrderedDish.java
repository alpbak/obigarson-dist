package com.obigarson.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mstf on 03/12/15.
 */
public class OrderedDish implements Parcelable {
    public Integer id;
    public  DishCategory Dishes_category;
    public String Name;
    public String Short_Description;
    public String Description;
    public String Price;
    public String Image;
    public String AvgCookTime;
    public Boolean Hot;
    public Props  DishesUnit;
    public int basketCount;

    @Override
    public String toString() {
        return "OrderedDish{" +
                "id=" + id +
                ", DishesCategory=" + Dishes_category +
                ", Name='" + Name + '\'' +
                ", ShortDescription='" + Short_Description + '\'' +
                ", Description='" + Description + '\'' +
                ", Price='" + Price + '\'' +
                ", Image='" + Image + '\'' +
                ", AvgCookTime='" + AvgCookTime + '\'' +
                ", Hot=" + Hot +
                ", DishesUnit=" + DishesUnit +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.Dishes_category, 0);
        dest.writeString(this.Name);
        dest.writeString(this.Short_Description);
        dest.writeString(this.Description);
        dest.writeString(this.Price);
        dest.writeString(this.Image);
        dest.writeString(this.AvgCookTime);
        dest.writeValue(this.Hot);
        dest.writeParcelable(this.DishesUnit, 0);
    }

    public OrderedDish() {
    }

    protected OrderedDish(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Dishes_category = in.readParcelable(DishCategory.class.getClassLoader());
        this.Name = in.readString();
        this.Short_Description = in.readString();
        this.Description = in.readString();
        this.Price = in.readString();
        this.Image = in.readString();
        this.AvgCookTime = in.readString();
        this.Hot = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.DishesUnit = in.readParcelable(Props.class.getClassLoader());
    }

    public static final Parcelable.Creator<OrderedDish> CREATOR = new Parcelable.Creator<OrderedDish>() {
        public OrderedDish createFromParcel(Parcel source) {
            return new OrderedDish(source);
        }

        public OrderedDish[] newArray(int size) {
            return new OrderedDish[size];
        }
    };
}


