package com.obigarson.app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.PriceType;
import com.obigarson.app.util.ObiApp;

import java.util.ArrayList;
import java.util.List;

public class PaketSiparisNotActivity extends AppCompatActivity implements View.OnClickListener{

    @Bind(R.id.not_back_button)
    ImageButton back;
    @Bind(R.id.not_pay_button)
    Button payButton;
    @Bind(R.id.not_editText)
    EditText note_text;
    SweetAlertDialog pDialog;
    ImageButton payBackButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_paket_siparis_not);
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        payButton.setOnClickListener(this);
        note_text.setOnClickListener(this);


    }
    @Override
    public void onBackPressed() {
        Intent intent;
        intent = new Intent(PaketSiparisNotActivity.this, DiscoverActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.not_back_button:
                finish();
                break;
            case R.id.not_pay_button:
                //basketPay();
                if (note_text.length() == 0){
                    System.out.println("NOT BOŞ");
                    basketPay();

                }
                else{
                    System.out.println("NOT DOLU: " + note_text.getText().toString());
                    saveNote();

                }

                break;
        }
    }

    public void saveNote(){
        Log.d("saveNote", "saveNote");

        try {
            Call<Message> call;
            call = ObiApp.api.packageNote(ObiApp.venue.venueCode, ObiApp.address.id, note_text.getText().toString());

            call.enqueue(new Callback<Message>() {

                @Override
                public void onResponse(retrofit.Response<Message> response) {
                    Log.d("saveNote rspse.raw", response.raw().toString());
                    try {
                        Message msg;

                        if ((response.isSuccess())) {
                            msg = response.body();
                            Log.d("saveNote rspse.body()", response.body().toString());
                            basketPay();


                        } else if (response.errorBody() != null) {
                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Log.d("PAYMENT: ERROR: ", msg.message);
                            Log.d("PAYMENT: RAW RSPN: ", response.raw().toString());
                            Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void basketPay(){
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();

        List<PriceType> payments = new ArrayList<>();
        if (ObiApp.isQRActive) {
            payments = ObiApp.basket.getVenuePayment();
        } else if (ObiApp.basket != null) {
            payments = ObiApp.basket.getPackagePayment();
        }

            final ArrayAdapter<PriceType> adapter = new ArrayAdapter<>(PaketSiparisNotActivity.this, android.R.layout.simple_list_item_1,
                    payments);

            AlertDialog.Builder dialog = new AlertDialog.Builder(PaketSiparisNotActivity.this);
            dialog.setCustomTitle(getLayoutInflater().inflate(R.layout.custom_alert_dialog, null));
            dialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    callPayment(adapter.getItem(which).id);
                }
            });

            dialog.show();
    }
    public void callPayment(int paymentId) {

        Log.d("callPayment", "callPayment");

        try {
            Call<Message> call;
            if (ObiApp.isQRActive) {
                call = ObiApp.api.orderPayment(ObiApp.venueID,
                        ObiApp.tableID, paymentId);
            } else {
                call = ObiApp.api.orderPackagePayment(ObiApp.venue.venueCode,
                        ObiApp.address.id, paymentId);
                Log.d("venueCode: ", ObiApp.venue.venueCode.toString());
                Log.d("addressId: ", ObiApp.address.id.toString());
                System.out.println("paymentId: " + paymentId);
            }
            call.enqueue(new Callback<Message>() {

                @Override
                public void onResponse(retrofit.Response<Message> response) {
                    Log.d("callPaymnt rspse.raw", response.raw().toString());
                    try {
                        Message msg;

                        if ((response.isSuccess())) {
                            msg = response.body();
                            Log.d("callPaymnt rspse.body()", response.body().toString());

                            setContentView(R.layout.layout_after_order);
                            payBackButton = (ImageButton) findViewById(R.id.pay_back_button);
                            payBackButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ObiApp.venue = null;
                                    ObiApp.basket = null;
                                    startActivity(new Intent(PaketSiparisNotActivity.this, MainScreenActivity.class));
                                    finish();
                                }
                            });
                            pDialog.dismissWithAnimation();
                            new SweetAlertDialog(PaketSiparisNotActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getString(R.string.payment_forwarded))
                                    .setConfirmText(getString(R.string.okey))
                                    .show();
                        } else if (response.errorBody() != null) {
                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Log.d("PAYMENT: ERROR: ", msg.message);
                            Log.d("PAYMENT: RAW RSPN: ", response.raw().toString());
                            Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
