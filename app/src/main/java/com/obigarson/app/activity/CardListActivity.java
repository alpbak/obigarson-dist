package com.obigarson.app.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.CardsAdapter;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.CreditCardResp;
import com.obigarson.app.util.ObiApp;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;

public class CardListActivity extends BaseActivity {
    String msg = "Android : ";

    @Bind(R.id.cards_back_button)  ImageButton backButton;
    @Bind(R.id.cards_add_button) Button addButton;


    ListView cardList;
    CardsAdapter adapter;
    boolean fromBasket;
    static boolean whatFrom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_cards);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            fromBasket = extras.getBoolean("fromBasket", false);
            whatFrom = fromBasket;
            if (fromBasket)
                addButton.setVisibility(View.INVISIBLE);
        }

        cardList = (ListView) findViewById(R.id.my_cards_list);
        adapter = new CardsAdapter(this);
        cardList.setAdapter(adapter);

        backButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
        new ListCards().execute();
        initiliaze(R.id.my_cards);
        hideActionBar();


    }


    public static boolean fromBasket(){
        return whatFrom;
    }

    public void onBackPressed() {
        backButton.performClick();
    }

     @Override
    protected void onStart() {
        super.onStart();
     }

     @Override
    protected void onResume() {
        super.onResume();
     }

     @Override
    protected void onPause() {
        super.onPause();
     }

     @Override
    protected void onStop() {
        super.onStop();
     }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.cards_back_button:
                if (fromBasket){
                    intent = new Intent(CardListActivity.this, BasketActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    intent = new Intent(CardListActivity.this, MainScreenActivity.class);
                    intent.putExtra("fromScanner", true);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.cards_add_button:
                intent = new Intent(CardListActivity.this, CardAddActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

     @Override
    public void onDestroy() {
        super.onDestroy();
     }


    private class ListCards extends AsyncTask<Void, Void, Void> {

        private CreditCardResp res;
        private Message msg;


        @Override
        protected Void doInBackground(Void... params) {
            try {
                Call<CreditCardResp> call = ObiApp.api.getCreditCards("");

                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                    res =(CreditCardResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (res != null || !res.creditcards.isEmpty() ) {
                cardList.setVisibility(View.VISIBLE);
                adapter.clear();
                adapter.addAll(res.creditcards);
                adapter.notifyDataSetChanged();
            }else{
                Toast.makeText(CardListActivity.this,msg.message,Toast.LENGTH_SHORT).show();
            }
        }


    }
}