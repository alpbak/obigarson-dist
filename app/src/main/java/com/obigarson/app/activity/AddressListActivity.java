package com.obigarson.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.AddressAdapter;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.AddressResp;
import com.obigarson.app.util.ObiApp;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class AddressListActivity extends BaseActivity {

    @Bind(R.id.address_back_button)
    ImageButton backButton;
    @Bind(R.id.address_add_button)
    Button addButton;


    ListView addressList;
    AddressAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_addresses);
        ButterKnife.bind(this);

        addressList = (ListView) findViewById(R.id.address_list);
        adapter = new AddressAdapter(this);
        addressList.setAdapter(adapter);
        backButton.setOnClickListener(this);
        addButton.setOnClickListener(this);

        //initiliaze(R.id.my_addresses);
        hideActionBar();
        getAddresses();
    }


    public void onBackPressed() {
        backButton.performClick();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.address_back_button:
                intent = new Intent(AddressListActivity.this,  MainScreenActivity.class);
                intent.putExtra("fromScanner", true);
                finish();
                startActivity(intent);
                break;
            case R.id.address_add_button:
                intent = new Intent(AddressListActivity.this,  AddressAddActivity.class);
                finish();
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void getAddresses() {
        try {
            Call<AddressResp> call = ObiApp.api.getUserAddresses();

            call.enqueue(new Callback<AddressResp>() {

                @Override
                public void onResponse(Response<AddressResp> response) {

                    try {
                        if ((response.isSuccess())) {
                            adapter.clear();
                            adapter.addAll(response.body().address);

                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Toast.makeText(AddressListActivity.this, msg.message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(AddressListActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}