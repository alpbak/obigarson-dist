package com.obigarson.app.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.obigarson.app.R;
import com.obigarson.app.util.ObiApp;

public class FastFoodOrderOKActivity extends AppCompatActivity {
    ImageButton payBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_after_order);

        payBackButton = (ImageButton) findViewById(R.id.pay_back_button);
        payBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObiApp.venue = null;
                ObiApp.basket = null;
                startActivity(new Intent(FastFoodOrderOKActivity.this, DiscoverActivity.class));
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        ObiApp.venue = null;
        ObiApp.basket = null;
        startActivity(new Intent(FastFoodOrderOKActivity.this, DiscoverActivity.class));
        finish();
    }

}
