package com.obigarson.app.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.obigarson.app.R;
import com.obigarson.app.adapters.ReservationAdapter;
import com.obigarson.app.api.response.ReservationResp;
import com.obigarson.app.util.ObiApp;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;

public class ReservationListActivity extends BaseActivity {
    String msg = "Android : ";

    @Bind(R.id.reservation_back_button)  ImageButton backButton;
    @Bind(R.id.reserve_empty_warning)  TextView warningText;

    ListView reserveList;
    ReservationAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_reservation_list);
        ButterKnife.bind(this);

        reserveList = (ListView) findViewById(R.id.reservation_list);
        adapter = new ReservationAdapter(this);
        reserveList.setAdapter(adapter);

        backButton.setOnClickListener(this);
        new ListReservation().execute();
        initiliaze(R.id.my_reservations);
        hideActionBar();
    }


    public void onBackPressed() {
        backButton.performClick();
    }

     @Override
    protected void onStart() {
        super.onStart();
     }

     @Override
    protected void onResume() {
        super.onResume();
     }

     @Override
    protected void onPause() {
        super.onPause();
     }

     @Override
    protected void onStop() {
        super.onStop();
     }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reservation_back_button:
                //Intent loginIntent = new Intent(ReservationListActivity.this, MainScreenActivity.class);
                //loginIntent.putExtra("fromScanner", true);
                //startActivity(loginIntent);
                finish();
                break;
        }
    }

     @Override
    public void onDestroy() {
        super.onDestroy();
     }
    private class ListReservation extends AsyncTask<Void, Void, Void> {

        private ReservationResp res;


        @Override
        protected Void doInBackground(Void... params) {
            try {
                Call<ReservationResp> call = ObiApp.api.getUserReserves();

                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                    res =(ReservationResp) resp.body();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (res != null && res.reservations != null && !res.reservations.isEmpty()  ) {
                reserveList.setVisibility(View.VISIBLE);
                warningText.setVisibility(View.GONE);
                adapter.clear();
                adapter.addAll(res.reservations);
                adapter.notifyDataSetChanged();
            }else{
                reserveList.setVisibility(View.GONE);
                warningText.setVisibility(View.VISIBLE);
            }
        }


    }
}