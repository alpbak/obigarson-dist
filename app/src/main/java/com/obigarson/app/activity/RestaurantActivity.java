package com.obigarson.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.RestaurantSpecAdapter;
import com.obigarson.app.api.model.Address;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.AddressResp;
import com.obigarson.app.api.response.QrResp;
import com.obigarson.app.util.ObiApp;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class RestaurantActivity extends Activity implements View.OnClickListener {
    String msg = "Android : ";
    Venue restaurant;

    @Bind(R.id.restaurant_top_name)
    TextView restNameTop;
    @Bind(R.id.restaurant_name)
    TextView restName;
    @Bind(R.id.restaurant_type)
    TextView restType;
    @Bind(R.id.restaurant_address)
    TextView restAddress;
    @Bind(R.id.working_hours_val)
    TextView restWorkHours;
    @Bind(R.id.order_count)
    TextView orderCount;

    @Bind(R.id.restaurant_img)
    ImageView restaurantImg;
    @Bind(R.id.order_before_active)
    ImageView orderBeforeImg;
    @Bind(R.id.reserve_active)
    ImageView reserveImg;
    @Bind(R.id.takeaway_active)
    ImageView takeAwayImg;

    @Bind(R.id.restaurant_reserve_button)
    Button reserveButton;
    @Bind(R.id.restaurant_package_button)
    Button packageButton;
    @Bind(R.id.restaurant_menu_button)
    Button menuButton;
    @Bind(R.id.restaurant_back_button)
    ImageButton back;
    @Bind(R.id.restaurant_basket_button)
    ImageButton basket;

    private RecyclerView mRecyclerView;
    private RestaurantSpecAdapter adapter;


    private ProgressDialog dialog;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ObiApp.venue == null && false == getIntent().hasExtra("restaurant")) {
            finish();
            startActivity(new Intent(this, DiscoverActivity.class));

        } else {
            setContentView(R.layout.layout_restaurant);
            ButterKnife.bind(this);
            mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            GridLayoutManager mgr = new GridLayoutManager(RestaurantActivity.this, 3);
            mgr.setSmoothScrollbarEnabled(false);
            mRecyclerView.setLayoutManager(mgr);
            mRecyclerView.setHasFixedSize(true);
            restaurant = getIntent().getParcelableExtra("restaurant");
            ObiApp.venue = restaurant;

            dialog = new ProgressDialog(RestaurantActivity.this);
            restNameTop.setText(restaurant.venueName);
            reserveButton.setText(restaurant.reservation_status ? getString(R.string.reservation) : getString(R.string.no_reservation));
            packageButton.setText(restaurant.takeaway_status ? getString(R.string.takeaway)  : getString(R.string.no_takeaway) );
            setActiveImgs();
            back.setOnClickListener(this);
            basket.setOnClickListener(this);
            reserveButton.setOnClickListener(restaurant.reservation_status ? this : null);
            packageButton.setOnClickListener(restaurant.takeaway_status ? this : null);
            menuButton.setOnClickListener(this);
            adapter = new RestaurantSpecAdapter(RestaurantActivity.this, restaurant.venueProperty);
            mRecyclerView.setAdapter(adapter);


        }
    }
    public void updatebasketCount(){

        int activeOrders = 0 ;
        if(ObiApp.basket!=null) activeOrders = ObiApp.basket.activeOrders();

        if(activeOrders == 0){
            orderCount.setVisibility(View.GONE);
        }else{
            orderCount.setVisibility(View.VISIBLE);
            orderCount.setText(String.valueOf(activeOrders));
        }
    }
    public void setActiveImgs() {
        if (restaurant.reservation_status)
            Picasso.with(RestaurantActivity.this).load(R.drawable.reservation_on).into(reserveImg);
        if (restaurant.takeaway_status)
            Picasso.with(RestaurantActivity.this).load(R.drawable.takeaway_on).into(takeAwayImg);
        if (restaurant.getgoing_status)
            Picasso.with(RestaurantActivity.this).load(R.drawable.order_before_on).into(orderBeforeImg);


        restName.setText(restaurant.venueName);
        restType.setText(restaurant.venueCategory.getName());
        restAddress.setText(restaurant.venueAddress);
        restWorkHours.setText(restaurant.openTime + "-" + restaurant.closeTime);

        if (restaurant.venueImage != null) {
            Picasso.with(RestaurantActivity.this).load(restaurant.venueImage).into(restaurantImg);
        } else {
            Picasso.with(RestaurantActivity.this).load(R.drawable.obi_default).into(restaurantImg);
        }
    }

    public void getRestaurant() {
        Intent intent = new Intent(RestaurantActivity.this, DiscoverActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        back.performClick();
    }

    /**
     * Called when the activity is about to become visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        updatebasketCount();
        Log.d(msg, "The onStart() event");
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatebasketCount();
        Log.d(msg, "The onResume() event");
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    private void showDialog() {


    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.restaurant_package_button:
                ObiApp.isQRActive = false;
                ObiApp.packageMenu = null;
                if (ObiApp.address == null) {
                    Log.d("RESTAURANT","address == null");
                    getAddresses();
                } else {
                    Log.d("RESTAURANT","checkaddress");
                    checkAddress();
                }
                break;

            case R.id.restaurant_back_button:
                intent = new Intent(RestaurantActivity.this, DiscoverActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.restaurant_menu_button:
                    ObiApp.address = null;
                    intent = new Intent(RestaurantActivity.this, MenuActivity.class).putExtra("restaurant", restaurant);;
                      startActivity(intent);
                      finish();
                break;

            case R.id.restaurant_basket_button:
                intent = new Intent(RestaurantActivity.this, BasketActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.restaurant_reserve_button:
                intent = new Intent(RestaurantActivity.this, ReservationActivity.class);
                intent.putExtra("restaurant", restaurant);
                startActivity(intent);
                finish();
                break;


        }
    }

    public void getAddresses() {
        try {
            Call<AddressResp> call = ObiApp.api.getUserAddresses();

            call.enqueue(new Callback<AddressResp>() {
                List<Address> addresses;

                @Override
                public void onResponse(Response<AddressResp> response) {
                    try {
                        if ((response.isSuccess())) {
                            addresses = response.body().address;
                            if (addresses.isEmpty()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RestaurantActivity.this);
                                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(RestaurantActivity.this, AddressAddActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                                builder.setMessage(getString(R.string.ask_to_add_address));
                                builder.show();
                            } else {

                                ArrayAdapter<Address> adapter = new ArrayAdapter<>(RestaurantActivity.this, android.R.layout.simple_list_item_1, addresses);
                                AlertDialog.Builder dialog = new AlertDialog.Builder(RestaurantActivity.this);

                                View view = getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);
                                TextView title = (TextView) view.findViewById(R.id.alertTitle);
                                title.setText(getString(R.string.choose_address));
                                dialog.setCustomTitle(view);
                                dialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ObiApp.address = addresses.get(which);
                                        dialog.dismiss();
                                        checkAddress();

                                    }
                                });

                                dialog.show();
                            }
                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void checkAddress() {
        try {
            Call<QrResp> call = ObiApp.api.checkPackage(ObiApp.venue.venueCode, ObiApp.address.id);
            call.enqueue(new Callback<QrResp>() {

                @Override
                public void onResponse(Response<QrResp> response) {
                    try {
                        if ((response.isSuccess())) {
                            ObiApp.packageMenu = response.body();
                            ObiApp.isQRActive = false;
                            Intent intent = new Intent(RestaurantActivity.this, MenuActivity.class);
                             startActivity(intent);
                            finish();
                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Toast.makeText(RestaurantActivity.this, msg.message, Toast.LENGTH_SHORT).show();
                            ObiApp.address = null;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

}