package com.obigarson.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pinball83.maskededittext.MaskedEditText;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Activation;
import com.obigarson.app.api.model.Credentials;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.util.IntlPhoneInput;
import com.obigarson.app.util.ObiApp;
import java.lang.ref.WeakReference;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class RegisterActivity extends Activity implements View.OnClickListener {

    public Credentials credentials;
    //EditTexts
    @Bind(R.id.register_email_val)
    EditText email;
    @Bind(R.id.register_name_val)
    EditText name;
    @Bind(R.id.register_pass_val)
    EditText pass;
    @Bind(R.id.register_repass_val)
    EditText repass;
    @Bind(R.id.register_phone_val)
    com.obigarson.app.util.IntlPhoneInput phone;

    //Buttons
    @Bind(R.id.register_button)
    Button register;

    @Bind(R.id.login_back_button)
    ImageButton loginBack;

    @Bind(R.id.register_accept_textView)
    TextView registerText;

    // DO NOT BIND WITH ButterKnife since these are belongs to layout_confirm it would
    // throw null pointer. Define these as button where you initialize the layout.

    Button sendSms, askSms;
    EditText confirmValue;
    String telNo;
    boolean smsSent;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register_page);
        ButterKnife.bind(this);
        loginBack.setOnClickListener(this);
        register.setOnClickListener(this);
        registerText.setOnClickListener(this);

        //Check for facebook login in bundle data, if exists then set these values.
        if (getIntent().hasExtra("facebook")) {
            credentials = getIntent().getParcelableExtra("facebook");
            setFacebook(credentials);
        } else {
            credentials = new Credentials();
        }

        // Android phone formatter
        //phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        /*
        phone.setMaskIconCallback(new MaskedEditText.MaskIconCallback() {
            @Override
            public void onIconPushed() {
                System.out.println("CLICK");
                //phone.setText("");
            }
        });
        */

        phone.setOnValidityChange(new IntlPhoneInput.IntlPhoneInputListener() {
            @Override
            public void done(View view, boolean isValid) {
                if(phone.isValid()) {
                    telNo = phone.getNumber();
                    //System.out.println("VALID: " + telNo);
                }
            }
        });
    }

    // When user uses Facebook Login set the email, name
    // and make sure it is not editable.
    public void setFacebook(Credentials credentials) {
        if (credentials != null) {
            email.setText(credentials.email);
            name.setText(credentials.first_name + " " + credentials.last_name);
            email.setKeyListener(null);
            name.setKeyListener(null);
            phone.requestFocus();
        }
    }


    /**
     * Called when the activity is about to become visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        loginBack.performClick();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_accept_textView:
                Intent privacyIntent = new Intent(RegisterActivity.this, PrivacyActivity.class);
                startActivity(privacyIntent);
                //finish();
                break;

            case R.id.login_back_button:
                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.register_button:
                final String emailVal = email.getText().toString();
                final String nameVal = name.getText().toString();
                final String passVal = pass.getText().toString();
                //final String phoneVal = phone.getText().toString();
                final String phoneVal = telNo;
                //final String phoneVal = "+90 (535) 234-8539";

                //Check if email is valid
                if (!isEmailValid(emailVal)) {
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.email_not_valid))
                            .setConfirmText(getString(R.string.okey))
                            .show();
                } else if (!passVal.equals(repass.getText().toString())) { // Check if the passwords match.
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.pass_match_fail))
                            .setConfirmText(getString(R.string.okey))
                            .show();
                }
                else if(!phone.isValid()){
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.must_enter_tel))
                            .setConfirmText(getString(R.string.okey))
                            .show();
                }

                /*
                else if(phoneVal.length() == 0){
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.must_enter_tel))
                            .setConfirmText(getString(R.string.okey))
                            .show();
                }
                */

                else {

                    credentials.email = emailVal;
                    credentials.telephone = phoneVal;
                    credentials.password = passVal;
                    credentials.first_name = getName(nameVal);
                    credentials.last_name = getLastName(nameVal);
                    credentials.devicetoken = "";

                    new RegisterTask().execute();
                }
                break;
            case R.id.ask_sms_button:
                //Check if last sms was 5 minutes before
                final long timeLimit = 300; // 300 seconds waiting time
                final long currentTime = (System.currentTimeMillis() - ObiApp.timer) / 1000; // How many seconds passed after the last sms code was asked.
                if (currentTime < timeLimit) {
                    Toast.makeText(RegisterActivity.this, String.format(getString(R.string.ask_sms_timer), (timeLimit - currentTime)), Toast.LENGTH_SHORT).show();
                } else {
                    register.performClick();
                }
                break;
            case R.id.confirm_sms_button:
                sendSms(confirmValue.getText().toString());
                break;
        }
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public String getName(String fullName) {
        String[] nameList = fullName.split(" ");
        if (nameList.length > 0) {
            return nameList[0];

        }
        return "";
    }

    public String getLastName(String fullName) {
        String[] nameList = fullName.split(" ");
        if (nameList.length > 1) {
            return nameList[nameList.length - 1];
        }

        return "";
    }

    public void sendSms(String code) {
        try {
            //Call<LoginResp> call = ObiApp.api.activateAccount(Utils.encode(new Activation(credentials, Integer.valueOf(code)).toString()));
            String whatt = Utils.encode(new Activation(credentials, code).toString());
            Call<LoginResp> call = ObiApp.api.activateAccount(whatt);

            System.out.println("ACTIVATE: " + whatt);

            call.enqueue(new Callback<LoginResp>() {
                LoginResp user;

                @Override
                public void onResponse(Response<LoginResp> response) {
                    /*
                    System.out.println("RESPONSE message: " + response.message());
                    System.out.println("RESPONSE body: " + response.body());
                    System.out.println("RESPONSE code: " + response.code());
                    System.out.println("RESPONSE headers: " + response.headers());
                    System.out.println("RESPONSE raw: " + response.raw());
                    */

                    try {
                        if ((response.isSuccess())) {
                            //Set user info to database and shared info and switch to the MainScreen.
                            user = response.body();
                            ObiApp.setToken(user.token);
                            ObiApp.user = user;
                            ObiApp.obiDB.setLogin(user);
                            saveUser();
                            Intent intent = new Intent(RegisterActivity.this, MainScreenActivity.class);
                            ObiApp.profileImg = ObiApp.obiDB.getFbId();
                            finish();
                            startActivity(intent);
                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Toast.makeText(RegisterActivity.this, msg.message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(RegisterActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    //Save user token and info to the SharedPreferences for further uses.
    public void saveUser() {
        SharedPreferences sh_Pref = getBaseContext().getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh_Pref.edit();
        editor.putString("userResp", ObiApp.user.toString());
        editor.apply();
    }

    public LoginResp getUser() {
        SharedPreferences sh_Pref = getBaseContext().getSharedPreferences("USER", Context.MODE_PRIVATE);
        return new Gson().fromJson(sh_Pref.getString("userResp", ""), LoginResp.class);
    }


    public class RegisterTask extends AsyncTask<String, String, String> {

        private ProgressDialog dialog;
        private Message msg;
        private boolean register;

        @Override
        protected String doInBackground(String... params) {
            try {
                Call<Message> call = ObiApp.api.sendSms(Utils.encode(new Gson().toJson(credentials)));
                //System.out.println("REG: " + Utils.encode(new Gson().toJson(credentials)));

                retrofit.Response resp = call.execute();
                if ((register = resp.isSuccess())) {
                    msg = (Message) resp.body();
                } else if (resp.errorBody() != null) {
                    msg = new Gson().fromJson(resp.errorBody().string(), Message.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            if (msg != null) {
                Toast.makeText(RegisterActivity.this, msg.message, Toast.LENGTH_SHORT).show();
                if (register) {
                    setConfirm();
                    //Check if the message is already activated. If so ask user to enter code.
                } else if (msg.message.contains("aktivasyon kodu")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setConfirm();
                        }
                    });
                    builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.setMessage(getString(R.string.ask_activate_for_given_code));
                    builder.show();
                }
                else if (msg.message.contains("Aktivasyon kodu daha önce gönderildi")){
                    System.out.println("HOBAAAAAA");
                    setConfirm();
                }
            } else {
                Toast.makeText(RegisterActivity.this, getString(R.string.not_available), Toast.LENGTH_SHORT).show();
            }

        }


        @Override
        protected void onPreExecute() {

            dialog = new ProgressDialog(RegisterActivity.this);
            dialog.setMessage(getString(R.string.connecting));
            dialog.show();
            dialog.setCancelable(false);

        }

        //Set layout to the confirm page and set clicks to the Activity.
        public void setConfirm() {
            ObiApp.timer = System.currentTimeMillis();
            setContentView(R.layout.layout_confirm_page);
            sendSms = (Button) findViewById(R.id.confirm_sms_button);
            askSms = (Button) findViewById(R.id.ask_sms_button);
            confirmValue = (EditText) findViewById(R.id.confirm_sms_val);
            sendSms.setOnClickListener(RegisterActivity.this);
            askSms.setOnClickListener(RegisterActivity.this);
        }
    }





}