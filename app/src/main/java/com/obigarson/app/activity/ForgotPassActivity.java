package com.obigarson.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Activation;
import com.obigarson.app.api.model.ForgotPassword;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.ForgetPasswordResp;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.api.response.ReservationResp;
import com.obigarson.app.util.ObiApp;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class ForgotPassActivity extends Activity implements View.OnClickListener{
    String msg = "Android : ";
    SweetAlertDialog pDialog;
    String telVal;

    @Bind(R.id.forgot_back_button) ImageButton forgotBack;

    @Bind(R.id.forgot_send_button) Button send;

    @Bind(R.id.forgot_email_val)
    com.obigarson.app.util.IntlPhoneInput phone;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot_password);
        ButterKnife.bind(this);
        forgotBack.setOnClickListener(this);
        send.setOnClickListener(this);

        Log.d(msg, "The onCreate() event");
    }



    /** Called when the activity is about to become visible. */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "The onResume() event");
    }

    /** Called when another activity is taking focus. */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** Called when the activity is no longer visible. */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    @Override
    public void onBackPressed() {
        forgotBack.performClick();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_back_button:
                Intent loginIntent = new Intent(ForgotPassActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.forgot_send_button:
                if(!phone.isValid()){
                    new SweetAlertDialog(ForgotPassActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.must_enter_tel))
                            .setConfirmText(getString(R.string.okey))
                            .show();
                }
                else{
                    telVal = phone.getNumber();
                    //new PasswordTask().execute();
                    sendPassword();
                }


                break;
        }
    }

    /** Called just before the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void sendPassword() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();

        try {
            Call<Message> call=null;


            String teleVal = "{\"telephone\":\""+telVal+"\"}";
            System.out.println("FORGET: " + Utils.encode(teleVal));

            call= ObiApp.api.forgotPassword(Utils.encode(teleVal));
            call.enqueue(new Callback<Message>() {

                @Override
                public void onResponse(Response<Message> response) {

                    try {
                        pDialog.dismiss();
                        Message msg;
                        if(response.isSuccess()){
                            msg = response.body();

                            System.out.println("Response: " + response.body());



                        }else{

                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        }

                        Toast.makeText(ForgotPassActivity.this, msg.message, Toast.LENGTH_SHORT).show();
                        new SweetAlertDialog(ForgotPassActivity.this, SweetAlertDialog.NORMAL_TYPE)
                                .setTitleText(msg.message)
                                .setConfirmText(getString(R.string.okey))
                                .show();



                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}