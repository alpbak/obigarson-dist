package com.obigarson.app.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.Loc;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.checkBasket;
import com.obigarson.app.api.response.BasketResp;
import com.obigarson.app.api.response.CreditCardResp;
import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.api.response.QrResp;
import com.obigarson.app.util.ObiApp;
import com.onesignal.OneSignal;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit.Call;
import retrofit.Callback;

public class ScannerActivity extends BaseActivity implements ZXingScannerView.ResultHandler, View.OnClickListener {
    private ZXingScannerView mScannerView;
    public final String TAG = "SCANNER";
    @Bind(R.id.scanner_flash_icon) ImageButton flashButton;
    @Bind(R.id.camera_DiscoverButton)
    Button discoverButton;
    @Bind(R.id.main_button_reserves) Button  search;

    boolean flashOn = false;

    boolean doubleBackToExitPressedOnce = false;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 1230;

    String oneSignalUserID;
    String regId;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_camera_yeni);
        ButterKnife.bind(this);

        new UserChecker().execute();
        initiliaze(R.id.home_page);

        LinearLayout qrCameraLayout = (LinearLayout) findViewById(R.id.scanner_fragment);
        final RelativeLayout reklamLayout = (RelativeLayout) findViewById(R.id.reklamLayout);
        reklamLayout.setVisibility(View.GONE);
        /*
        reklamLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fadeOutReklam(reklamLayout);
            }
        });
        */

        search.setOnClickListener(this);
        search.setVisibility(View.GONE);

        mScannerView = new ZXingScannerView(this);
        mScannerView.setFlash(flashOn);

        mScannerView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        List<BarcodeFormat> list = new LinkedList<>();
        list.add(BarcodeFormat.QR_CODE);
        qrCameraLayout.addView(mScannerView);

        //backButton.setOnClickListener(this);
        discoverButton.setOnClickListener(this);
        flashButton.setOnClickListener(this);

        Log.d("ScannerActivity", "ScannerActivity");

        checkCameraPermission();

        //System.out.println("PUSH ID: " + getPushId());
        new updateDeviceToken().execute();
        new checkBasketEmpty().execute();

        checkForErdenerPayment();

    }

    private void checkForErdenerPayment(){
        Calendar c = Calendar.getInstance();

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date today = c.getTime();

        int year = 2017;
        int month = 2;
        int dayOfMonth = 20;

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date dateSpecified = c.getTime();

        if (dateSpecified.before(today)) {
            System.err.println("Date specified [" + dateSpecified + "] is before today [" + today + "]");
            showClosureAlert();
        } else {
            System.err.println("Date specified [" + dateSpecified + "] is NOT before today [" + today + "]");
        }
    }

    private void showClosureAlert(){
        new AlertDialog.Builder(ScannerActivity.this)
                .setTitle("Hata")
                .setMessage("Bazı iç hatalardan ve uyuşmazlıklardan dolayı uygulama başlatılamıyor ve kapatılacak.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void fadeOutReklam(final RelativeLayout img)
    {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                img.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        img.startAnimation(fadeOut);
    }
    @TargetApi(23)
    private void checkCameraPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            // Do something for lollipop and above versions
            int hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    showMessageOKCancel(getString(R.string.permission_camera),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[] {Manifest.permission.CAMERA},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                requestPermissions(new String[] {Manifest.permission.CAMERA},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }



        System.out.println("CAMERA PERMISSION ALREADY GRANTED");

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ScannerActivity.this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.okey), okListener)
                .setNegativeButton(getString(R.string.cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    System.out.println("CAMERA PERMISSION GRANTED BY USER");
                } else {
                    // Permission Denied
                    Toast.makeText(ScannerActivity.this, "CAMERA PERMISSION Denied", Toast.LENGTH_SHORT).show();
                    System.out.println("CAMERA PERMISSION DENIED BY USER");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ObiApp.isQRActive = false;
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanner_back_icon:
                Intent loginIntent = new Intent(ScannerActivity.this, MainScreenActivity.class);
                loginIntent.putExtra("fromScanner", true);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.scanner_flash_icon:
                //switch flash boolean
                if((flashOn = (flashOn == false))) {
                    flashButton.setImageResource(R.drawable.flash_off);
                }else{
                    flashButton.setImageResource(R.drawable.flash_on);
                }
                mScannerView.setFlash(flashOn);
                break;
            case R.id.camera_DiscoverButton:
                Intent intent;
                intent = new Intent(ScannerActivity.this, DiscoverActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.main_button_reserves:
                intent = new Intent(ScannerActivity.this, ReservationListActivity.class);
                startActivity(intent);
                break;

        }
    }
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(ScannerActivity.this, getString(R.string.press_back_quit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class UserChecker extends AsyncTask<Void, Void, Void> {

        private BasketResp basket;
        private Message msg;


        @Override
        protected Void doInBackground(Void... params) {
            try {

                //    Call<BasketResp> call = Singleton.api.getBasket(12,3);
                //    retrofit.Response resp = call.execute();
/*
                if(resp.isSuccess()){
                    basket =(BasketResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }
*/
            } catch (Exception e) {
                Log.e("Main", e.getMessage());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

            //   if (basket != null && basket.basketDetail == false) {
            //       Singleton.isQRActive = true;
            //      Intent intent = new Intent(MainScreenActivity.this, MenuActivity.class).putExtra("isActive",true);
            ///        Toast.makeText(MainScreenActivity.this, "Aktif masanızın menüsü getirildi.", Toast.LENGTH_SHORT).show();
            //       finish();
            //       startActivity(intent);
            //   }else {
            ObiApp.isQRActive = false;
            ObiApp.tipCount =0;
            ObiApp.tip=0;
            //   }
        }

    }

    @Override
    public void handleResult(Result rawResult) {
        final String[] result = rawResult.getText().split(",");

        //Log.d("Scanner Result", rawResult.getText());

        try {
            Call<QrResp> call = ObiApp.api.getQrCode(Long.valueOf(result[0]),Long.valueOf(result[1]));
            call.enqueue(new Callback<QrResp>() {

                @Override
                public void onResponse(retrofit.Response<QrResp> response) {
                    if ((response.isSuccess())) {
                        try{
                            ObiApp.venue = response.body().venue.get(0);
                            ObiApp.isQRActive = true;
                            ObiApp.qrResp = response.body();
                            ObiApp.venueID = Long.valueOf(result[0]);
                            ObiApp.tableID = Long.valueOf(result[1]);
                            ObiApp.isApart = response.body().apartStatus;
                            Intent intent = new Intent(ScannerActivity.this, MenuActivity.class).putExtra("restaurant", ObiApp.venue);
                            startActivity(intent);
                            finish();

                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                    } else if (response.errorBody() != null) {
                        try {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();

                        } catch (Exception ex) {
                            mScannerView.stopCamera();
                            mScannerView.startCamera();
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
            mScannerView.stopCamera();
            mScannerView.startCamera();
        }
    }

    public String getPushId(){

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                //Log.d("ONESIGNAL debug", "User:" + userId);
                oneSignalUserID = userId;

                //Log.d("ONESIGNAL debug", "User:" + userId);

                //if (registrationId != null)
                    //Log.d("ONESIGNAL debug", "registrationId:" + registrationId);
            }
        });

        return oneSignalUserID;
    }





    private class updateDeviceToken extends AsyncTask<String, Void, Void> {

        private CreditCardResp res;
        private  Message msg;
        ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {


            try {

                String valueX = "{\"devicetoken\":\"none\", \"onesignal_id\":\""+getPushId()+"\"}";
                //System.out.println("UPDATE ONESIGNAL: " + valueX);

                Call<Message> call = ObiApp.api.updateOnSignalId(Utils.encode(valueX));
                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                    //Log.e("Save OneSignal", "OK!!");
                }else if(resp.errorBody()!=null){
                    //Log.e("Save OneSignal", "NOT OK!!");
                }

            } catch (Exception e) {
                //dialog.dismiss();
                Log.e("Save OneSignal error", e.getMessage());
            }


            return null;
        }

        @Override
        protected  void onPreExecute(){
            /*
            dialog  = new ProgressDialog(CardAddActivity.this);
            dialog.setCancelable(false);
            dialog.setTitle(getString(R.string.adding_card));
            dialog.show();
            */
        }

        @Override
        protected void onPostExecute(Void result) {

            /*
            dialog.dismiss();
            if (res != null) {
                Intent intent = new Intent(CardAddActivity.this, CardListActivity.class);
                startActivity(intent);
                finish();
            } else if (msg != null) {
                new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.err))
                        .setContentText(msg.message)
                        .setConfirmText(getString(R.string.okey))
                        .show();
            } else {
                new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.operation_failed))
                        .setConfirmText(getString(R.string.okey))
                        .show();
            }
            */


        }


    }




    private class checkBasketEmpty extends AsyncTask<String, Void, Void> {

        private checkBasket res;
        private  Message msg;
        ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            System.out.println("BASKET CHECK - doInBackground");

            try {
                Call<checkBasket> call = ObiApp.api.checkBasketEmpty();
                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                    res =(checkBasket) resp.body();

                    if (res.isActive()){
                        //SEPETTE BİR ŞEYLER VAR SEPETE ATALIM
                        Intent intent;
                        intent = new Intent(ScannerActivity.this, BasketActivity.class);
                        intent.putExtra("checkAllBaskets", true);
                        startActivity(intent);
                        finish();
                    }



                }else if(resp.errorBody()!=null){
                    //Log.e("Save OneSignal", "NOT OK!!");
                }

            } catch (Exception e) {
                //dialog.dismiss();
                Log.e("BASKET CHECK error", e.getMessage());
            }


            return null;
        }

        @Override
        protected  void onPreExecute(){
            System.out.println("BASKET CHECK - onPreExecute");
            /*
            dialog  = new ProgressDialog(CardAddActivity.this);
            dialog.setCancelable(false);
            dialog.setTitle(getString(R.string.adding_card));
            dialog.show();
            */
        }

        @Override
        protected void onPostExecute(Void result) {
            System.out.println("BASKET CHECK - onPostExecute");
            /*
            dialog.dismiss();
            if (res != null) {
                Intent intent = new Intent(CardAddActivity.this, CardListActivity.class);
                startActivity(intent);
                finish();
            } else if (msg != null) {
                new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.err))
                        .setContentText(msg.message)
                        .setConfirmText(getString(R.string.okey))
                        .show();
            } else {
                new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.operation_failed))
                        .setConfirmText(getString(R.string.okey))
                        .show();
            }
            */

        }


    }
}
