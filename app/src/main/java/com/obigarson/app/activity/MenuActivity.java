package com.obigarson.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.MenuAdapter;
import com.obigarson.app.api.ObiApi;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.OrderSort;
import com.obigarson.app.api.model.OrderedDish;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.MenuResp;
import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.api.response.QrResp;
import com.obigarson.app.api.response.VenueDish;
import com.obigarson.app.fragments.MenuFragmentAdapter;
import com.obigarson.app.util.ObiApp;
import com.obigarson.app.util.ShakeDetector;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class MenuActivity extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener{

    // The following are used for the shake detection
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    MenuAdapter adapter;
    Venue restaurant;
    ViewPager pager;
    MenuFragmentAdapter fragmentAdapter;
    TabLayout tabLayout;

    private ProgressDialog dialog;

    @Bind(R.id.menu_back_button) ImageButton back;
    @Bind(R.id.menu_basket_button) ImageButton basket;
    @Bind(R.id.menu_rest_name) TextView restName;
    @Bind(R.id.menu_rest_type) TextView restType;
    @Bind(R.id.menu_rest_img) ImageView restImg;
    @Bind(R.id.order_count) TextView orderCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu);
        ButterKnife.bind(this);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                System.out.println("SHAKE IT BABY");
                checkWaiter();
            }
        });

        pager = (ViewPager) findViewById(R.id.viewpager);
        fragmentAdapter  = new MenuFragmentAdapter(getSupportFragmentManager());

        restaurant = ObiApp.venue;
        back.setOnClickListener(this);
        basket.setOnClickListener(this);

        restName.setText(restaurant.venueName);
        restType.setText(restaurant.venueCategory.getName());
        if(restaurant.venueImage != null ){
            Picasso.with(MenuActivity.this).load(String.valueOf(restaurant.venueImage)).into(restImg);
        }

         adapter = new MenuAdapter(this);

        /*
        if(ObiApp.isQRActive && ObiApp.basket !=null){
           back.setVisibility(View.GONE);
        }else{
           back.setVisibility(View.VISIBLE);
        }
        */

        //updatebasketCount();



        setBasket();

        tabLayout = (TabLayout) findViewById(R.id.viewpagertab);
        pager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(pager);

        if (ObiApp.isQRActive) {
            back.setVisibility(View.GONE);
        }
        else {
            back.setImageResource(R.drawable.back_blue);
            back.setVisibility(View.VISIBLE);
        }

    }
public void updatebasketCount(){

    int activeOrders = 0 ;
    if(ObiApp.basket!=null) activeOrders = ObiApp.basket.activeOrders();

    if(activeOrders == 0){
        orderCount.setVisibility(View.GONE);
    }else{
        orderCount.setVisibility(View.VISIBLE);
        orderCount.setText(String.valueOf(activeOrders));
    }
}
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final OrderedDish dish = (OrderedDish) parent.getItemAtPosition(position);

        System.out.println("MENU CLICK");

        Intent loginIntent = new Intent(MenuActivity.this, DishActivity.class).putExtra("dish",dish);
        startActivity(loginIntent);
        finish();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        dialog = new ProgressDialog(MenuActivity.this);
        dialog.setMessage(getString(R.string.connecting));
        dialog.setCancelable(false);
        dialog.show();

        new MenuTask().execute();
    }

    /** Called when the activity is about to become visible. */
    @Override
    protected void onStart() {
        super.onStart();
        updatebasketCount();
    }


    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
        updatebasketCount();
    }

    /** Called when another activity is taking focus. */
    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    /** Called when the activity is no longer visible. */
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.menu_back_button:
                if (!ObiApp.isQRActive){
                    intent  = new Intent(MenuActivity.this, RestaurantActivity.class).putExtra("restaurant",restaurant);
                    startActivity(intent);
                    finish();
                }
                else{
                    checkWaiter();
                }
                break;
            case R.id.menu_basket_button:
               intent = new Intent(MenuActivity.this, BasketActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if(back.getVisibility() == View.VISIBLE){
            //back.performClick();
        }
    }
    /** Called just before the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class MenuTask extends AsyncTask<Void, Void, Void> {

        private QrResp res;
        private MenuResp menu;
        private Message msg;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if(ObiApp.isQRActive){
                    res = ObiApp.qrResp;
                }else if(ObiApp.address !=null) {
                    res = ObiApp.packageMenu;
                }else{
                    //System.out.println("MENU ACTIVITY venue code: " + "{\"venue\":"+ ObiApp.venue.venueCode+"}");

                    Call<MenuResp> call = ObiApp.api.getMenu(Utils.encode("{\"venue\":"+ ObiApp.venue.venueCode+"}"));

                    Response resp = call.execute();
                    if (resp.isSuccess()) {
                        menu = (MenuResp) resp.body();
                    } else if (resp.errorBody() != null) {
                        msg = new Gson().fromJson(resp.errorBody().string(), Message.class);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            dialog.dismiss();
             if (res != null) {
                setFragment(res.venueDishes);
            } else if(menu != null) {
                 setFragment(menu.venueDishes);
            }else{

                 Toast.makeText(MenuActivity.this,msg.message,Toast.LENGTH_SHORT).show();
             }
        }

        private void setFragment(List<VenueDish> dishes){
            Iterator<VenueDish> i = dishes.iterator();
            while (i.hasNext()) {
                VenueDish dish = i.next();

                if(dish.dishes.isEmpty() || (!ObiApp.isQRActive && dish.alcohol_inc)){
                    i.remove();
                }
            }

            fragmentAdapter.setDataList(dishes);
            fragmentAdapter.notifyDataSetChanged();
            tabLayout.setupWithViewPager(pager);
        }
    }

    public void setBasket() {
        try {
            if (ObiApp.isQRActive) {
                if (ObiApp.isApart){//Apart sepeti ise
                    setPackageApart();
                }
                else { //Normal sepet ise

                    Call<Basket> call = ObiApp.api.getBasket(ObiApp.venueID, ObiApp.tableID);

                    call.enqueue(new Callback<Basket>() {

                        @Override
                        public void onResponse(retrofit.Response<Basket> response) {

                            try {
                                if ((response.isSuccess())) {

                                    ObiApp.basket = new Basket();
                                    ObiApp.basket = response.body();
                                    updatebasketCount();

                                } else if (response.errorBody() != null) {
                                    //Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                                    updatebasketCount();

                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();

                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                        }
                    });

                }
            }
            else if (ObiApp.isfastFood){
                System.out.println("FAST FOOD");
                setPackageFastFood();
            }
            else {
                setPackage();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void setPackageFastFood() {
        //System.out.println("FAST SET BASKET: venueCode: " + ObiApp.venue.venueCode);

        Call<PackageResp> call = ObiApp.api.getFastFoodBasket(ObiApp.venue.venueCode);

        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {
                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        updatebasketCount();

                    } else if (response.errorBody() != null) {
                        //Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        updatebasketCount();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }
    public void setPackageApart() {

        //System.out.println("venue: " + ObiApp.venue.venueCode);
        //System.out.println("table: " + ObiApp.tableID);

        Call<PackageResp> call = ObiApp.api.getApartBasket(ObiApp.venue.venueCode, ObiApp.tableID);
        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {
                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        updatebasketCount();


                    } else if (response.errorBody() != null) {
                        //Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        updatebasketCount();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    //Log.d("BASKET ERROR 01", ex.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                //Log.d("BASKET ERROR 02", t.toString());
            }
        });
    }

    public void setPackage() {
        Call<PackageResp> call = ObiApp.api.getPackageBasket(ObiApp.venue.venueCode, ObiApp.address.id);
        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {

                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        updatebasketCount();

                    } else if (response.errorBody() != null) {
                        //Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        updatebasketCount();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }
    public void checkWaiter(){
        if (ObiApp.isQRActive && !ObiApp.isApart) {

            new SweetAlertDialog(MenuActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText(ObiApp.venue.venueName)
                    .setContentText(getString(R.string.garson_cagir))
                    .setConfirmText(getString(R.string.yes))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            callWaiter();
                        }
                    })
                    .setCancelText(getString(R.string.no))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    })
                    .show();
        }
    }
    public void callWaiter(){
        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Message> call;
        call = ObiApp.api.callWaiter(ObiApp.tableID);
        call.enqueue(new Callback<Message>() {

            @Override
            public void onResponse(Response<Message> response) {

                try {
                    Message msg;
                    if(response.isSuccess()){
                        pDialog.dismissWithAnimation();
                        msg = response.body();

                        new SweetAlertDialog(MenuActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.garson_ok))
                                .setConfirmText(getString(R.string.okey))
                                .show();

                    }else{
                        msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }
}