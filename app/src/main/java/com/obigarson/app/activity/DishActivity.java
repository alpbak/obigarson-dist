package com.obigarson.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.OrderedDish;
import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.util.ObiApp;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;

public class DishActivity extends Activity implements View.OnClickListener {
    String msg = "Android : ";
    @Bind(R.id.dish_minus_button)
    ImageButton minusButton;
    @Bind(R.id.dish_plus_button)
    ImageButton plusButton;
    @Bind(R.id.dish_back_button)
    ImageButton backButton;
    @Bind(R.id.dish_basket_button)
    ImageButton basketButton;
    @Bind(R.id.dish_confirm_button)
    Button confirmButton;
    @Bind(R.id.dish_img)
    ImageView dishImage;

    @Bind(R.id.dish_name)
    TextView dishName;
    @Bind(R.id.dish_specific)
    TextView dishType;
    @Bind(R.id.dish_not_available)
    TextView dishAvailable;
    @Bind(R.id.dish_price)
    TextView dishPrice;
    @Bind(R.id.dish_count_picker)
    TextView dishCount;
    @Bind(R.id.order_count)
    TextView orderCount;
    @Bind(R.id.dish_note)
    EditText dishNote;

    @Bind(R.id.dish_note_label)
    TextView dishNoteLabel;



    OrderedDish dish;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dish);
        ButterKnife.bind(this);
        dish = getIntent().getParcelableExtra("dish");
        backButton.setOnClickListener(this);
        basketButton.setOnClickListener(this);
        confirmButton.setOnClickListener(this);
        minusButton.setOnClickListener(this);
        plusButton.setOnClickListener(this);

        dishName.setText(dish.Name);
        dishType.setText(dish.Description);
        dishAvailable.setVisibility(View.GONE);
        dishPrice.setText(String.valueOf(dish.Price));
        Picasso.with(DishActivity.this).load(dish.Image).into(dishImage);
        if(dish.basketCount == 0) dish.basketCount =1;
        dishCount.setText(String.valueOf(dish.basketCount));
         int activeOrders = 0;
        if(ObiApp.basket !=null) activeOrders= ObiApp.basket.activeOrders();

        if(activeOrders == 0){
            orderCount.setVisibility(View.GONE);
        }else{
           orderCount.setVisibility(View.VISIBLE);
            orderCount.setText(String.valueOf(activeOrders));
        }

        //Apart siparişi ise notu ve labelini saklayalım
        if (ObiApp.isApart){ //Apart ödemesi ise
            dishNote.setVisibility(View.INVISIBLE);
            dishNoteLabel.setVisibility(View.INVISIBLE);
        }

        Log.d("DISH ACTIVITY", "000000");

        System.out.println("REST SELECT MENU: " + ObiApp.venue.venueName + " ID: " + ObiApp.venue.id);
    }


    /**
     * Called when the activity is about to become visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        //Log.d(msg, "The onStart() event");
    }
    public void onBackPressed() {
        backButton.performClick();
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
        //Log.d(msg, "The onResume() event");
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        //Log.d(msg, "The onPause() event");
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        //Log.d(msg, "The onStop() event");
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.dish_back_button:
                intent = new Intent(DishActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.dish_basket_button:
                intent = new Intent(DishActivity.this, BasketActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.dish_minus_button:
                dish.basketCount = dish.basketCount == 1 ? 1 : dish.basketCount - 1;
                dishCount.setText(String.valueOf(dish.basketCount));
               break;
            case R.id.dish_plus_button:
                dish.basketCount = dish.basketCount == 10 ? 10 : dish.basketCount + 1;
                dishCount.setText(String.valueOf(dish.basketCount));
                break;
            case R.id.dish_confirm_button:
                if(ObiApp.isQRActive && !ObiApp.isApart) {
                    orderConfirm();
                }
                else if (ObiApp.isfastFood){
                    Log.e("DISH", "FAST FOOD");
                    addFastFoodOrder();
                }
                else{
                    addOrder();
                }
                break;
        }
    }


    public void orderConfirm(){

        new SweetAlertDialog(DishActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(DishActivity.this.getString(R.string.sipari_im))
                .setContentText(DishActivity.this.getString(R.string.confirm_order))
                .setConfirmText(DishActivity.this.getString(R.string.yes))
                .setCancelText(DishActivity.this.getString(R.string.no))
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        addOrder();

                    }
                })
                .show();
    }
    public void addOrder() {
        try {
            if(ObiApp.isQRActive){

                if (ObiApp.isApart){//Apart order ise
                    apartOrder();
                }
                else { //Normal order ise
                    System.out.println("VenueID: " + ObiApp.venueID);
                    System.out.println("TableID: " + ObiApp.tableID);
                    System.out.println("dish.id: " + dish.id);
                    System.out.println("dish.basketCount: " + dish.basketCount);

                    SharedPreferences.Editor editor = DishActivity.this.getSharedPreferences("basket", MODE_PRIVATE).edit();
                    editor.putLong("venueID", ObiApp.venueID);
                    editor.putLong("tableID", ObiApp.tableID);
                    editor.putInt("typeOfBasket", 2);
                    editor.commit();

                    Call<Basket> call = ObiApp.api.addOrder(ObiApp.venueID, ObiApp.tableID, dish.id, dish.basketCount);

                    call.enqueue(new Callback<Basket>() {

                        @Override
                        public void onResponse(retrofit.Response<Basket> response) {

                            try {
                                if ((response.isSuccess())) {
                                    ObiApp.basket = new Basket();
                                    ObiApp.basket = response.body();
                                    Intent intent = new Intent(DishActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else if (response.errorBody() != null) {
                                    System.out.println("DISH ACTIVITY ERROR: " + response.errorBody());
                                    Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                                    Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                System.out.println("DISH ACTIVITY ERROR 02: " + ex.toString());
                                ex.printStackTrace();

                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
                Intent intent = new Intent(DishActivity.this, BasketActivity.class);
                startActivity(intent);
                finish();



            }
            else{
                packageOrder();
            }
            addNote();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void addNote(){
        try {
            if(dishNote.getText().toString().equals("")){
               return;
            }
            String note = dish.Name + " " + dishNote.getText().toString();
                Call<Message> call;
                if(ObiApp.isQRActive){
                    call = ObiApp.api.orderNote(ObiApp.venueID,
                            ObiApp.tableID, note);
                }else {
                    call = ObiApp.api.packageNote(ObiApp.venue.venueCode,
                            ObiApp.address.id, note);
                }

                call.enqueue(new Callback<Message>() {

                    @Override
                    public void onResponse(retrofit.Response<Message> response) {

                        try {
                            if (response.errorBody() != null) {
                                Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                                Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void packageOrder(){

        SharedPreferences.Editor editor = DishActivity.this.getSharedPreferences("basket", MODE_PRIVATE).edit();
        editor.putLong("venueCode", ObiApp.venue.venueCode);
        editor.putLong("addressId", ObiApp.address.id);
        editor.putInt("typeOfBasket", 4);
        editor.commit();
        try{
            Call<PackageResp> call = ObiApp.api.addPackageOrder(ObiApp.venue.venueCode, ObiApp.address.id, dish.id, dish.basketCount);

             call.enqueue(new Callback<PackageResp>() {

                @Override
                public void onResponse(retrofit.Response<PackageResp> response) {

                    try {
                        if ((response.isSuccess())) {
                            ObiApp.basket = new Basket();
                            ObiApp.basket.setBasket(response.body());
                            Intent intent = new Intent(DishActivity.this, MenuActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        }catch (Exception ex){

        }
    }
    public void apartOrder(){
        Log.d("DISH ACTIVITY", "ADD APART ORDER");

        SharedPreferences.Editor editor = DishActivity.this.getSharedPreferences("basket", MODE_PRIVATE).edit();
        editor.putLong("venueID", ObiApp.venueID);
        editor.putLong("tableID", ObiApp.tableID);
        editor.putInt("typeOfBasket", 1);
        editor.commit();

        Call<Basket> call = ObiApp.api.addApartOrder(ObiApp.venueID,
                ObiApp.tableID, dish.id, dish.basketCount);

        call.enqueue(new Callback<Basket>() {

            @Override
            public void onResponse(retrofit.Response<Basket> response) {

                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket = response.body();
                        Intent intent = new Intent(DishActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (response.errorBody() != null) {
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }
    public void addFastFoodOrder() {
        Log.d("DISH ACTIVITY", "ADD FAST FOOD ORDER");

        SharedPreferences.Editor editor = DishActivity.this.getSharedPreferences("basket", MODE_PRIVATE).edit();
        editor.putLong("venueCode", ObiApp.venue.venueCode);
        editor.putInt("typeOfBasket", 3);
        editor.commit();

        Call<Basket> call = ObiApp.api.addFastFoodOrder(ObiApp.venue.venueCode, dish.id, dish.basketCount);

        System.out.println("VenueID: " + ObiApp.venue.venueCode);
        System.out.println("dish.id: " + dish.id);
        System.out.println("dish.basketCount: " + dish.basketCount);

        call.enqueue(new Callback<Basket>() {

            @Override
            public void onResponse(retrofit.Response<Basket> response) {

                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket = response.body();
                        Intent intent = new Intent(DishActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (response.errorBody() != null) {
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}