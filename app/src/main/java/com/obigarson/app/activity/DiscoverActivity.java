package com.obigarson.app.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.MessageQueue;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.DiscoverAdapter;
import com.obigarson.app.adapters.RestaurantAdapter;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Loc;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.VenueGroup;
import com.obigarson.app.api.response.VenueResp;
import com.obigarson.app.util.ObiApp;
import com.sa90.materialarcmenu.ArcMenu;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit.Call;
import retrofit.Callback;

public class DiscoverActivity extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    @Bind(R.id.discover_back_button)
    ImageButton back;
    @Bind(R.id.discover_qr_button)
    ImageButton basket;
    @Bind(R.id.discover_order_before)
    Button orderBefore;
    @Bind(R.id.discover_reserve)
    Button reservation;
    @Bind(R.id.discover_takeaway)
    Button takeaway;
    @Bind(R.id.searchCancelButton)
    Button searchCancel;
    @Bind(R.id.searchEditText)
    EditText searchBox;

    @Bind(R.id.fabLocationButton)
    ImageButton fabLocationButton;
    @Bind(R.id.fabShopButton)
    ImageButton fabShopButton;
    @Bind(R.id.arcMenu)
    ArcMenu arcMenu;



    ViewPager pager;
    RestaurantAdapter restaurantAdapter;
    TabLayout tabLayout;
    ProgressDialog pd;
    int selectedBottomMenu = 2;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 1230;
    boolean isLocationPermissionGranted;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        ObiApp.selectedShopCategory = 2;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_discover);
        ButterKnife.bind(this);
        pager = (ViewPager) findViewById(R.id.viewpager);

        restaurantAdapter = new RestaurantAdapter(getSupportFragmentManager());
        back.setOnClickListener(this);
        fabLocationButton.setOnClickListener(this);
        fabShopButton.setOnClickListener(this);
        basket.setOnClickListener(this);
        orderBefore.setOnClickListener(this);
        reservation.setOnClickListener(this);
        searchCancel.setOnClickListener(this);
        takeaway.setOnClickListener(this);
        tabLayout = (TabLayout) findViewById(R.id.viewpagertab);




        reservation.setVisibility(View.GONE);
        takeaway.setVisibility(View.GONE);

        arcMenu.setVisibility(View.GONE);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        searchBox.setFocusable(false);
        searchBox.setFocusableInTouchMode(true);
        searchBox.clearFocus();
        searchCancel.setVisibility(View.INVISIBLE);
        searchBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!searchBox.hasFocus()){
                    searchCancel.setVisibility(View.INVISIBLE);
                }
                else{
                    searchCancel.setVisibility(View.INVISIBLE);
                }
            }
        });

        pager.setAdapter(restaurantAdapter);
        tabLayout.setupWithViewPager(pager);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 searchCancel.setVisibility(View.VISIBLE);
                 Utils.setSearchString(s.toString());
                 restaurantAdapter.filter("4");
                 restaurantAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        isLocationPermissionGranted = false;

        checkLocationPermission();

        //orderBefore.performClick();
    }

    public void startDiscovery(){
        //checkLocation();
        isLocationPermissionGranted = true;
    }

    @TargetApi(23)
    private void checkLocationPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            int hasCoarseLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            if (hasCoarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {

                    showMessageOKCancel(getString(R.string.permission_location),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }
        System.out.println("LOCATION PERMISSION ALREADY GRANTED");
        startDiscovery();

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(DiscoverActivity.this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.okey), okListener)
                .setNegativeButton(getString(R.string.cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    System.out.println("LOCATION PERMISSION GRANTED BY USER");
                    startDiscovery();
                } else {
                    // Permission Denied
                    Toast.makeText(DiscoverActivity.this, "LOCATION PERMISSION Denied", Toast.LENGTH_SHORT).show();
                    System.out.println("LOCATION PERMISSION DENIED BY USER");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        pd = new ProgressDialog(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        ObiApp.isQRActive = false;
        checkLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.fabLocationButton:
                System.out.println("SHOP");
                break;
            case R.id.fabShopButton:
                System.out.println("LOCATION");
                selectShopType();
                break;
            case R.id.discover_back_button:
                ObiApp.isfastFood = false;
                intent = new Intent(DiscoverActivity.this, MainScreenActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.discover_qr_button:
                ObiApp.isfastFood = false;
                intent = new Intent(DiscoverActivity.this, ScannerActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.discover_order_before:
                System.out.println("SELF SERVICE");

                ObiApp.isfastFood = true;
                selectedBottomMenu = 1;
                restaurantAdapter.filter("1");
                setPassive();
                orderBefore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.order_before_on), null, null);
                orderBefore.setTextColor(Color.parseColor("#DF5F87"));
                tabLayout.getTabAt(0).select();
                break;

            case R.id.discover_reserve:
                ObiApp.isfastFood = true;
                selectedBottomMenu = 2;
                restaurantAdapter.filter("1");
                setPassive();
                reservation.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.reservation_on), null, null);
                reservation.setTextColor(Color.parseColor("#DF5F87"));
                break;

            case R.id.discover_takeaway:
                ObiApp.isfastFood = true;
                selectedBottomMenu = 3;
                restaurantAdapter.filter("1");
                setPassive();
                takeaway.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.takeaway_on), null, null);
                takeaway.setTextColor(Color.parseColor("#DF5F87"));
                break;

            case R.id.discover_listview:
                //  intent = new Intent(DiscoverActivity.this, MenuActivity.class);
                //  startActivity(intent);
                //  finish();
                break;
            case R.id.searchCancelButton:
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                searchBox.getText().clear();
                searchBox.clearFocus();
                searchBox.setCursorVisible(false);
                searchBox.getText().clear();
                break;
        }
    }

    public void setPassive() {
        orderBefore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.order_before_off), null, null);
        orderBefore.setTextColor(Color.parseColor("#A3A3A3"));
        reservation.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.reservation_off), null, null);
        reservation.setTextColor(Color.parseColor("#A3A3A3"));
        takeaway.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.takeaway_off), null, null);
        takeaway.setTextColor(Color.parseColor("#A3A3A3"));
    }

    @Override
    public void onBackPressed() {
        back.performClick();
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Venue restaurant = (Venue) parent.getItemAtPosition(position);
        Intent intent = new Intent(DiscoverActivity.this, RestaurantActivity.class).putExtra("restaurant", restaurant);
        startActivity(intent);
        finish();
    }
    public void updateMenu(VenueGroup groups){
        try {
            //restaurantAdapter.setDataList(groups.venues);
            //restaurantAdapter.notifyDataSetChanged();
            //tabLayout.setupWithViewPager(pager);
        }
        catch (Exception e){
            Log.e("MENU Error: ", e.getMessage());
        }

    }

    public void checkLocation() {

        System.out.println("checkLocation");


        boolean gps_enabled = false;
        boolean network_enabled = false;

        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        } catch (Exception ex) {

        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch (Exception ex) {

        }


        try {

            if (!gps_enabled && !network_enabled) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(DiscoverActivity.this);
                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        //finish();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        back.performClick();
                    }
                });
                builder.setCancelable(false);
                builder.setTitle(getString(R.string.location_not_accessible));  // GPS not found
                builder.setMessage(getString(R.string.ask_location_activation)); // Want to enable?
                builder.show();
            } else {
                discoverRestaurants();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void discoverRestaurants() {

        System.out.println("discoverRestaurants");

        if (isLocationPermissionGranted) {
            pd.setCancelable(false);
            pd.setMessage(getString(R.string.restaurant_list_by_location));
            pd.show();

            if (ObiApp.location != null) {
                getRestaurants(ObiApp.location);
            } else {
                SmartLocation.with(DiscoverActivity.this).location()
                        .start(new OnLocationUpdatedListener() {
                            @Override
                            public void onLocationUpdated(Location location) {
                                ObiApp.location = location;
                                getRestaurants(location);
                            }
                        });
            }
        }
    }

    public final void getRestaurants(Location location) {
        try {
            Call<VenueResp> call = ObiApp.api.getVenues(Utils.encode(new Loc(location.getLongitude(), location.getLatitude()).getJson()));
            call.enqueue(new Callback<VenueResp>() {

                @Override
                public void onResponse(retrofit.Response<VenueResp> response) {
                    try {
                        if ((response.isSuccess())) {
                            VenueGroup groups = new VenueGroup();
                            final List<Venue> venues = response.body().venues;
                            Collections.sort(venues, new Comparator<Venue>() {
                                public int compare(Venue o1, Venue o2) {
                                    return o1.distance.compareTo(o2.distance);
                                }
                            });

                            groups.setCats(response.body().venues);
                            restaurantAdapter.setDataList(groups.venues);
                            restaurantAdapter.notifyDataSetChanged();
                            tabLayout.setupWithViewPager(pager);
                            pd.dismiss();
                            tabLayout.getTabAt(2).select();
                            restaurantAdapter.filter("1");
                        } else if (response.errorBody() != null) {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception ex) {
            pd.dismiss();
            ex.printStackTrace();
        }
    }

    public void selectShopType(){
        AlertDialog.Builder builder = new AlertDialog.Builder(DiscoverActivity.this);
        builder.setTitle(getString(R.string.category));
        builder.setItems(new CharSequence[]
                        {"Restaurant", "Cafe", "Bar"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0:
                                ObiApp.selectedShopCategory = 2;
                                dialog.dismiss();
                                refreshShop();
                                break;
                            case 1:
                                ObiApp.selectedShopCategory = 3;
                                dialog.dismiss();
                                refreshShop();
                                break;
                            case 2:
                                ObiApp.selectedShopCategory = 1;
                                dialog.dismiss();
                                refreshShop();
                                break;
                        }
                    }
                });
        builder.create().show();
    }
    public void refreshShop(){
        selectedBottomMenu = 1;
        restaurantAdapter.filter("1");

        switch (selectedBottomMenu){
            case 1:
                System.out.println("SELECTED 1");
                restaurantAdapter.filter("1");
                break;
            case 2:
                System.out.println("SELECTED 2");
                restaurantAdapter.filter("2");
                break;
            case 3:
                System.out.println("SELECTED 3");
                restaurantAdapter.filter("3");
                break;
        }
    }
}