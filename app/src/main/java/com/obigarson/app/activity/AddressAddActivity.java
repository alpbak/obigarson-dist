package com.obigarson.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Address;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Props;
import com.obigarson.app.api.response.AddressResp;
import com.obigarson.app.api.response.CountyResp;
import com.obigarson.app.api.response.DistrictResp;
import com.obigarson.app.api.response.NeighBourResp;
import com.obigarson.app.api.response.TownResp;
import com.obigarson.app.util.ObiApp;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class AddressAddActivity extends  BaseActivity implements AdapterView.OnItemSelectedListener {
    String msg = "Android : ";

    @Bind(R.id.address_back_button)  ImageButton backButton;
    @Bind(R.id.address_add_button) Button addButton;


    @Bind(R.id.address_val) EditText addrVal;
    @Bind(R.id.address_tel_val) EditText telVal;
    @Bind(R.id.address_title_val)  EditText titleVal;
    @Bind(R.id.countySpinner) Spinner countySpinner;
    @Bind(R.id.townSpinner) Spinner townSpinner;
    @Bind(R.id.districtSpinner) Spinner districtSpinner;
    @Bind(R.id.neighbourSpinner) Spinner neighbourSpinner;

    public List<Props> counties = new LinkedList<>();
    public List<Props> districts= new LinkedList<>();
    public List<Props> towns= new LinkedList<>();
    public List<Props> neighbourhood= new LinkedList<>();

    public Props selectedCounty,selectedTown,selectedDistrict , selectedNeighbourhood;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_address);
        ButterKnife.bind(this);

        backButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
        addrVal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    addrVal.setSelection(0);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(addrVal.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        //initiliaze(R.id.my_addresses);
        hideActionBar();
        setDataList();
        countySpinner.setOnItemSelectedListener(this);
        townSpinner.setOnItemSelectedListener(this);
        districtSpinner.setOnItemSelectedListener(this);
        neighbourSpinner.setOnItemSelectedListener(this);
    }


    public void onBackPressed() {
        backButton.performClick();
    }


    public void setDataList(){
        try{
            counties.add(new Props(0,getString(R.string.select_county)));
            districts.add(new Props(0,getString(R.string.select_district)));
            towns.add(new Props(0,getString(R.string.select_town)));
            neighbourhood.add(new Props(0, getString(R.string.select_neighbourhood)));

            Call<CountyResp> call = ObiApp.api.getCounties();
            call.enqueue(new Callback<CountyResp>() {
                @Override
                public void onResponse(Response<CountyResp> response) {
                    counties.addAll(response.body().counties);
                    ArrayAdapter<Props> adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, counties);
                    adapter.setDropDownViewResource(R.layout.dropdown_spinner);
                    countySpinner.setAdapter(adapter);
                    countySpinner.invalidate();
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });

            ArrayAdapter<Props> adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, districts);
            adapter.setDropDownViewResource(R.layout.dropdown_spinner);
            districtSpinner.setAdapter(adapter);
            districtSpinner.invalidate();

            adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, towns);
            adapter.setDropDownViewResource(R.layout.dropdown_spinner);
            townSpinner.setAdapter(adapter);
            townSpinner.invalidate();

            adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, neighbourhood);
            adapter.setDropDownViewResource(R.layout.dropdown_spinner);
            neighbourSpinner.setAdapter(adapter);
            neighbourSpinner.invalidate();


        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
     @Override
    protected void onStart() {
        super.onStart();
     }

     @Override
    protected void onResume() {
        super.onResume();
     }

     @Override
    protected void onPause() {
        super.onPause();
     }

     @Override
    protected void onStop() {
        super.onStop();
     }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.address_back_button:
                intent = new Intent(AddressAddActivity.this,  AddressListActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.address_add_button:
                new AddAddress().execute(addrVal.getText().toString(),
                        titleVal.getText().toString(),
                        telVal.getText().toString());
                break;
        }
    }

     @Override
    public void onDestroy() {
        super.onDestroy();
     }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        switch (spinner.getId()){
            case R.id.countySpinner :
                selectedCounty = (Props) countySpinner.getSelectedItem();
                setTowns();
            break;
            case R.id.townSpinner :
                selectedTown = (Props) townSpinner.getSelectedItem();
                setNeighbours();
                break;
            case R.id.districtSpinner :
                selectedDistrict = (Props) districtSpinner.getSelectedItem();
                break;
            case R.id.neighbourSpinner :
                selectedNeighbourhood = (Props) neighbourSpinner.getSelectedItem();
                setDistricts();
                break;
        }

    }
    public void setTowns(){
        if(selectedCounty.id == 0){
            return;
        }
        Call<TownResp> call = ObiApp.api.getTowns(selectedCounty.id);
        call.enqueue(new Callback<TownResp>() {
            @Override
            public void onResponse(Response<TownResp> response) {
                towns = response.body().towns;
                towns.add(0,new Props(0,getString(R.string.select_town)));
                ArrayAdapter<Props> adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, towns);
                adapter.setDropDownViewResource(R.layout.dropdown_spinner);
                townSpinner.setAdapter(adapter);
                townSpinner.invalidate();

            }

            @Override
            public void onFailure(Throwable t) {
                System.err.println("ERRROR:" + t.getLocalizedMessage());

            }
        });
    }

    public void setNeighbours(){
        if(selectedTown.id == 0){
            return;
        }

        Call<NeighBourResp> call = ObiApp.api.getNeighbours(selectedTown.id);
        call.enqueue(new Callback<NeighBourResp>() {
            @Override
            public void onResponse(Response<NeighBourResp> response) {
                neighbourhood = response.body().neighborhoods;
                neighbourhood.add(0,new Props(0, getString(R.string.select_neighbourhood)));
                ArrayAdapter<Props> adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, neighbourhood);
                adapter.setDropDownViewResource(R.layout.dropdown_spinner);
                neighbourSpinner.setAdapter(adapter);
                neighbourSpinner.invalidate();

            }

            @Override
            public void onFailure(Throwable t) {
                System.err.println("ERRROR:" + t.getLocalizedMessage());

            }
        });
    }

    public void setDistricts(){
        if(selectedNeighbourhood.id == 0){
            return;
        }

        Call<DistrictResp> call = ObiApp.api.getDistricts(selectedNeighbourhood.id);
        call.enqueue(new Callback<DistrictResp>() {
            @Override
            public void onResponse(Response<DistrictResp> response) {
                districts = response.body().districts;
                districts.add(0,new Props(0,getString(R.string.select_district)));
                ArrayAdapter<Props> adapter = new ArrayAdapter<>(AddressAddActivity.this, R.layout.spinner_item, districts);
                adapter.setDropDownViewResource(R.layout.dropdown_spinner);
                districtSpinner.setAdapter(adapter);
                districtSpinner.invalidate();
            }

            @Override
            public void onFailure(Throwable t) {
                System.err.println("ERRROR:" + t.getLocalizedMessage());

            }
        });
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private class AddAddress extends AsyncTask<String, Void, Void> {

        private AddressResp result;
        private Message  msg;

        @Override
        protected Void doInBackground(String... params) {
            try {
                final Address address = new Address();
                address.address_title = params[1];
                address.address = params[0];
                address.telephone = params[2];
                address.addressCounty =   selectedCounty;
                address.addressCountyTown =   selectedTown;
                address.addressneighborhood =  selectedNeighbourhood;
                address.addressdistrict =   selectedDistrict;

                System.out.println("ADDRESS ADD: " + Utils.encode(address.getJson()));

                Call<AddressResp> call = ObiApp.api.addUserAddress(Utils.encode(address.getJson()));
                retrofit.Response resp = call.execute();

                System.out.println("ADDRESS resp: " + resp);

                if(resp.isSuccess()){
                    result =(AddressResp) resp.body();
                }
                else if(resp.errorBody()!=null){
                     msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res) {
            if (result != null) {
                Toast.makeText(AddressAddActivity.this,getString(R.string.new_address_added),Toast.LENGTH_SHORT).show();
                backButton.performClick();
            }else if (msg == null){
                Toast.makeText(AddressAddActivity.this, getString(R.string.not_available), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(AddressAddActivity.this,msg.toString(),Toast.LENGTH_SHORT).show();
            }
        }


    }
}