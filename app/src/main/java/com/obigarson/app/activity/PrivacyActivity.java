package com.obigarson.app.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.obigarson.app.R;

import java.util.Locale;

public class PrivacyActivity extends AppCompatActivity implements View.OnClickListener{

    @Bind(R.id.privacy_webView)
    WebView privacyText;

    @Bind(R.id.forgot_back_button)
    ImageButton forgotBack;

    Uri path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_privacy);

        ButterKnife.bind(this);
        forgotBack.setOnClickListener(this);
        final String devLang = Locale.getDefault().getLanguage();
        String fileName = "privacy_"+devLang+".html";
        privacyText.loadUrl("file:///android_res/raw/"+fileName);
        System.out.println("PRIVACY 01");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_back_button:

                //Intent loginIntent = new Intent(PrivacyActivity.this, RegisterActivity.class);
                //startActivity(loginIntent);
                finish();
                break;
        }
    }

}
