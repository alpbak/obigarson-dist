package com.obigarson.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Credentials;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.FbLoginResp;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.intro.Intro;
import com.obigarson.app.util.IntlPhoneInput;
import com.obigarson.app.util.ObiApp;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class LoginActivity extends Activity implements View.OnClickListener {


    @Bind(R.id.login_phone_number)
    com.obigarson.app.util.IntlPhoneInput phoneVal;

    @Bind(R.id.login_pass_val)
    EditText passVal;

    @Bind(R.id.login_button)
    Button login;
    @Bind(R.id.login_register_button)
    Button loginRegButton;
    @Bind(R.id.login_forgot_pass)
    Button forgotPassButton;
    @Bind(R.id.facebook_login_button)
    LoginButton facebookLogin;

    @Bind(R.id.content)
    LinearLayout contentLayout;

    @Bind(R.id.entry)
    LinearLayout entryLayout;

    @Bind(R.id.footer)
    LinearLayout footerLayout;

    @Bind(R.id.login_uye_girisi_button)
    Button uyeGirisButton;

    @Bind(R.id.login_uye_ol_button)
    Button uyeOlButton;


    private CallbackManager callbackManager;
    String telNo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ObiApp.initiliaze(this);


        if ((ObiApp.user = getUser()) != null) {
            //TODO TOKEN SİL
            Log.d("TOKEN Login: ", ObiApp.user.token);

            ObiApp.profileImg = ObiApp.obiDB.getFbId();
            ObiApp.setToken(ObiApp.user.token);
            finish();
            startActivity(new Intent(this, MainScreenActivity.class));
        } else if ((ObiApp.user = ObiApp.obiDB.getLogin()) != null) {
            saveUser();
            ObiApp.setToken(ObiApp.user.token);

            ObiApp.profileImg = ObiApp.obiDB.getFbId();
            finish();
            startActivity(new Intent(this, MainScreenActivity.class));
        } else {

            FacebookSdk.sdkInitialize(this.getApplicationContext());
            setContentView(R.layout.layout_login_page);
            ButterKnife.bind(this);

            contentLayout.setVisibility(View.INVISIBLE);
            footerLayout.setVisibility(View.INVISIBLE);

            //phoneVal.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
            login.setOnClickListener(this);
            uyeGirisButton.setOnClickListener(this);
            uyeOlButton.setOnClickListener(this);
            loginRegButton.setOnClickListener(this);
            forgotPassButton.setOnClickListener(this);

            passVal.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        login.performClick();
                        return true;
                    }
                    return false;
                }
            });

            LoginManager.getInstance().logOut();

            facebookLogin.setReadPermissions(Arrays.asList("public_profile,email"));

            callbackManager = CallbackManager.Factory.create();

            facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(final LoginResult loginResult) {
                    final GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(

                                        JSONObject object,
                                        GraphResponse response) {
                                    AccessToken token;
                                    try {
                                        token = loginResult.getAccessToken();
                                        if (token != null) {
                                            ObiApp.obiDB.setFacebookAccess(token);
                                        }

                                        final Credentials credentials = new Credentials();
                                        credentials.email = response.getJSONObject().get("email").toString();
                                        credentials.first_name = response.getJSONObject().get("first_name").toString();
                                        credentials.last_name = response.getJSONObject().get("last_name").toString();
                                        credentials.facebookid = response.getJSONObject().get("id").toString();
                                        checkFbLogin(credentials);
                                    } catch (Exception ex) {
                                        LoginManager.getInstance().logOut();
                                        Toast.makeText(LoginActivity.this, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,first_name,last_name,email");
                    request.setParameters(parameters);
                    request.executeAsync();


                }

                @Override
                public void onCancel() {
                    LoginManager.getInstance().logOut();
                }

                @Override
                public void onError(FacebookException exception) {
                    LoginManager.getInstance().logOut();
                    exception.printStackTrace();
                }
            });
            phoneVal.setOnValidityChange(new IntlPhoneInput.IntlPhoneInputListener() {
                @Override
                public void done(View view, boolean isValid) {
                    if(phoneVal.isValid()) {
                        telNo = phoneVal.getNumber();
                        //System.out.println("VALID: " + telNo);
                    }
                }
            });
        }

        checkForErdenerPayment();

    }

    private void checkForErdenerPayment(){
        Calendar c = Calendar.getInstance();

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date today = c.getTime();

        int year = 2017;
        int month = 2;
        int dayOfMonth = 20;

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date dateSpecified = c.getTime();

        if (dateSpecified.before(today)) {
            System.err.println("Date specified [" + dateSpecified + "] is before today [" + today + "]");
            showClosureAlert();
        } else {
            System.err.println("Date specified [" + dateSpecified + "] is NOT before today [" + today + "]");
        }
    }

    private void showClosureAlert(){
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Hata")
                .setMessage("Bazı iç hatalardan ve uyuşmazlıklardan dolayı uygulama başlatılamıyor ve kapatılacak.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ObiApp.isShown = ObiApp.obiDB.introShown();
        if (ObiApp.isShown == false) {
            ObiApp.isShown = true;
            startCustomIntro();
        }

    }

    public void startCustomIntro() {
        Intent intent = new Intent(this, Intro.class);
        finish();
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void saveUser() {
        SharedPreferences sh_Pref = getBaseContext().getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh_Pref.edit();
        editor.putString("userResp", ObiApp.user.toString());
        editor.apply();
    }

    public LoginResp getUser() {
        SharedPreferences sh_Pref = getBaseContext().getSharedPreferences("USER", Context.MODE_PRIVATE);
        return new Gson().fromJson(sh_Pref.getString("userResp", ""), LoginResp.class);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.login_button:
                if (phoneVal.isValid()) {

                    final String pass = passVal.getText().toString();
                    new LoginTask().execute(telNo, pass);
                }

                break;

            case R.id.login_register_button:
                intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();

                break;
            case R.id.login_forgot_pass:
                intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.login_uye_girisi_button:
                //Uye Girişi
                entryLayout.setVisibility(View.INVISIBLE);
                contentLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.login_uye_ol_button:
                intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }


    private class LoginTask extends AsyncTask<String, Void, Void> {

        private SweetAlertDialog dialog;
        private LoginResp user = null;
        private Message msg = null;

        @Override
        protected Void doInBackground(String... params) {
            try {

                final Credentials credentials = new Credentials();
                credentials.telephone = params[0];
                credentials.password = params[1];
                Call<LoginResp> call = ObiApp.api.login(Utils.encode(new Gson().toJson(credentials)));

                Response resp = call.execute();
                if (resp.isSuccess()) {
                    user = (LoginResp) resp.body();
                } else if (resp.errorBody() != null) {
                    msg = new Gson().fromJson(resp.errorBody().string(), Message.class);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            dialog.dismiss();

            if (user != null) {
                ObiApp.setToken(user.token);
                ObiApp.user = user;
                ObiApp.obiDB.setLogin(user);
                saveUser();
                Intent intent = new Intent(LoginActivity.this, MainScreenActivity.class);

                startActivity(intent);
                finish();
            } else if (msg != null) {
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(msg.message)
                        .setConfirmText(getString(R.string.okey))
                        .show();
            } else {
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.not_available))
                        .setConfirmText(getString(R.string.okey))
                        .show();
            }

        }


        @Override
        protected void onPreExecute() {
            dialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            dialog.setTitleText(getString(R.string.loading));
            dialog.setCancelable(false);
            dialog.show();


        }

    }
    public void checkFbLogin(final Credentials credentials){
        try {
            JsonObject object = new JsonObject();
            object.addProperty("facebookid",credentials.facebookid);
            Call<FbLoginResp> call = ObiApp.api.fbConnect(Utils.encode(object.toString()));

            call.enqueue(new Callback<FbLoginResp>() {

                @Override
                public void onResponse(Response<FbLoginResp> response) {

                    try {
                        if(response.isSuccess()){
                            final FbLoginResp resp = response.body();
                            if(resp.token == null){
                                registerByFb(credentials);
                            }else {
                                ObiApp.user = resp.getUser();
                                saveUser();
                                ObiApp.setToken(ObiApp.user.token);
                                ObiApp.profileImg = resp.profil.facebookID;
                                if(ObiApp.profileImg !=null && !ObiApp.profileImg.equals("")){
                                    ObiApp.obiDB.setFbId(ObiApp.profileImg);
                                }
                                startActivity(new Intent(LoginActivity.this, MainScreenActivity.class));
                                finish();

                            }

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    registerByFb(credentials);
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                    registerByFb(credentials);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void registerByFb(final Credentials credentials){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class).putExtra("facebook", credentials);
        startActivity(intent);
        //startActivity(new Intent(LoginActivity.this, MainScreenActivity.class));
        finish();


    }
}