package com.obigarson.app.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.obigarson.app.R;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.BasketResp;
import com.obigarson.app.util.ObiApp;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainScreenActivity extends BaseActivity{
    String msg = "Android : ";

    @Bind(R.id.main_button_reserves) Button  search;
    @Bind(R.id.main_button_discover) Button  discover;
    @Bind(R.id.main_button_scan) Button  scan;

    boolean doubleBackToExitPressedOnce = false;
    Boolean fromScanner;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent;
        intent = new Intent(MainScreenActivity.this, ScannerActivity.class);
        startActivity(intent);
        finish();



        setContentView(R.layout.layout_home_page);
        ButterKnife.bind(this);
        search.setOnClickListener(this);
        discover.setOnClickListener(this);
        scan.setOnClickListener(this);
        new UserChecker().execute();
        initiliaze(R.id.home_page);

        /*
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            fromScanner = false;
        } else {
            fromScanner= extras.getBoolean("fromScanner", false);
        }

        if (!fromScanner) {
            Intent intent;
            intent = new Intent(MainScreenActivity.this, ScannerActivity.class);
            startActivity(intent);
            finish();
        }
        */



    }



     @Override
    protected void onStart() {
        super.onStart();

     }

     @Override
    protected void onResume() {
        super.onResume();
        ObiApp.isQRActive = false;
     }

     @Override
    protected void onPause() {
        super.onPause();
     }

     @Override
    protected void onStop() {
        super.onStop();
     }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.main_button_discover:

                intent = new Intent(MainScreenActivity.this, DiscoverActivity.class);
                startActivity(intent);
                finish();

                break;
            case R.id.main_button_scan:

                intent = new Intent(MainScreenActivity.this, ScannerActivity.class);
                startActivity(intent);
                //finish();

                break;
            case R.id.main_button_reserves:

                intent = new Intent(MainScreenActivity.this, ReservationListActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(MainScreenActivity.this, getString(R.string.press_back_quit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


     @Override
    public void onDestroy() {
        super.onDestroy();
     }

    private class UserChecker extends AsyncTask<Void, Void, Void> {

        private BasketResp basket;
        private Message msg;


        @Override
        protected Void doInBackground(Void... params) {
            try {

            //    Call<BasketResp> call = Singleton.api.getBasket(12,3);
            //    retrofit.Response resp = call.execute();
/*
                if(resp.isSuccess()){
                    basket =(BasketResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }
*/
            } catch (Exception e) {
                Log.e("Main", e.getMessage());
             }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

          //   if (basket != null && basket.basketDetail == false) {
          //       Singleton.isQRActive = true;
          //      Intent intent = new Intent(MainScreenActivity.this, MenuActivity.class).putExtra("isActive",true);
         ///        Toast.makeText(MainScreenActivity.this, "Aktif masanızın menüsü getirildi.", Toast.LENGTH_SHORT).show();
          //       finish();
          //       startActivity(intent);
          //   }else {
                 ObiApp.isQRActive = false;
            ObiApp.tipCount =0;
            ObiApp.tip=0;
          //   }
        }

    }

}