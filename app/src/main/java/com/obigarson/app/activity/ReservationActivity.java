package com.obigarson.app.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.ReservationResp;
import com.obigarson.app.util.ObiApp;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;

public class ReservationActivity extends Activity implements View.OnClickListener, TextView.OnEditorActionListener {
    String msg = "Android : ";

    @Bind(R.id.person_minus_button)
    ImageButton minusButton;
    @Bind(R.id.person_plus_button)
    ImageButton plusButton;
    @Bind(R.id.reservation_back_button)
    ImageButton backButton;
    @Bind(R.id.date_later)
    Button dateLater;
    @Bind(R.id.today)
    Button dateToday;
    @Bind(R.id.reservation_confirm_button)
    Button confirm;


    @Bind(R.id.person_count_picker)
    TextView countText;
    @Bind(R.id.reservation_date_val)
    TextView dateVal;
    @Bind(R.id.reservation_note_val)
    EditText noteVal;
    @Bind(R.id.reservation_date_selected)
    TextView date;
    @Bind(R.id.reservation_note)
    TextView note;
    @Bind(R.id.reservation_tel)
    TextView tel;
    @Bind(R.id.reservation_tel_val)
    EditText telVal;

    private int personCount = 1;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Venue restaurant;
    final Calendar c = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_reservation);
        ButterKnife.bind(this);

        backButton.setOnClickListener(this);
        minusButton.setOnClickListener(this);
        plusButton.setOnClickListener(this);
        dateLater.setOnClickListener(this);
        dateToday.setOnClickListener(this);
        confirm.setOnClickListener(this);
        noteVal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    noteVal.setSelection(0);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(noteVal.getWindowToken(), 0);
                    return true;
                }
                return  false;
            }
        });
        setButtons(false);
        restaurant = ObiApp.venue;
    }

    public void setButtons(boolean val) {
        date.setVisibility(val ? View.VISIBLE : View.GONE);
        dateVal.setVisibility(val ? View.VISIBLE : View.GONE);
        note.setVisibility(val ? View.VISIBLE : View.GONE);
        noteVal.setVisibility(val ? View.VISIBLE : View.GONE);
        tel.setVisibility(val ? View.VISIBLE : View.GONE);
        telVal.setVisibility(val ? View.VISIBLE : View.GONE);
    }


    /**
     * Called when the activity is about to become visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onBackPressed() {
        backButton.performClick();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Just ignore the [Enter] key
            return true;
        }
        // Handle all other keys in the default way
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reservation_back_button:
                Intent loginIntent = new Intent(ReservationActivity.this, RestaurantActivity.class).putExtra("restaurant", restaurant);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.person_minus_button:
                personCount = personCount == 1 ? 1 : personCount - 1;
                countText.setText(String.valueOf(personCount));
                break;
            case R.id.person_plus_button:
                personCount = personCount == 10 ? 10 : personCount + 1;
                countText.setText(String.valueOf(personCount));
                break;
            case R.id.date_later:
                dateLater.setBackgroundColor(Color.parseColor("#01BBD6"));
                dateLater.setTextColor(Color.WHITE);
                dateToday.setBackground(ContextCompat.getDrawable(this, R.drawable.customshape));
                dateToday.setTextColor(Color.parseColor("#01BBD6"));
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                setButtons(true);
                                mYear = year;
                                mMonth = monthOfYear;
                                mDay = dayOfMonth;
                                setTimePicker(c);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                break;
            case R.id.today:
                dateToday.setBackgroundColor(Color.parseColor("#01BBD6"));
                dateToday.setTextColor(Color.WHITE);
                dateLater.setBackground(ContextCompat.getDrawable(this, R.drawable.customshape));
                dateLater.setTextColor(Color.parseColor("#01BBD6"));
                mYear = c.get(Calendar.YEAR);
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                setTimePicker(c);
                break;
            case R.id.reservation_confirm_button:
                new MakeReservation().execute(dateVal.getText().toString(), countText.getText().toString(),
                        telVal.getText().toString());
                break;


        }
    }

    public void setTimePicker(Calendar c) {
        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        setButtons(true);
                        dateVal.setText(mDay + "-" + (mMonth + 1) + "-" + mYear + " " + hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, true);
        tpd.show();
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((actionId == EditorInfo.IME_ACTION_DONE)) {
             InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;
    }

    private class MakeReservation extends AsyncTask<String, Void, Void> {

        private ReservationResp res;
        private  Message msg;


        @Override
        protected Void doInBackground(String... params) {
            try {

                Call<ReservationResp> call = ObiApp.api.addUserReserve(
                        restaurant.venueCode, params[0], params[2],Integer.valueOf(params[1]));

                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                    res =(ReservationResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }

            } catch (Exception e) {
                Log.e("Reservation", e.getMessage());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (res != null) {
                Intent loginIntent = new Intent(ReservationActivity.this, ReservationListActivity.class);
                startActivity(loginIntent);
                finish();
            } else if (msg != null) {
                 new SweetAlertDialog(ReservationActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.err))
                        .setContentText(msg.message)
                        .setConfirmText(getString(R.string.okey))
                        .show();
             } else {
                new SweetAlertDialog(ReservationActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.operation_failed))
                        .setConfirmText(getString(R.string.okey))
                        .show();
            }
        }


    }
}