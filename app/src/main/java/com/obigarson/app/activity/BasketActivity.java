package com.obigarson.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.adapters.BasketAdapter;
import com.obigarson.app.api.model.BKMMessage;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.OrderSort;
import com.obigarson.app.api.model.PriceType;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.PackageResp;
import com.obigarson.app.util.ObiApp;
import com.obigarson.app.util.getappContext;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;

public class BasketActivity extends Activity implements View.OnClickListener {
    private static Context mContext;

    String msg = "Android : ";
    @Bind(R.id.basket_back_button)
    ImageButton back;

    @Bind(R.id.basket_pay_button)
    Button payButton;

    ImageButton payBackButton;
    public static TextView totalVal;
    public static TextView restName;
    @Bind(R.id.basket_rest_type)
    TextView restType;
    Venue restaurant;
    public ListView menuList;
    public static BasketAdapter adapter;
    boolean package_sent;
    SweetAlertDialog pDialog;
    static Activity activity;
    boolean fastFoodPayment;
    boolean apartPayment;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_basket);
        mContext = this;
        activity = this;
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        package_sent = false;
        fastFoodPayment = false;
        apartPayment = false;

        restaurant = ObiApp.venue;
        restName = (TextView) findViewById(R.id.basket_rest_name);
        totalVal = (TextView) findViewById(R.id.basket_total_value);
        restType.setVisibility(View.GONE);
        restName.setVisibility(View.INVISIBLE);
        payButton.setOnClickListener(this);

        if (ObiApp.isApart){//Apart ise buton yazısını değiştirelim
            payButton.setText(getString(R.string.odeme));
        }

        menuList = (ListView) findViewById(R.id.basket_listview);
        adapter = new BasketAdapter(this);
        menuList.setAdapter(adapter);
        Log.d("BASKET ACTIVITY", "START");
        /*
        System.out.println("ObiApp.venue.id: " + ObiApp.venueID);
        System.out.println("ObiApp.venueName: " + restaurant.venueName);
        System.out.println("ObiApp.table.id: " + ObiApp.tableID);
        */



        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            Boolean checkAll = bundle.getBoolean("checkAllBaskets");
            if (checkAll)
                checkAllBaskets();
            else{

                //SharedPreferences.Editor editor = mContext.getSharedPreferences("basket", MODE_PRIVATE).edit();
                //editor.clear();
                //editor.commit();

                setBasket();

            }
        }
        else{
            //SharedPreferences.Editor editor = mContext.getSharedPreferences("basket", MODE_PRIVATE).edit();
            //editor.clear();
            //editor.commit();

            setBasket();
        }



    }

public static void checkbasketEmpty(){

    if (ObiApp.basket == null || ObiApp.basket.orders.isEmpty()) {
        new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(mContext.getString(R.string.basket_empty))
                .setConfirmText(mContext.getString(R.string.okey))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        activity.onBackPressed();
                    }
                })
                .show();

    }
}
    /**
     * Called when the activity is about to become visible.
     */

    @Override
    protected void onStart() {
        super.onStart();
        //Log.d(msg, "The onStart() event");
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
        //Log.d(msg, "The onResume() event");
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        //Log.d(msg, "The onPause() event");
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        //Log.d(msg, "The onStop() event");
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.basket_back_button:
                if (restaurant == null) {
                    intent = new Intent(BasketActivity.this, DiscoverActivity.class);
                } else if (ObiApp.qrResp != null || ObiApp.packageMenu !=null) {
                    intent = new Intent(BasketActivity.this, MenuActivity.class).putExtra("restaurant", restaurant);
                }else{
                    intent = new Intent(BasketActivity.this, RestaurantActivity.class).putExtra("restaurant", restaurant);
                }
                startActivity(intent);
                finish();
                break;
            case R.id.basket_pay_button:
                basketPay();
                break;
        }
    }

    public void basketPay(){

        System.out.println("*************basketPay*************");


        if (ObiApp.isApart){//Apart siparişi ise
            apartBasketPay();
        }
        else if (ObiApp.isfastFood){
            fastFoodBasketPay();
        }
        else {//Normal siparişi ise
            System.out.println("Normal sipariş");
        List<PriceType> payments = new ArrayList<>();
        if (ObiApp.isQRActive) {
            System.out.println("Normal sipariş - isQRActive");
            payments = ObiApp.basket.getVenuePayment();
        } else if (ObiApp.basket != null) {
            System.out.println("Normal sipariş - basket != null");
            payments = ObiApp.basket.getPackagePayment();
        }


            System.out.println("basketPay DEVAM");

            final ArrayAdapter<PriceType> adapter = new ArrayAdapter<>(BasketActivity.this, android.R.layout.simple_list_item_1,
                    payments);

            AlertDialog.Builder dialog = new AlertDialog.Builder(BasketActivity.this);
            dialog.setCustomTitle(getLayoutInflater().inflate(R.layout.custom_alert_dialog, null));
            dialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    System.out.println("basketPay SEÇİLEN: " + adapter.getItem(which).id);

                    if (adapter.getItem(which).id == 5){
                        selectCreditCard();
                    }
                    else if(adapter.getItem(which).id == 10){ //BKM Express
                        System.out.println("BKM EXPRESS");
                        callBKMExpress(adapter.getItem(which).id);
                    }
                    else {
                        callPayment(adapter.getItem(which).id);
                    }
                }
            });

            dialog.show();

        }
    }

    public void apartBasketPay(){
            List<PriceType> payments = new ArrayList<>();
            if (ObiApp.isQRActive) {
                payments = ObiApp.basket.getVenuePayment();
            } else if (ObiApp.basket != null) {
                payments = ObiApp.basket.getPackagePayment();
            }

            final ArrayAdapter<PriceType> adapter = new ArrayAdapter<>(BasketActivity.this, android.R.layout.simple_list_item_1,
                    payments);

            AlertDialog.Builder dialog = new AlertDialog.Builder(BasketActivity.this);
            dialog.setCustomTitle(getLayoutInflater().inflate(R.layout.custom_alert_dialog, null));
            dialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (adapter.getItem(which).id == 5){
                        selectCreditCard();
                    }
                    else if(adapter.getItem(which).id == 10){ //BKM Express
                        System.out.println("BKM EXPRESS");
                        apartPayment = true;
                        callBKMExpress(adapter.getItem(which).id);
                    }
                    else {
                        callPayment(adapter.getItem(which).id);
                    }
                }
            });

            dialog.show();
    }

    public void fastFoodBasketPay(){
        System.out.println("********fastFoodBasketPay*****");
        System.out.println("ObiApp.venue.id: " + ObiApp.venueID);
        System.out.println("ObiApp.venueName: " + restaurant.venueName);
        System.out.println("ObiApp.table.id: " + ObiApp.tableID);
        System.out.println("**********fastFoodBasketPay*****");


        List<PriceType> payments = new ArrayList<>();
        payments = ObiApp.basket.getFastFoodPayment();

        final ArrayAdapter<PriceType> adapter = new ArrayAdapter<>(BasketActivity.this, android.R.layout.simple_list_item_1,
                payments);

        AlertDialog.Builder dialog = new AlertDialog.Builder(BasketActivity.this);
        dialog.setCustomTitle(getLayoutInflater().inflate(R.layout.custom_alert_dialog, null));
        dialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                System.out.println("fastFoodBasketPay***** SELECTED: " + adapter.getItem(which).id);

                if (adapter.getItem(which).id == 5){
                    selectCreditCard();
                }
                else if(adapter.getItem(which).id == 10){ //BKM Express
                    System.out.println("BKM EXPRESS");
                    fastFoodPayment = true;
                    callBKMExpress(adapter.getItem(which).id);
                }
                else {
                    callPayment(adapter.getItem(which).id);
                }
            }
        });

        dialog.show();
    }

    public void selectCreditCard(){
        Intent intent;
        intent = new Intent(this, CardListActivity.class);
        intent.putExtra("fromBasket", true);
        finish();
        startActivity(intent);
    }

    public static void setBasket() {
        Log.d("BASKET ACTIVITY", "Set Basket");

        System.out.println("**************setBasket**************");
        System.out.println("ObiApp.venue.id: " + ObiApp.venueID);
        System.out.println("ObiApp.table.id: " + ObiApp.tableID);
        System.out.println("**************setBasket**************");


        try {
            if (ObiApp.isQRActive) {
                if (ObiApp.isApart){//Apart sepeti ise
                    Log.d("BASKET ACTIVITY", "Apart Siparişi Başladı");
                    setPackageApart();
                }
                else { //Normal sepet ise
                    Log.d("BASKET ACTIVITY", "Normal Sepet");
                    System.out.println("BASKET ACTIVITY ObiApp.venueID" + ObiApp.venueID);
                    System.out.println("BASKET ACTIVITY ObiApp.tableID" + ObiApp.tableID);

                    Call<Basket> call = ObiApp.api.getBasket(ObiApp.venueID, ObiApp.tableID);

                    call.enqueue(new Callback<Basket>() {

                        @Override
                        public void onResponse(retrofit.Response<Basket> response) {
                            Log.d("BASKET ACTIVITY", "RESPONSE");
                            try {
                                if ((response.isSuccess())) {
                                    Log.d("BASKET ACTIVITY", "SUCCESS");
                                    ObiApp.basket = new Basket();
                                    ObiApp.basket = response.body();
                                    adapter.clear();
                                    Collections.sort(ObiApp.basket.orders, new OrderSort());
                                    adapter.addAll(ObiApp.basket.orders);
                                    restName.setVisibility(View.VISIBLE);
                                    restName.setText(ObiApp.basket.basketDetail.get(0).venue.venueName);
                                    totalVal.setText(String.valueOf(ObiApp.basket.getTotals()) + " TL");
                                    adapter.notifyDataSetChanged();
                                    checkbasketEmpty();
                                    sendOrder();


                                } else if (response.errorBody() != null) {
                                    checkbasketEmpty();
                                    Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);

                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();

                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                        }
                    });

                }




            } else {
                if (ObiApp.isfastFood){
                    setFastFoodBasket();
                }
                else{
                    setPackage();
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent;
        if (restaurant == null) {
            intent = new Intent(BasketActivity.this, DiscoverActivity.class);
        } else if (ObiApp.qrResp != null || ObiApp.packageMenu !=null) {
            intent = new Intent(BasketActivity.this, MenuActivity.class).putExtra("restaurant", restaurant);
        }else{
            intent = new Intent(BasketActivity.this, RestaurantActivity.class).putExtra("restaurant", restaurant);
        }
        startActivity(intent);
        finish();
        /*
        Intent intent;
        intent = new Intent(BasketActivity.this, DiscoverActivity.class);
        startActivity(intent);
        finish();
        */
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void callBKMExpress(int paymentId) {
        System.out.println("callBKMExpress");

        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();



        try {
            Call<BKMMessage> call;
            if (fastFoodPayment){
                call = ObiApp.api.BKMExpressFastFood(ObiApp.venueID, ObiApp.tableID, paymentId);
            }
            else if (apartPayment){
                System.out.println("ObiApp.ObiApp.venueID: " + ObiApp.venueID);
                System.out.println("ObiApp.ObiApp.tableID: " + ObiApp.tableID);
                System.out.println("paymentId: " + paymentId);

                call = ObiApp.api.BKMExpressApart(ObiApp.venueID, ObiApp.tableID, paymentId);
            }
            else {
                call = ObiApp.api.BKMExpress(ObiApp.venueID, ObiApp.tableID, paymentId);
            }



            call.enqueue(new Callback<BKMMessage>() {

                @Override
                public void onResponse(retrofit.Response<BKMMessage> response) {
                    Log.d("callPaymnt rspse.raw", response.raw().toString());
                    try {
                        BKMMessage msg;

                        if ((response.isSuccess())) {
                            pDialog.dismissWithAnimation();
                            msg = response.body();
                            //Log.d("callPaymnt rspse.body()", response.body().toString());
                            System.out.print("MSG HTML: " + msg.html);

                            Intent i = new Intent(getBaseContext(), BKMExpressActivity.class);
                            i.putExtra("HTML", msg.html);
                            startActivity(i);


                            /*
                            setContentView(R.layout.layout_after_order);
                            payBackButton = (ImageButton) findViewById(R.id.pay_back_button);
                            payBackButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ObiApp.venue = null;
                                    ObiApp.basket = null;
                                    startActivity(new Intent(BasketActivity.this, DiscoverActivity.class));
                                    finish();
                                }
                            });
                            */


                            /*
                            pDialog.dismissWithAnimation();
                            String payment_confirmation;

                            if (ObiApp.isApart){
                                payment_confirmation = getString(R.string.order_forwarded);
                            }
                            else{
                                payment_confirmation = getString(R.string.payment_forwarded);
                            }

                            new SweetAlertDialog(BasketActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(payment_confirmation)
                                    .setConfirmText(getString(R.string.okey))
                                    .show();
                                    */


                        } else if (response.errorBody() != null) {
                            pDialog.dismissWithAnimation();
                            msg = new Gson().fromJson(response.errorBody().string(), BKMMessage.class);
                            Log.d("PAYMENT: ERROR: ", msg.html);
                            Log.d("PAYMENT: RAW RSPN: ", response.raw().toString());
                            //Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();

                            new SweetAlertDialog(BasketActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.err))
                                    .setContentText(msg.html)
                                    .setConfirmText(getString(R.string.okey))
                                    .show();


                        }
                    } catch (Exception ex) {
                        pDialog.dismissWithAnimation();
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void callPayment(int paymentId) {
        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();



        try {
            Call<Message> call;
            if (ObiApp.isQRActive) {
                if (ObiApp.isApart){ //Apart ödemesi ise
                    call = ObiApp.api.orderApartPayment(ObiApp.venueID,
                            ObiApp.tableID, paymentId);
                }
                else {
                    call = ObiApp.api.orderPayment(ObiApp.venueID,
                                ObiApp.tableID, paymentId);

                }
            } else {
                if (ObiApp.isfastFood){
                    System.out.println("CARD NORMAL: " );
                    call = ObiApp.api.orderPackagePayment(ObiApp.venue.venueCode,
                            ObiApp.address.id, paymentId);
                }
                else{
                    call = ObiApp.api.orderPackagePayment(ObiApp.venue.venueCode,
                            ObiApp.address.id, paymentId);
                }

            }
            call.enqueue(new Callback<Message>() {

                @Override
                public void onResponse(retrofit.Response<Message> response) {
                    //Log.d("callPaymnt rspse.raw", response.raw().toString());
                    try {
                        Message msg;

                        if ((response.isSuccess())) {
                            pDialog.dismissWithAnimation();
                            msg = response.body();
                            //Log.d("callPaymnt rspse.body()", response.body().toString());

                            setContentView(R.layout.layout_after_order);
                            payBackButton = (ImageButton) findViewById(R.id.pay_back_button);
                            payBackButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ObiApp.venue = null;
                                    ObiApp.basket = null;
                                    startActivity(new Intent(BasketActivity.this, DiscoverActivity.class));
                                    finish();
                                }
                            });
                            pDialog.dismissWithAnimation();
                            String payment_confirmation;

                            if (ObiApp.isApart){
                                payment_confirmation = getString(R.string.order_forwarded);
                            }
                            else{
                                payment_confirmation = getString(R.string.payment_forwarded);
                            }

                            new SweetAlertDialog(BasketActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(payment_confirmation)
                                    .setConfirmText(getString(R.string.okey))
                                    .show();
                        } else if (response.errorBody() != null) {
                            pDialog.dismissWithAnimation();
                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            //Log.d("PAYMENT: ERROR: ", msg.message);
                            //Log.d("PAYMENT: RAW RSPN: ", response.raw().toString());
                            //Toast.makeText(getBaseContext(), msg.message, Toast.LENGTH_SHORT).show();

                            new SweetAlertDialog(BasketActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.err))
                                    .setContentText(msg.message)
                                    .setConfirmText(getString(R.string.okey))
                                    .show();


                        }
                    } catch (Exception ex) {
                        pDialog.dismissWithAnimation();
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


public static void sendOrder(){
    try {
        Call<Message> call = ObiApp.api.sendOrder(ObiApp.venueID,
                ObiApp.tableID, ObiApp.basket.getId());
        call.enqueue(new Callback<Message>() {

            @Override
            public void onResponse(retrofit.Response<Message> response) {

                try {
                    if ((response.isSuccess())) {
                        Log.d("BasketActivity", "Order SENT");
                    } else if (response.errorBody() != null) {
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        Log.d("BasketActivity", "Order NOT SENT");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }
            @Override
            public void onFailure(Throwable t) {

            }
        });


    } catch (Exception ex) {
        ex.printStackTrace();
    }

}

    public static void checkAllBaskets() {
        System.out.println("BASKET ACTIVITY - checkAllBaskets");

        SharedPreferences prefs = mContext.getSharedPreferences("basket", MODE_PRIVATE);
        int typeOfBasket = prefs.getInt("typeOfBasket", 0);

        System.out.println("BASKET ACTIVITY - checkAllBaskets - typeOfBasket: " + typeOfBasket);

        if (typeOfBasket == 1){
            ObiApp.venueID = prefs.getInt("venueID",0);
            ObiApp.tableID = prefs.getLong("tableID",0);
            ObiApp.isApart = true;
            setPackageApart();
        }
        else if(typeOfBasket == 2){
            ObiApp.venueID = prefs.getLong("venueID",0);
            ObiApp.tableID = prefs.getLong("tableID",0);
            ObiApp.isQRActive = true;
            setNormalBasket();

        }
        else if(typeOfBasket == 3){
            ObiApp.venue.venueCode = prefs.getInt("venueCode",0);
            ObiApp.isfastFood = true;
            setFastFoodBasket();
        }
        else if(typeOfBasket == 4){
            ObiApp.venue.venueCode = prefs.getInt("venueCode",0);
            ObiApp.address.id = prefs.getInt("addressId",0);
            ObiApp.isfastFood = true;
            setPackage();
        }


    }

    public static void setNormalBasket(){
        System.out.println("**** setNormalBasket ****");
        System.out.println("BASKET ACTIVITY ObiApp.venueID" + ObiApp.venueID);
        System.out.println("BASKET ACTIVITY ObiApp.tableID" + ObiApp.tableID);


        Call<Basket> call = ObiApp.api.getBasket(ObiApp.venueID, ObiApp.tableID);

        call.enqueue(new Callback<Basket>() {

            @Override
            public void onResponse(retrofit.Response<Basket> response) {
                Log.d("BASKET ACTIVITY", "RESPONSE");
                try {
                    if ((response.isSuccess())) {
                        Log.d("BASKET ACTIVITY", "SUCCESS");
                        ObiApp.basket = new Basket();
                        ObiApp.basket = response.body();
                        adapter.clear();
                        Collections.sort(ObiApp.basket.orders, new OrderSort());
                        adapter.addAll(ObiApp.basket.orders);
                        restName.setVisibility(View.VISIBLE);
                        restName.setText(ObiApp.basket.basketDetail.get(0).venue.venueName);
                        totalVal.setText(String.valueOf(ObiApp.basket.getTotals()) + " TL");
                        adapter.notifyDataSetChanged();
                        checkbasketEmpty();
                        sendOrder();


                    } else if (response.errorBody() != null) {
                        checkbasketEmpty();
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public static void setPackageApart() {

        System.out.println("**** setPackageApart ****");

        //System.out.println("venue: " + ObiApp.venue.venueCode);
        //System.out.println("table: " + ObiApp.tableID);

        Call<PackageResp> call = ObiApp.api.getApartBasket(ObiApp.venue.venueCode, ObiApp.tableID);
        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {
                try {
                    //Log.d("BASKET ACTIVITY", "Apart Sepet Getirme 02");
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        adapter.clear();
                        adapter.addAll(ObiApp.basket.orders);
                        restName.setVisibility(View.VISIBLE);
                        restName.setText(ObiApp.basket.basketDetail.get(0).venue.venueName);
                        totalVal.setText(String.valueOf(ObiApp.basket.getTotal()) + " TL");
                        adapter.notifyDataSetChanged();
                        checkbasketEmpty();

                    } else if (response.errorBody() != null) {
                        checkbasketEmpty();
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    //Log.d("BASKET ERROR 01", ex.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                //Log.d("BASKET ERROR 02", t.toString());
            }
        });
    }

    public static void setPackage() {
        System.out.println("**** setPackage ****");

        if (ObiApp.address == null) {
            checkbasketEmpty();
            return;
        }

        Call<PackageResp> call = ObiApp.api.getPackageBasket(ObiApp.venue.venueCode, ObiApp.address.id);


        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {

                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        adapter.clear();
                        adapter.addAll(ObiApp.basket.orders);
                        restName.setVisibility(View.VISIBLE);
                        restName.setText(ObiApp.basket.basketDetail.get(0).venue.venueName);
                        totalVal.setText(String.valueOf(ObiApp.basket.getTotal()) + " TL");
                        adapter.notifyDataSetChanged();
                        checkbasketEmpty();
                    } else if (response.errorBody() != null) {
                        checkbasketEmpty();
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public static void setFastFoodBasket() {

        System.out.println("**** setFastFoodBasket ****");


        Call<PackageResp> call = ObiApp.api.getFastFoodBasket(ObiApp.venue.venueCode);
        call.enqueue(new Callback<PackageResp>() {

            @Override
            public void onResponse(retrofit.Response<PackageResp> response) {

                try {
                    if ((response.isSuccess())) {
                        ObiApp.basket = new Basket();
                        ObiApp.basket.setBasket(response.body());
                        adapter.clear();
                        adapter.addAll(ObiApp.basket.orders);
                        restName.setVisibility(View.VISIBLE);
                        restName.setText(ObiApp.basket.basketDetail.get(0).venue.venueName);
                        totalVal.setText(String.valueOf(ObiApp.basket.getTotal()) + " TL");
                        adapter.notifyDataSetChanged();
                        checkbasketEmpty();
                    } else if (response.errorBody() != null) {
                        checkbasketEmpty();
                        Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
