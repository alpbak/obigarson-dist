package com.obigarson.app.activity;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Profile;
import com.obigarson.app.util.LogUtils;
import com.obigarson.app.util.PicassoCache;
import com.obigarson.app.util.ObiApp;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, DrawerLayout.DrawerListener
        , NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;
    //@Bind(R.id.drawer_user_name)
    TextView userName;
    //@Bind(R.id.profile_image)
    ImageView profileImg;

    int mCurrentMenuItem;
    public boolean isOn = false;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        //mDrawerToggle.syncState();
    }

    public void initiliaze(int id) {
        mCurrentMenuItem = id;
        if (false == isOn) {
            setupToolbar();
            setupNavDrawer();
            showActionBar();
            isOn = true;
        }

        mNavigationView.getMenu().findItem(id).setChecked(true);
        mDrawerLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.obi_menu));
        mNavigationView.setBackgroundColor(ContextCompat.getColor(this, R.color.obi_menu));
        new GetUserProfile().execute();


        View drawerHeader = mNavigationView.inflateHeaderView(R.layout.drawer_header);
        userName = (TextView) drawerHeader.findViewById(R.id.drawer_user_name);
        profileImg = (ImageView) drawerHeader.findViewById(R.id.profile_image);



    }

    public void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.hide();
    }

    public void showActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.show();
    }

    public void setupToolbar() {
        mToolbar = ButterKnife.findById(this, R.id.toolbar);
        if (mToolbar == null) {
            LogUtils.LOGD(this, "Didn't find a toolbar");
            return;
        }
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    public void setupNavDrawer() {
        if (mDrawerLayout == null) {
            LogUtils.LOGE(this, "mDrawerLayout is null - Can not setup the NavDrawer! Have you set the android.support.v7.widget.DrawerLayout?");
            return;
        }
        mDrawerLayout.setDrawerListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this
                , mDrawerLayout
                , mToolbar
                , R.string.navigation_drawer_open
                , R.string.navigation_drawer_close);

        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
        LogUtils.LOGD(this, "setup setupNavDrawer");
    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        mDrawerToggle.onDrawerOpened(drawerView);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        mDrawerToggle.onDrawerClosed(drawerView);
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        mDrawerToggle.onDrawerStateChanged(newState);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        @IdRes int id = menuItem.getItemId();
        if (id == mCurrentMenuItem) {
            mDrawerLayout.closeDrawers();
            return false;
        }

        Intent intent;
        switch (id) {

            case R.id.home_page:
                intent = new Intent(this, MainScreenActivity.class);
                finish();
                startActivity(intent);
                break;
            case R.id.my_cards:
                intent = new Intent(this, CardListActivity.class);
                finish();
                startActivity(intent);
                break;

            case R.id.my_reservations:
                intent = new Intent(this, ReservationListActivity.class);
                //finish();
                startActivity(intent);
                break;

            case R.id.my_orders:
                intent = new Intent(this, OldOrdersListActivity.class);
                finish();
                startActivity(intent);
                break;

            case R.id.my_favorites:
                //intent = new Intent(this, MainScreenActivity.class);
                //finish();
                //startActivity(intent);
                break;
        }
        mCurrentMenuItem = id;
        menuItem.setChecked(true);
        return false;
    }

    private class GetUserProfile extends AsyncTask<Void, Void, Void> {

        private Profile profile;
        private Message msg;


        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (ObiApp.profile == null) {

                    Call<Profile> call = ObiApp.api.getUserProfile();

                    retrofit.Response resp = call.execute();

                    if (resp.isSuccess()) {
                        profile = (Profile) resp.body();
                        ObiApp.profile = profile;
                    } else if (resp.errorBody() != null) {
                        msg = new Gson().fromJson(resp.errorBody().string(), Message.class);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (ObiApp.profile != null) {
                userName.setText(ObiApp.user.data.first_name + " " + ObiApp.user.data.last_name);
            }
            if (ObiApp.profileImg != null && !ObiApp.profileImg.equals("")) {
                String img = "http://graph.facebook.com/" + ObiApp.profileImg + "/picture?width=250&height=250";
                PicassoCache.getPicassoInstance(BaseActivity.this).load(img).into(profileImg);
            }
        }
    }

}
