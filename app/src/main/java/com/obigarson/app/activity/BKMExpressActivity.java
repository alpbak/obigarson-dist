package com.obigarson.app.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;

import com.obigarson.app.R;
import com.obigarson.app.util.ObiApp;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BKMExpressActivity extends AppCompatActivity {

    WebView webView;
    ImageButton backButton;
    final Context myApp = this;
    String lastLoadedURL;
    Handler handlerForJavascriptInterface = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bkmexpress);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }


        webView = (WebView)findViewById(R.id.bkmWebView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");



        backButton = (ImageButton)findViewById(R.id.BKMBackButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        String htmlString = null;

        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {
            htmlString = extras.getString("HTML");

            /*
            System.out.println("************************************************");
            System.out.println("************************************************");
            System.out.println("************************************************");
            System.out.println(htmlString);
            System.out.println("************************************************");
            System.out.println("************************************************");
            System.out.println("************************************************");
            */
        }


        webView.loadData(htmlString, "text/html; charset=UTF-8", null);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // For all other links, let the WebView do it's normal thing
                return false;
            }

            public void onPageFinished(WebView view, String url) {
                System.out.println("BKM - url: " + url);

                //if (url.equalsIgnoreCase("https://api.obigarson.com/api/venue/orders/bkmexpress_payment/callback/")) {
                if (url.contains("api.obigarson.com/api/venue/orders/bkmexpress_payment/callback") || url.contains("api.obigarson.com/api/venue/apart/bkmexpress_payment/callback")) {
                    System.out.println("BKM - url BİZİM: ");

                    webView.loadUrl("javascript:window.HtmlViewer.showHTML" +
                            "('&lt;html&gt;'+document.getElementsByTagName('html')[0].innerHTML+'&lt;/html&gt;');");
                }

            }

        });
    }

    class MyJavaScriptInterface
    {
        private Context ctx;

        MyJavaScriptInterface(Context ctx)
        {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void showHTML(final String html)
        {
            System.out.println("BKM - url: showHTML");
            //code to use html content here
            handlerForJavascriptInterface.post(new Runnable() {
                @Override
                public void run()
                {
                    //Toast toast = Toast.makeText(this, "Page has been loaded in webview. html content :"+ html, Toast.LENGTH_LONG);
                    //toast.show();

                    System.out.println("SOURCE: " + "Page has been loaded in webview. html content :"+ html);
                    parseResult(html);



                }});
        }
    }

    public void parseResult (String s){

        System.out.println("BKM - parseResult: " + s);


        s = s.substring(s.indexOf("{"));
        s = s.substring(0, s.indexOf("}") + 1);

        System.out.println(s);


        try {

            JSONObject obj = new JSONObject(s);

            Log.d("parseResult", obj.toString());

            Log.d("parseResult status", obj.getString("status"));
            Log.d("parseResult: token", obj.getString("token"));

            if (obj.getString("status").equalsIgnoreCase("SUCCESS")){
                showThankYouScreen();
            }
            else{
                showFailureScreen();
            }

        } catch (Throwable t) {
            Log.e("parseResult", "Could not parse malformed JSON: \"" + s + "\"");
        }

    }

    public void showThankYouScreen(){
        setContentView(R.layout.layout_after_order);
        ImageButton payBackButton;

        payBackButton = (ImageButton) findViewById(R.id.pay_back_button);
        payBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObiApp.venue = null;
                ObiApp.basket = null;
                startActivity(new Intent(BKMExpressActivity.this, DiscoverActivity.class));
                finish();
            }
        });
        //pDialog.dismissWithAnimation();
        String payment_confirmation;

        if (ObiApp.isApart){
            payment_confirmation = getString(R.string.order_forwarded);
        }
        else{
            payment_confirmation = getString(R.string.payment_forwarded);
        }

        new SweetAlertDialog(BKMExpressActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(payment_confirmation)
                .setConfirmText(getString(R.string.okey))
                .show();
    }

    public void showFailureScreen(){
        new SweetAlertDialog(BKMExpressActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.err))
                .setContentText(getString(R.string.bkm_error))
                .setConfirmText(getString(R.string.okey))
                .show();
    }

}
