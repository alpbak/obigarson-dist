package com.obigarson.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.CreditCard;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.CreditCardResp;
import com.obigarson.app.util.ObiApp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;

public class CardAddActivity extends Activity implements View.OnClickListener {
    String msg = "Android : ";


    @Bind(R.id.credit_add_button)
    Button confirmButton;
    @Bind(R.id.credit_back_button)
    ImageButton backButton;

    @Bind(R.id.card_description)
    EditText cardDescription;
    @Bind(R.id.card_owner)
    EditText cardOwner;

    @Bind(R.id.card_number)
    EditText cardNumber;


    @Bind(R.id.card_ccv)
    EditText cardCCV;

    @Bind(R.id.year_spinner)
    Spinner yearSpinner;
    @Bind(R.id.month_spinner)
    Spinner monthSpinner;


    final Calendar c = Calendar.getInstance();

    CreditCard card;
    public List<EditText> editTexts;
    public List<String> months = new ArrayList<>();
    public List<String> years = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_credit);
        ButterKnife.bind(this);
        editTexts = new LinkedList<>();
        editTexts.add(cardOwner);
        editTexts.add(cardDescription);
        editTexts.add(cardNumber);
        editTexts.add(cardCCV);

        backButton.setOnClickListener(this);
        confirmButton.setOnClickListener(this);
        setSpinners();
    }

    public void setSpinners(){

        for (int i = 1; i <= 12; i++){
            months.add(String.valueOf(i));
        }
        for (int i = c.get(Calendar.YEAR)%100; i <= c.get(Calendar.YEAR)%100+10; i++){
            years.add(String.valueOf(i));
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<>(CardAddActivity.this, R.layout.spinner_item, months);
        adapter.setDropDownViewResource(R.layout.dropdown_spinner);
        monthSpinner.setAdapter(adapter);
        monthSpinner.invalidate();

        adapter = new ArrayAdapter<>(CardAddActivity.this, R.layout.spinner_item, years);
        adapter.setDropDownViewResource(R.layout.dropdown_spinner);
        yearSpinner.setAdapter(adapter);
        yearSpinner.invalidate();

    }

    /**
     * Called when the activity is about to become visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onBackPressed() {
        backButton.performClick();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.credit_back_button:
                Intent loginIntent = new Intent(CardAddActivity.this, MainScreenActivity.class);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.credit_add_button:
                final String selectedMonth = (String) monthSpinner.getSelectedItem();
                final String selectedYear = (String) yearSpinner.getSelectedItem();

                if(!checkEditTexts(editTexts)){
                    Toast.makeText(this,getString(R.string.fill_empty_areas),Toast.LENGTH_SHORT).show();
                    return;
                }


                final String cardNum = cardNumber.getText().toString();
                final String CCV = cardCCV.getText().toString();


                if(!isValidCard(cardNum)){
                    Toast.makeText(this,getString(R.string.require_valid_card),Toast.LENGTH_SHORT).show();
                }else if(CCV.length() != 3){
                    Toast.makeText(this,getString(R.string.require_valid_ccv),Toast.LENGTH_SHORT).show();
                }else{
                    card = new CreditCard();
                    card.card_number =cardNum;
                    card.year = selectedYear;
                    card.month = selectedMonth;
                    card.ccv = CCV;
                    card.description = cardDescription.getText().toString();
                    card.holdername = cardOwner.getText().toString();
                    card.status = "1";
                    new AddCard().execute();
                }
                break;



        }
    }

    public boolean checkEditTexts(List<EditText> editTexts){
        for(EditText edit : editTexts){
            if(TextUtils.isEmpty(edit.getText())){
                return  false;
            }
        }
        return true;
    }

    public boolean isValidCard(String card) {

        final char[] raw = card.toCharArray();
        final int[] digits = new int[raw.length];

        for (int i = 0; i < raw.length; i++) {
            digits[i] = raw[i] - '0';
        }

        int sum = 0;
        int length = digits.length;
        for (int i = 0; i < length; i++) {

            // get digits in reverse order
            int digit = digits[length - i - 1];

            // every 2nd number multiply with 2
            if (i % 2 == 1) {
                digit *= 2;
            }
            sum += digit > 9 ? digit - 9 : digit;
        }
        return sum % 10 == 0;
    }


    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }



    private class AddCard extends AsyncTask<String, Void, Void> {

        private CreditCardResp res;
        private  Message msg;
        ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            try {

                Call<CreditCardResp> call = ObiApp.api.addCreditCard(Utils.encode(new Gson().toJson(card)));

                retrofit.Response resp = call.execute();

                if(resp.isSuccess()){
                     res =(CreditCardResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }

            } catch (Exception e) {
                dialog.dismiss();
                Log.e("Credit card", e.getMessage());
            }
            return null;
        }

        @Override
        protected  void onPreExecute(){
            dialog  = new ProgressDialog(CardAddActivity.this);
            dialog.setCancelable(false);
            dialog.setTitle(getString(R.string.adding_card));
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            dialog.dismiss();
            if (res != null) {
                Intent intent = new Intent(CardAddActivity.this, CardListActivity.class);
                startActivity(intent);
                finish();
            } else if (msg != null) {
                 new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.err))
                        .setContentText(msg.message)
                        .setConfirmText(getString(R.string.okey))
                        .show();
             } else {
                new SweetAlertDialog(CardAddActivity.this,SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.operation_failed))
                        .setConfirmText(getString(R.string.okey))
                        .show();
            }
        }


    }
}