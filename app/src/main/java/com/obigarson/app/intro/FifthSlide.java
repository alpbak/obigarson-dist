package com.obigarson.app.intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.obigarson.app.R;

/**
 * Created by abak on 24/03/16.
 */
public class FifthSlide extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.intro_fifth_screen, container, false);
        return v;
    }
}
