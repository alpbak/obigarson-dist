package com.obigarson.app.intro;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.obigarson.app.activity.LoginActivity;
import com.obigarson.app.util.ObiApp;

public class Intro extends AppIntro2 {

    // Please DO NOT override onCreate. Use init
    @Override
    public void init(Bundle savedInstanceState) {

        // Add your slide's fragments here
         addSlide(new FirstSlide(), getApplicationContext());
         addSlide(new SecondSlide(), getApplicationContext());
         addSlide(new ThirdSlide(), getApplicationContext());
         addSlide(new FourthSlide(), getApplicationContext());
         addSlide(new FifthSlide(), getApplicationContext());

        // OPTIONAL METHODS
        // Override bar/separator color
        //setBarColor(Color.parseColor("#01BBD6"));
        //setSeparatorColor(Color.parseColor("#01BBD6"));

        // Hide Skip button
        //showSkipButton(true);


        // Turn vibration on and set intensity
         setVibrate(true);
        setVibrateIntensity(10);
    }

    private void loadMainActivity(){

        Intent intent = new Intent(this, LoginActivity.class);
        ObiApp.obiDB.setIntro();
        finish();
        startActivity(intent);
    }
/*
    @Override
    public void onSkipPressed() {
        loadMainActivity();
    }
*/

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }
}