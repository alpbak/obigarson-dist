package com.obigarson.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.obigarson.app.R;
import com.obigarson.app.activity.DishActivity;
import com.obigarson.app.adapters.MenuAdapter;
import com.obigarson.app.api.model.OrderedDish;
import com.obigarson.app.api.response.VenueDish;
import com.obigarson.app.util.ObiApp;


public class MenuFragment extends Fragment implements  AdapterView.OnItemClickListener {
    MenuAdapter adapter;
    ListView list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new MenuAdapter(getContext());
     }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_menu_list, container, false);
        list = (ListView) v.findViewById(R.id.menu_listview);
            list.setOnItemClickListener(this);
        VenueDish cat = getArguments().getParcelable("cat");
        adapter.clear();
        adapter.addAll(cat.dishes);
        list.setAdapter(adapter);
        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e("Menu", "click");
        /*
        if(ObiApp.isQRActive == false && ObiApp.address == null){
            Log.e("Menu", "FAST???");
        }
        else {
            final OrderedDish dish = (OrderedDish) parent.getItemAtPosition(position);
            Intent loginIntent = new Intent(getContext(), DishActivity.class).putExtra("restaurant", ObiApp.venue).putExtra("dish", dish);
            getActivity().finish();
            startActivity(loginIntent);
        }
        */
        final OrderedDish dish = (OrderedDish) parent.getItemAtPosition(position);
        Intent loginIntent = new Intent(getContext(), DishActivity.class).putExtra("restaurant", ObiApp.venue).putExtra("dish", dish);
        getActivity().finish();
        startActivity(loginIntent);
    }


    public static MenuFragment newInstance(VenueDish cat) {
        MenuFragment f = new MenuFragment();
        Bundle b = new Bundle();
        b.putParcelable("cat",cat);
        f.setArguments(b);
        return f;
    }
}
