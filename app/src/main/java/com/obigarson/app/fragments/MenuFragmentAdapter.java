package com.obigarson.app.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.obigarson.app.api.response.VenueDish;

import java.util.LinkedList;
import java.util.List;

public class MenuFragmentAdapter extends FragmentPagerAdapter {

    List<VenueDish> dataList = new LinkedList<>();
    public FragmentManager fm;

    public void setDataList(List<VenueDish> dataList ){

        if(dataList != null)
        {this.dataList = dataList;}else{
            this.dataList = new LinkedList<>();
        }
    }

    public MenuFragmentAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }


    @Override
    public Fragment getItem(int i) {
        return MenuFragment.newInstance(dataList.get(i));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dataList.get(position).categoryName;
    }
    @Override
    public int getCount() {
        return dataList.size();
    }

}

