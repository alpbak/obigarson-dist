package com.obigarson.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.obigarson.app.R;
import com.obigarson.app.activity.RestaurantActivity;
import com.obigarson.app.adapters.DiscoverAdapter;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.VenueCat;
import com.obigarson.app.util.ObiApp;

import java.util.LinkedList;

public class RestaurantFragment extends Fragment implements  AdapterView.OnItemClickListener {
    DiscoverAdapter adapter;
    ListView list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new DiscoverAdapter(getContext() ,new LinkedList<Venue>());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_discover_list, container, false);
        list = (ListView) v.findViewById(R.id.discover_listview);
        list.setOnItemClickListener(this);
        VenueCat venue = getArguments().getParcelable("venueCat");
        adapter.clear();
        adapter.addAll(venue.venues);
        list.setAdapter(adapter);
        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Venue restaurant = (Venue) parent.getItemAtPosition(position);

        System.out.println("RESTAURANT CLICK");

        ObiApp.venue = restaurant;
        ObiApp.venueID = restaurant.venueCode;

        System.out.println("RESTAURANT CLICK - ObiApp.venueID: " + ObiApp.venueID);

        Intent intent = new Intent(getContext(), RestaurantActivity.class).putExtra("restaurant", restaurant);
        getActivity().finish();
        startActivity(intent);
    }


    public void filter(String value){
        adapter.getFilter().filter(value);
    }


    public static RestaurantFragment newInstance(VenueCat venue) {
        RestaurantFragment f = new RestaurantFragment();
        Bundle b = new Bundle();
        b.putParcelable("venueCat",venue);
        f.setArguments(b);
        return f;
    }
}
