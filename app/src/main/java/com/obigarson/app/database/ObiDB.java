package com.obigarson.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.obigarson.app.api.response.LoginResp;

public class ObiDB extends SQLiteOpenHelper {

    public static final String TAG = "ObiDB";
    public static final int VERSION = 1;

    public ObiDB(Context context) {
        super(context, SQL.TABLE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL.CREATE_TABLE_STATUS);
        db.execSQL(SQL.CREATE_TABLE_LOGIN);
        db.execSQL(SQL.CREATE_TABLE_ACCESS);
        db.execSQL(SQL.CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO : new version update works.
    }

    public synchronized boolean introShown() {
        final SQLiteDatabase db = this.getReadableDatabase();
        boolean retval = false;
        try {
            Cursor cursor = db.query(SQL.TABLE_STATUS, new String[]{"VALUE"},
                    "NAME = ?", new String[]{"INTRO"}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    retval = cursor.getString(0).equals("1");
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Cannot get first screen value:", ex);
        } finally {
            db.close();
        }
        return retval;
    }
    public synchronized boolean setIntro() {
        final SQLiteDatabase db = this.getWritableDatabase();
        boolean retval = false;
        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put("NAME", "INTRO");
            contentValues.put("VALUE", 1);
            db.insertWithOnConflict(SQL.TABLE_STATUS, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            retval = true;
        } catch (Exception e) {
            Log.e(TAG, "set Intro exception", e);
        } finally {
            db.close();
        }
        return retval;
    }

    public synchronized boolean setFacebookLogin(Profile profile) {
        final SQLiteDatabase db = this.getWritableDatabase();
        boolean retval = false;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("NAME",profile.getFirstName());
            contentValues.put("SURNAME", profile.getLastName());
            contentValues.put("FACEBOOK_ID", profile.getId());
            contentValues.put("LINK_URI", profile.getLinkUri().toString());

            db.insertWithOnConflict(SQL.TABLE_LOGIN, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            retval = true;
        } catch (Exception e) {
            Log.e(TAG, "set login exception", e);
        } finally {
            db.close();
        }
        return retval;
    }

    public synchronized boolean setFacebookAccess(AccessToken token) {
        final SQLiteDatabase db = this.getWritableDatabase();
        boolean retval = false;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("USER_ID",token.getUserId());
            contentValues.put("EXPIRES_ON", token.getExpires().toString());
            contentValues.put("TOKEN", token.getToken());
            contentValues.put("APP_ID", token.getApplicationId());

            db.insertWithOnConflict(SQL.TABLE_ACCESS, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            retval = true;
        } catch (Exception e) {
            Log.e(TAG, "set TOKEN exception", e);
        } finally {
            db.close();
        }
        return retval;
    }

    public synchronized boolean getLoggedProfile() {
        final SQLiteDatabase db = this.getReadableDatabase();
        System.err.println("IN LOGGED CHECK");
        boolean retval = false;
        try {
            Cursor cursor = db.query(SQL.TABLE_LOGIN, new String[]{"*"},
                    null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    retval = true;
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Cannot get profile value:", ex);
        } finally {
            db.close();
        }
         return retval;
    }

    public synchronized String getFbId() {
        final SQLiteDatabase db = this.getReadableDatabase();
        System.err.println("IN LOGGED CHECK");
        String retval = "";
        try {
            Cursor cursor = db.query(SQL.TABLE_ACCESS, new String[]{"USER_ID"},
                    null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    retval = cursor.getString(0);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Cannot get profile value:", ex);
        } finally {
            db.close();
        }
        return retval;
    }

    public synchronized boolean setFbId(String facebookId) {
        final SQLiteDatabase db = this.getWritableDatabase();
        boolean retval = false;
        try {
            db.execSQL("DELETE FROM " + SQL.TABLE_ACCESS);
            ContentValues contentValues = new ContentValues();
            contentValues.put("USER_ID", facebookId);
            contentValues.put("EXPIRES_ON", "");
            contentValues.put("TOKEN","");
            contentValues.put("APP_ID", "");

            db.insertWithOnConflict(SQL.TABLE_ACCESS, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            retval = true;
        } catch (Exception e) {
            Log.e(TAG, "set login exception", e);
        } finally {
            db.close();
        }
        return retval;
    }


    public synchronized boolean setLogin(LoginResp user) {
        final SQLiteDatabase db = this.getWritableDatabase();
        boolean retval = false;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("TOKEN", user.token);
            contentValues.put("FIRST_NAME",user.data.first_name);
            contentValues.put("LAST_NAME", user.data.last_name);
            contentValues.put("EMAIL", user.data.email);

            db.insertWithOnConflict(SQL.TABLE_USER, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            retval = true;
        } catch (Exception e) {
            Log.e(TAG, "set login exception", e);
        } finally {
            db.close();
        }
        return retval;
    }

    public synchronized LoginResp getLogin() {
        final SQLiteDatabase db = this.getReadableDatabase();
        System.err.println("IN LOGGED CHECK");
        LoginResp retval = null;
        try {
            Cursor cursor = db.query(SQL.TABLE_USER, null,
                    null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    retval= new LoginResp();
                    retval.token = cursor.getString(0);
                    retval.data.first_name = cursor.getString(1);
                    retval.data.last_name = cursor.getString(2);
                    retval.data.email = cursor.getString(3);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Cannot get user :", ex);
            retval =null;
        } finally {
            db.close();
        }
        return retval;
    }


}
