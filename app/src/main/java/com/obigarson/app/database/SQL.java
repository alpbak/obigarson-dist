package com.obigarson.app.database;


public class SQL {
    public static final String TABLE_NAME = "ObiDB";

    //GLOBALS
    public static final String TABLE_STATUS = "status";
    public static final String CREATE_TABLE_STATUS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_STATUS
            + " (ID INTEGER PRIMARY KEY, "
            + "NAME TEXT, "
            + "VALUE TEXT);";



    public static final String TABLE_LOGIN = "login";
    public static final String CREATE_TABLE_LOGIN = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LOGIN
            + " (FACEBOOK_ID TEXT PRIMARY KEY, "
            + "NAME TEXT, "
            + "SURNAME TEXT, "
            + "LINK_URI TEXT);";



    public static final String TABLE_ACCESS = "access";
    public static final String CREATE_TABLE_ACCESS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ACCESS
            + " (USER_ID TEXT PRIMARY KEY, "
            + "EXPIRES_ON TEXT, "
            + "TOKEN TEXT, "
            + "APP_ID REAL);";

    public static final String TABLE_USER = "user";
    public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS "
            + TABLE_USER
            + " (TOKEN TEXT PRIMARY KEY, "
            + "FIRST_NAME TEXT, "
            + "LAST_NAME TEXT, "
            + "EMAIL TEXT, "
            + "FACEBOOK_ID TEXT);";

}
