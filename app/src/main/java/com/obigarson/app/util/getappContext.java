package com.obigarson.app.util;

import android.app.Application;
import android.content.Context;

import com.onesignal.OneSignal;

/**
 * Created by abak on 15/03/16.
 */
public class getappContext extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        getappContext.context = getApplicationContext();
        OneSignal.startInit(this).init();
    }

    public static Context getAppContext() {
        return getappContext.context;
    }

}
