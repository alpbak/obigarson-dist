package com.obigarson.app.util;

import android.app.Activity;
import android.location.Location;

import com.obigarson.app.api.ObiApi;
import com.obigarson.app.api.Serve;
import com.obigarson.app.api.model.Address;
import com.obigarson.app.api.model.Basket;
import com.obigarson.app.api.model.Profile;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.response.LoginResp;
import com.obigarson.app.api.response.QrResp;
import com.obigarson.app.database.ObiDB;

public class ObiApp {
    public static boolean isShown = false;
    public static ObiDB obiDB = null;

    public static LoginResp user;
    public static ObiApi api;
    public static boolean isQRActive=false;
    public static boolean isApart=false;
    public static boolean isfastFood=false;
    public static String profileImg;
    public static Basket basket;
    public static double tip = 0;
    public static int tipCount = 0;

    public static Profile profile;
    public static Venue venue;
    public static QrResp qrResp;
    public static Address address;
    public static QrResp packageMenu;

    public static long venueID;
    public static long tableID;

    public static Long timer;
    public static Location  location;
    public static int selectedShopCategory;



    public static void initiliaze(Activity _activity) {
        if (obiDB == null) {
            obiDB = new ObiDB(_activity);
        }
        api = Serve.createService(ObiApi.class);
    }
    public static void setToken(String token){
         api = Serve.createService(ObiApi.class, token);
    }
}
