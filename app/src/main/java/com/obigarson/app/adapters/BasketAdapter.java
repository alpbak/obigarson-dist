package com.obigarson.app.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.activity.BasketActivity;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.Order;
import com.obigarson.app.util.ObiApp;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class BasketAdapter extends ArrayAdapter<Order>    {
    LayoutInflater inflater;
    Order order;

    public BasketAdapter(Context context ) {
        super(context, R.layout.list_item_basket, new ArrayList<Order>());
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View row = convertView;
        final MenuHolder holder;
        order = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_basket,parent,false);
            holder = new MenuHolder();
            holder.name = (TextView) row.findViewById(R.id.dish_name);
            holder.specific = (TextView) row.findViewById(R.id.dish_specific);
            holder.price = (TextView) row.findViewById(R.id.dish_price);
            holder.img = (ImageView) row.findViewById(R.id.dish_img);
            holder.plusButton = (ImageButton) row.findViewById(R.id.plus_button);
            holder.minusButton = (ImageButton) row.findViewById(R.id.minus_button);
            holder.orderCount = (TextView) row.findViewById(R.id.dish_count);
            holder.basket_buttons_background = (LinearLayout) row.findViewById(R.id.basket_buttons);

             row.setTag(holder);
        } else {
            holder = (MenuHolder) row.getTag();
        }


        holder.plusButton.setTag(order.id);
        holder.minusButton.setTag(order.id);

        if(order.ordered_dishes.Image != null ){
            Picasso.with(getContext()).load(String.valueOf(order.ordered_dishes.Image)).into(holder.img);
        }else{
            Picasso.with(getContext()).load(R.drawable.default_img).into(holder.img);
        }

        holder.name.setText(order.ordered_dishes.Name);
        holder.price.setText(String.valueOf(order.total_price));
        holder.specific.setText(order.ordered_dishes.Short_Description);
        holder.orderCount.setText(String.valueOf(order.quantity));
        holder.orderId = order.id;
        holder.order_Count = order.quantity;

        if(order.quantity == 1){
            holder.minusButton.setImageResource(R.drawable.x_icon);
        }else{
            holder.minusButton.setImageResource(R.drawable.minus_icon);
        }

        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOrder("plus",(int) v.getTag());
            }
        });
        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               final int orderId = (int) v.getTag();

                if(holder.order_Count == 1){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setPositiveButton(getContext().getString(R.string.okey), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            updateOrder("cancel",orderId);
                        }
                    });
                    builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.setMessage(getContext().getString(R.string.ask_to_del_order));
                    builder.show();
                }else{
                    updateOrder("minus",orderId);
                }

            }
        });

        if (ObiApp.isQRActive && !ObiApp.isApart){
            holder.plusButton.setVisibility(View.INVISIBLE);
            holder.minusButton.setVisibility(View.INVISIBLE);
            holder.basket_buttons_background.setBackgroundColor(Color.WHITE);
        }

        if(order.Status.equals("Sipariş Edildi")){
            row.setBackgroundColor(Color.parseColor("#c8e6c9"));
            holder.plusButton.setOnClickListener(null);
            holder.minusButton.setOnClickListener(null);
            holder.basket_buttons_background.setBackgroundColor(Color.parseColor("#c8e6c9"));
         }else {
            row.setBackgroundColor(Color.WHITE);
        }
        return row;
    }



     class MenuHolder {
         ImageView img;
         TextView name;
         TextView specific;
         TextView price;
         LinearLayout basket_buttons_background;

         public TextView orderCount;
         public ImageButton plusButton;
         public ImageButton minusButton;

         int orderId;
         int order_Count;

    }
    public void updateOrder(final String param, int orderId){
        Log.d("ADAPTER","updateOrder");
        try {
            Call<Message> call=null;

            if(ObiApp.isQRActive){
                if (ObiApp.isApart){ //Apart ise
                    call= ObiApp.api.updateApartOrder(ObiApp.venueID,orderId,param);
                }
                else{//Normal sipariş ise
                    call= ObiApp.api.updateOrder(ObiApp.venueID, ObiApp.tableID,orderId,param);
                }



            } else{
                if (ObiApp.isfastFood){
                    call= ObiApp.api.updateFastFoodOrder(ObiApp.venue.venueCode,orderId,param);
                }
                else{ //Normal order
                    call= ObiApp.api.updatePackageOrder(ObiApp.venue.venueCode, ObiApp.address.id,orderId,param);
                }

            }

            call.enqueue(new Callback<Message>() {

                @Override
                public void onResponse(Response<Message> response) {

                    try {
                        Message msg;
                        if(response.isSuccess()){
                            msg = response.body();
                            BasketActivity.setBasket();

                            if (ObiApp.isQRActive && !ObiApp.isApart) {
                                //sendOrder();
                            }

                        }else{
                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                        }
                        Toast.makeText(getContext(), msg.message, Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void sendOrder() {

        try {
                Call<Message> call = ObiApp.api.sendOrder(ObiApp.venueID,
                        ObiApp.tableID, ObiApp.basket.getId());
                call.enqueue(new Callback<Message>() {

                    @Override
                    public void onResponse(retrofit.Response<Message> response) {

                        try {
                            if ((response.isSuccess())) {
                                Log.d("BasketAdapter", "Order SENT");
                            } else if (response.errorBody() != null) {
                                Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                                Log.d("BasketAdapter", "Order NOT SENT");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }
                    }
                    @Override
                    public void onFailure(Throwable t) {

                    }
                });


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}

