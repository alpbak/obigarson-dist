package com.obigarson.app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.obigarson.app.api.response.VenueCat;
import com.obigarson.app.fragments.RestaurantFragment;

import java.util.LinkedList;
import java.util.List;

public class RestaurantAdapter extends FragmentPagerAdapter {

    List<VenueCat> dataList = new LinkedList<>();
    public FragmentManager fm;

    public void setDataList(List<VenueCat> dataList ){

        if(dataList != null){
            this.dataList = dataList;
        }else{
            this.dataList = new LinkedList<>();
        }
    }

    public RestaurantAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int i) {

        return RestaurantFragment.newInstance(dataList.get(i));
    }

    public void filter(String s) {
        List<Fragment> fragments = fm.getFragments();
        if(fragments == null){
            return;
        }

        for (int i = 0; i < fragments.size(); i++) {
            RestaurantFragment res = (RestaurantFragment)fragments.get(i);
            res.filter(s);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dataList.get(position).name;
    }
    @Override
    public int getCount() {
        return dataList.size();
    }



}

