package com.obigarson.app.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.Address;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.AddressResp;
import com.obigarson.app.util.ObiApp;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class AddressAdapter extends ArrayAdapter<Address> implements View.OnClickListener {
    LayoutInflater inflater;
    Context context;
    Address address;


    public AddressAdapter(Context context) {
        super(context, R.layout.list_item_address, new ArrayList<Address>());
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        AddressHolder holder;
        address = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_address, null);
            holder = new AddressHolder();
            holder.telVal = (TextView) row.findViewById(R.id.address_phone_number);
            holder.titleVal = (TextView) row.findViewById(R.id.address_title);
            holder.addressVal = (TextView) row.findViewById(R.id.address_details);
            holder.city = (TextView) row.findViewById(R.id.address_city);
            holder.delete = (ImageButton) row.findViewById(R.id.address_delete_button);

            row.setTag(holder);
        } else {
            holder = (AddressHolder) row.getTag();
        }


        try {
            holder.addressVal.setText(address.address);
            holder.telVal.setText(address.telephone);
            holder.titleVal.setText(address.address_title);
            holder.city.setText(address.addressCounty.Name);
            holder.delete.setOnClickListener(this);

        } catch (Exception ex) {
            Log.e("Addr List", ex.getMessage());
        }
         return row;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.address_delete_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteAddress();
                    }
                });
                builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.setMessage( getContext().getString(R.string.ask_to_del_address));
                builder.show();

                break;

        }
    }


    public void deleteAddress(){
        try {
            Call<AddressResp> call = ObiApp.api.delUserAddress(Utils.encodeURI(Utils.encode( "{'id' : " + address.id + "}")));

            call.enqueue(new Callback<AddressResp>() {

                @Override
                public void onResponse(Response<AddressResp> response) {
                    if (response.code() == 200) {
                        try{
                            AddressAdapter.this.remove(address);
                            AddressAdapter.this.notifyDataSetChanged();
                            new SweetAlertDialog(getContext(),SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("")
                                    .setContentText(getContext().getString(R.string.address_deleted))
                                    .setConfirmText(getContext().getString(R.string.okey))
                                    .show();
                        }catch (Exception ex){
                        }

                    } else if (response.errorBody() != null) {
                        try {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            new SweetAlertDialog(getContext(),SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("")
                                    .setContentText(msg.message)
                                    .setConfirmText(getContext().getString(R.string.okey))
                                    .show();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    class AddressHolder {
        TextView titleVal;
        TextView telVal;
        TextView addressVal;
        TextView city;
        ImageButton delete;

    }
}

