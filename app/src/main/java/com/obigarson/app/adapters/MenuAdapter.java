package com.obigarson.app.adapters;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.obigarson.app.R;
import com.obigarson.app.activity.DishActivity;
import com.obigarson.app.api.model.OrderedDish;
import com.obigarson.app.fragments.MenuFragment;
import com.obigarson.app.util.ObiApp;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MenuAdapter extends ArrayAdapter<OrderedDish>  implements Filterable{
    LayoutInflater inflater;
    Context context;
    Context mContext = getContext();

    public MenuAdapter(Context context) {
        super(context, R.layout.list_item_menu, new ArrayList<OrderedDish>());
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MenuHolder holder;
        final OrderedDish dish = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_menu,parent,false);
            holder = new MenuHolder();
            holder.name = (TextView) row.findViewById(R.id.dish_name);
            holder.specific = (TextView) row.findViewById(R.id.dish_specific);
            holder.price = (TextView) row.findViewById(R.id.dish_price);
            holder.img = (ImageView) row.findViewById(R.id.dish_img);
            holder.plusButton = (ImageButton) row.findViewById(R.id.plus_button);
            row.setTag(holder);
        } else {
            holder = (MenuHolder) row.getTag();
        }

        if(dish.Image == null || dish.Image.isEmpty() ){
            Picasso.with(getContext()).load(R.drawable.default_img).into(holder.img);
        }else{
            Picasso.with(getContext()).load(String.valueOf(dish.Image)).into(holder.img);
        }
        holder.name.setText(dish.Name);
        holder.price.setText(dish.Price);
        holder.specific.setText(dish.Description);

        if(ObiApp.isQRActive == false && ObiApp.address == null){
            holder.plusButton.setVisibility(View.GONE);
            row.setOnClickListener(null);
        }
        if (ObiApp.isfastFood){

            holder.plusButton.setVisibility(View.VISIBLE);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Menu", "FAST FOOD CLICK");
                    Intent loginIntent = new Intent(getContext(), DishActivity.class).putExtra("restaurant", ObiApp.venue).putExtra("dish", dish);
                    mContext.startActivity(loginIntent);
                }
            });

        }
        return row;
    }



    class MenuHolder {
        ImageView img;
        TextView name;
        TextView specific;
        TextView price;
         ImageButton plusButton;

    }
}

