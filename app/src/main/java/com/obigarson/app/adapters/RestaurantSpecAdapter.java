package com.obigarson.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.obigarson.app.R;
import com.obigarson.app.api.model.Props;

import java.util.List;

public class RestaurantSpecAdapter extends RecyclerView.Adapter<RestaurantSpecAdapter.CustomViewHolder> {
    private List<Props> feedItemList;
    private Context mContext;

    public RestaurantSpecAdapter(Context context, List<Props> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_rest_spec, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        viewHolder.text.setMinHeight(viewGroup.getHeight()/3);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Props feedItem = feedItemList.get(i);
         customViewHolder.text.setText(feedItem.getName());
    }

    public void setItems(List<Props> data){
        this.feedItemList = data;
    }
    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    public class  CustomViewHolder extends RecyclerView.ViewHolder {
       public TextView text;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.text = (TextView) itemView.findViewById(R.id.restaurant_spec1);
        }
    }
}