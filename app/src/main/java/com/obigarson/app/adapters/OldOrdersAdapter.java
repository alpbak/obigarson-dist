package com.obigarson.app.adapters;

/**
 * Created by abak on 19/03/16.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.obigarson.app.R;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.model.OldOrders;
import com.obigarson.app.api.model.Payment;
import com.obigarson.app.api.model.Reservation;
import com.obigarson.app.api.response.OldOrdersResp;
import com.obigarson.app.api.response.ReservationResp;
import com.obigarson.app.util.ObiApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Response;


public class OldOrdersAdapter extends ArrayAdapter<OldOrders>  implements View.OnClickListener{
    LayoutInflater inflater;
    Context context;
    OldOrders reservation;
    SimpleDateFormat sdf;
    SimpleDateFormat rdf;


    public OldOrdersAdapter(Context context) {
        super(context, R.layout.list_item_reservation, new ArrayList<OldOrders>());
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        rdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RestaurantHolder holder;
        reservation = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_old_reservation, null);
            holder = new RestaurantHolder();
            holder.name = (TextView) row.findViewById(R.id.reservation_place_name);
            holder.date = (TextView) row.findViewById(R.id.reservation_date);
            holder.amount = (TextView) row.findViewById(R.id.amount_textView);

            row.setTag(holder);
        } else {
            holder = (RestaurantHolder) row.getTag();
        }

        //row.setBackgroundColor(reservation.Status.equals("İletildi") ? Color.parseColor("#eeeeee") : Color.parseColor("#c8e6c9"));

        try {
            holder.name.setText(reservation.venue.venueName);
            holder.date.setText(rdf.format(sdf.parse(reservation.orderDate)));
            Payment pData = reservation.payment.get(0);
            holder.amount.setText(pData.payment_total + " TL");
        } catch (ParseException ex) {
            Log.e("Reserve List",ex.getMessage());
        }


        return row;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reservation_cancel_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new CancelReserve().execute(reservation.id);
                    }
                });
                builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.setMessage(getContext().getString(R.string.ask_to_del_reserve));
                builder.show();

                break;
            case R.id.reservation_call_button:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+reservation.venue.phone_number));
                try {
                    context.startActivity(callIntent);
                }
                catch (Exception ee){
                    Log.e("TELEPHONY", "ERROR");
                }

                break;
        }
    }


    class RestaurantHolder {
        TextView name;
        TextView date;
        TextView amount;
    }


    private class CancelReserve extends AsyncTask<Integer, Void, Void> {

        private ReservationResp res;
        private Message msg;


        @Override
        protected Void doInBackground(Integer... params) {
            try {

                Call<ReservationResp> call = ObiApp.api.cancelUserReserve(params[0]);

                Response resp = call.execute();

                if(resp.isSuccess()){
                    res =(ReservationResp) resp.body();
                }else if(resp.errorBody()!=null){
                    msg = new Gson().fromJson(resp.errorBody().string(),Message.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (res != null) {
                OldOrdersAdapter.this.remove(reservation);
                OldOrdersAdapter.this.notifyDataSetChanged();
                new SweetAlertDialog(getContext(),SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("")
                        .setContentText(getContext().getString(R.string.reservation_cancelled))
                        .setConfirmText(getContext().getString(R.string.okey))
                        .show();
            }else if(msg != null){
                new SweetAlertDialog(getContext(),SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("")
                        .setContentText(msg.message)
                        .setConfirmText(getContext().getString(R.string.okey))
                        .show();
            }else{
                new SweetAlertDialog(getContext(),SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("")
                        .setContentText(getContext().getString(R.string.operation_failed))
                        .setConfirmText(getContext().getString(R.string.okey))
                        .show();
            }
        }

    }
}
