package com.obigarson.app.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.obigarson.app.R;
import com.obigarson.app.activity.BasketActivity;
import com.obigarson.app.activity.CardListActivity;
import com.obigarson.app.activity.DiscoverActivity;
import com.obigarson.app.activity.FastFoodOrderOKActivity;
import com.obigarson.app.activity.MainScreenActivity;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.model.CreditServer;
import com.obigarson.app.api.model.Message;
import com.obigarson.app.api.response.CreditCardResp;
import com.obigarson.app.util.ObiApp;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class CardsAdapter extends ArrayAdapter<CreditServer> implements View.OnClickListener {
    LayoutInflater inflater;
    Context context;
    CreditServer card;
    boolean frombasket;
    SweetAlertDialog pDialog;

    public CardsAdapter(Context context) {
        super(context, R.layout.list_item_cards, new ArrayList<CreditServer>());
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

        frombasket = CardListActivity.fromBasket();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardHolder holder;
        card = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_cards, null);
            holder = new CardHolder();
            //holder.cardTitle = (TextView) row.findViewById(R.id.card_title);
            //holder.cardOwner = (TextView) row.findViewById(R.id.card_owner);
            holder.cardNumber = (TextView) row.findViewById(R.id.card_number);
            //holder.cardLastDate = (TextView) row.findViewById(R.id.card_last_date);
            holder.delete = (ImageButton) row.findViewById(R.id.card_delete_button);
            holder.cardList = (RelativeLayout) row.findViewById(R.id.layout_address_list);
            row.setTag(holder);
        } else {
            holder = (CardHolder) row.getTag();
        }


        try {
            holder.cardNumber.setText(card.cardNumber);
            holder.delete.setOnClickListener(this);
            if (frombasket) {
                holder.delete.setVisibility(View.INVISIBLE);
                holder.cardList.setOnClickListener(this);
            }
        } catch (Exception ex) {
            Log.e("Addr List", ex.getMessage());
        }
         return row;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.card_delete_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setPositiveButton(getContext().getString(R.string.okey), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteCard();
                    }
                });
                builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.setMessage(getContext().getString(R.string.ask_to_del_card));
                builder.show();

                break;
            case R.id.layout_address_list:
                System.out.println("CLICK");
                callPayment(5);
                break;

        }
    }

    public void callPayment(int paymentId) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(context.getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();


        try {
            Call<Message> call;
            if (ObiApp.isQRActive) {

                if (ObiApp.isApart){ //Apart ödemesi ise
                    call = ObiApp.api.orderApartPaymentOnlineCard(ObiApp.venueID,
                            ObiApp.tableID, 5, card.cardToken);
                }
                else {
                    call = ObiApp.api.orderPaymentOnlineCard(ObiApp.venueID,
                            ObiApp.tableID, 5, card.cardToken);
                }
            } else {
                if (ObiApp.isfastFood){
                    call = ObiApp.api.fastFoodPaymentOnlineCard(ObiApp.venue.venueCode,
                            5, card.cardToken);
                }
                else{
                    call = ObiApp.api.orderPackagePaymentOnlineCard(ObiApp.venue.venueCode,
                            ObiApp.address.id, 5, card.cardToken);
                }


            }
            call.enqueue(new Callback<Message>() {
                @Override
                public void onResponse(retrofit.Response<Message> response) {
                    Log.d("callPaymnt rspse.raw", response.raw().toString());
                    pDialog.dismissWithAnimation();
                    try {
                        Message msg;

                        if ((response.isSuccess())) {
                            msg = response.body();
                            String payment_confirmation;

                            if (ObiApp.isApart){
                                payment_confirmation = context.getString(R.string.order_forwarded);
                            }
                            else{
                                payment_confirmation = context.getString(R.string.payment_forwarded);
                            }

                            if (ObiApp.isfastFood){
                                ObiApp.isfastFood = false;
                                Intent intent;
                                intent = new Intent(context, FastFoodOrderOKActivity.class);
                                context.startActivity(intent);

                            }
                            else {
                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(payment_confirmation)
                                        .setConfirmText(context.getString(R.string.okey))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent intent;
                                                intent = new Intent(context, DiscoverActivity.class);
                                                context.startActivity(intent);
                                            }
                                        })
                                        .show();
                            }


                        } else if (response.errorBody() != null) {
                            System.out.println(response.body());

                            msg = new Gson().fromJson(response.errorBody().string(), Message.class);

                            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(context.getString(R.string.err))
                                    .setContentText(msg.message)
                                    .setConfirmText(context.getString(R.string.okey))

                                    .show();


                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    pDialog.dismissWithAnimation();

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            pDialog.dismissWithAnimation();
        }

    }

    public void deleteCard(){
        try {
            JsonObject obj = new JsonObject();
            obj.addProperty("card_number",card.cardNumber);
            obj.addProperty("card_id",card.id);

            Call<CreditCardResp> call = ObiApp.api.delCreditCard(Utils.encodeURI(Utils.encode(obj.toString())));

            call.enqueue(new Callback<CreditCardResp>() {

                @Override
                public void onResponse(Response<CreditCardResp> response) {
                    if (response.code() == 200) {
                        try{
                            CardsAdapter.this.remove(card);
                            CardsAdapter.this.notifyDataSetChanged();
                            new SweetAlertDialog(getContext(),SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("")
                                    .setContentText(getContext().getString(R.string.card_deleted))
                                    .setConfirmText(getContext().getString(R.string.okey))
                                    .show();
                        }catch (Exception ex){
                        }

                    } else if (response.errorBody() != null) {
                        try {
                            Message msg = new Gson().fromJson(response.errorBody().string(), Message.class);
                            new SweetAlertDialog(getContext(),SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("")
                                    .setContentText(msg.message)
                                    .setConfirmText(getContext().getString(R.string.okey))
                                    .show();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    class CardHolder {
        //TextView cardTitle;
        //TextView cardOwner;
        TextView cardNumber;
        //TextView cardLastDate;
        ImageButton delete;
        RelativeLayout cardList;

    }
}

