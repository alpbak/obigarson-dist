package com.obigarson.app.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.obigarson.app.R;
import com.obigarson.app.activity.DiscoverActivity;
import com.obigarson.app.api.model.Venue;
import com.obigarson.app.api.Utils;
import com.obigarson.app.api.response.VenueGroup;
import com.obigarson.app.util.PicassoCache;

import java.util.ArrayList;
import java.util.List;

public class DiscoverAdapter extends ArrayAdapter<Venue> implements Filterable {
    LayoutInflater inflater;
    private List<Venue> originalData = null;
    private List<Venue> filteredData = null;
    private ItemFilter mFilter = new ItemFilter();
    RestaurantAdapter fragmentAdapter;
    private Context mContext;

    public DiscoverAdapter(Context context, List<Venue> data) {
        super(context, R.layout.list_item_discover, data);
        this.mContext=context;
        this.filteredData = data;
        this.originalData = data;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Venue getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RestaurantHolder holder;
        final Venue restaurant = getItem(position);

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_discover, null);
            holder = new RestaurantHolder();
            holder.name = (TextView) row.findViewById(R.id.discover_rest_name);
            holder.type = (TextView) row.findViewById(R.id.discover_rest_type);
            holder.distance = (TextView) row.findViewById(R.id.discover_rest_distance);
            holder.img = (ImageView) row.findViewById(R.id.discover_list_img);

            row.setTag(holder);
        } else {
            holder = (RestaurantHolder) row.getTag();
        }
        if(!restaurant.venueImage.isEmpty()){
            PicassoCache.getPicassoInstance(getContext()).
                    load(String.valueOf(restaurant.venueImage)).placeholder(R.drawable.progress_xml).into(holder.img);
        }

        holder.name.setText(restaurant.venueName);
        holder.type.setText(restaurant.venueCategory.getName());
        holder.distance.setText(String.valueOf(restaurant.distance)+" km");


        return row;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            System.out.println("****FILTER*****");

            int filterType = Integer.parseInt(String.valueOf(constraint));
            FilterResults results = new FilterResults();
            final List<Venue> list = originalData;
            int count = list.size();
            final List<Venue> nlist = new ArrayList<>(count);

            Venue filterObject = null;
            VenueGroup groups = new VenueGroup();

            System.out.println("****FILTER***** / filterType: " + filterType);

            switch (filterType) {

                case 1:
                    for (int i = 0; i < count; i++) {
                        filterObject = list.get(i);

                        if (check(filterObject.fastfoodStatus)) {
                            nlist.add(filterObject);
                        }
                    }
                    groups.setCats(nlist);
                    updateM(groups);

                    break;
                case 2:
                    for (int i = 0; i < count; i++) {
                        filterObject = list.get(i);
                        //if (check(filterObject.reservation_status)) {
                        if (check(filterObject.fastfoodStatus)) {
                            nlist.add(filterObject);
                        }
                    }
                    groups.setCats(nlist);
                    updateM(groups);

                    break;
                case 3:
                    for (int i = 0; i < count; i++) {
                        filterObject = list.get(i);
                        //if (check(filterObject.takeaway_status)) {
                        if (check(filterObject.fastfoodStatus)) {
                            nlist.add(filterObject);
                        }
                    }
                    groups.setCats(nlist);
                    updateM(groups);

                    break;
                case 4:
                    for (int i = 0; i < count; i++) {
                        filterObject = list.get(i);
                        String venue = filterObject.venueName;
                        String sea = Utils.getSearchString();
                        if (venue.toLowerCase().startsWith(sea.toLowerCase())) {
                            nlist.add(filterObject);
                        }

                    }
                    break;

                default:
                    nlist.addAll(list);
                    break;
            }


            groups.setCats(nlist);

            results.values = nlist;
            results.count = nlist.size();



            return results;
        }

        public void updateM(final VenueGroup groups){

            ((DiscoverActivity)mContext).runOnUiThread(new Runnable() {
                @Override public void run() {
                    ((DiscoverActivity)mContext).updateMenu(groups);
                }
            });
        }


         @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<Venue>) results.values;
            notifyDataSetChanged();
        }

    }

    public boolean check(Boolean var){
        return var != null && var.booleanValue();
    }


    class RestaurantHolder {
        ImageView img;
        TextView name;
        TextView type;
        TextView distance;
    }
}

