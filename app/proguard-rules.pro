    -optimizationpasses 5
    -dontusemixedcaseclassnames
    -dontskipnonpubliclibraryclasses
    -dontpreverify
    -verbose
    -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

    -keep public class * extends android.app.Activity
    -keep public class * extends android.app.Application
   -keep public class * extends android.app.Service
  -keep public class * extends android.content.BroadcastReceiver
   -keep public class * extends android.content.ContentProvider
   -keep public class * extends android.app.backup.BackupAgentHelper
   -keep public class * extends android.preference.Preference
   -keep public class com.android.vending.licensing.ILicensingService

   #keep all classes that might be used in XML layouts
   -keep public class * extends android.view.View
   -keep public class * extends android.app.Fragment
   -keep public class * extends android.support.v4.Fragment


   #keep all public and protected methods that could be used by java reflection
   -keepclassmembernames class * {
       public protected <methods>;
   }

   -keepclasseswithmembernames class * {
       native <methods>;
   }

   -keepclassmembers class * extends android.app.Activity {
      public void *(android.view.View);
   }

   -keepclasseswithmembernames class * {
       public <init>(android.content.Context, android.util.AttributeSet);
   }

   -keepclasseswithmembernames class * {
       public <init>(android.content.Context, android.util.AttributeSet, int);
   }


   -keepclassmembers enum * {
       public static **[] values();
       public static ** valueOf(java.lang.String);
   }

   -keep class * implements android.os.Parcelable {
     public static final android.os.Parcelable$Creator *;
   }


# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }

# For debugging
#-renamesourcefileattribute SourceFile
#-keepattributes SourceFile,LineNumberTable

# For jsoup
#-keep public class org.jsoup.** {
#    public *;
#}

# ButterKnife - Start
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# ButterKnife - End
-keep class org.sqlite.** { *; }
-keep class org.sqlite.database.** { *; }


## Square Picasso specific rules ##
## https://square.github.io/picasso/ ##

-dontwarn com.squareup.okhttp.**

# Retrofit 1.X

-keep class com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn retrofit.**
-dontwarn rx.**

-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

# If in your rest service interface you use methods with Callback argument.
-keepattributes Exceptions

# If your rest service methods throw custom exceptions, because you've defined an ErrorHandler.
-keepattributes Signature

# Also you must note that if you are using GSON for conversion from JSON to POJO representation, you must ignore those POJO classes from being obfuscated.
# Here include the POJO's that have you have created for mapping JSON response to POJO for example.


-keep public class android.support.v7.** { *; }

-keep public class android.support.v4.** { *; }

## GSON 2.2.4 specific rules ##

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

-keepattributes EnclosingMethod

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

-keep class com.facebook.** { *; }
-keepattributes Signature

-keep class com.android.** { *; }

-keep class de.hdodenhof.** { *; }

-keep class com.github.** { *; }
-keep class me.dm7.** { *; }
-keep class cn.pedant.** { *; }
-keep class com.android.support.** { *; }
-dontwarn android.support.v4.**

-keep class com.obigarson.app.api.** { *; }