// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DishActivity$$ViewBinder<T extends com.obigarson.app.activity.DishActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624205, "field 'minusButton'");
    target.minusButton = finder.castView(view, 2131624205, "field 'minusButton'");
    view = finder.findRequiredView(source, 2131624206, "field 'plusButton'");
    target.plusButton = finder.castView(view, 2131624206, "field 'plusButton'");
    view = finder.findRequiredView(source, 2131624193, "field 'backButton'");
    target.backButton = finder.castView(view, 2131624193, "field 'backButton'");
    view = finder.findRequiredView(source, 2131624195, "field 'basketButton'");
    target.basketButton = finder.castView(view, 2131624195, "field 'basketButton'");
    view = finder.findRequiredView(source, 2131624197, "field 'confirmButton'");
    target.confirmButton = finder.castView(view, 2131624197, "field 'confirmButton'");
    view = finder.findRequiredView(source, 2131624198, "field 'dishImage'");
    target.dishImage = finder.castView(view, 2131624198, "field 'dishImage'");
    view = finder.findRequiredView(source, 2131624201, "field 'dishName'");
    target.dishName = finder.castView(view, 2131624201, "field 'dishName'");
    view = finder.findRequiredView(source, 2131624209, "field 'dishType'");
    target.dishType = finder.castView(view, 2131624209, "field 'dishType'");
    view = finder.findRequiredView(source, 2131624202, "field 'dishAvailable'");
    target.dishAvailable = finder.castView(view, 2131624202, "field 'dishAvailable'");
    view = finder.findRequiredView(source, 2131624203, "field 'dishPrice'");
    target.dishPrice = finder.castView(view, 2131624203, "field 'dishPrice'");
    view = finder.findRequiredView(source, 2131624207, "field 'dishCount'");
    target.dishCount = finder.castView(view, 2131624207, "field 'dishCount'");
    view = finder.findRequiredView(source, 2131624196, "field 'orderCount'");
    target.orderCount = finder.castView(view, 2131624196, "field 'orderCount'");
    view = finder.findRequiredView(source, 2131624211, "field 'dishNote'");
    target.dishNote = finder.castView(view, 2131624211, "field 'dishNote'");
    view = finder.findRequiredView(source, 2131624210, "field 'dishNoteLabel'");
    target.dishNoteLabel = finder.castView(view, 2131624210, "field 'dishNoteLabel'");
  }

  @Override public void unbind(T target) {
    target.minusButton = null;
    target.plusButton = null;
    target.backButton = null;
    target.basketButton = null;
    target.confirmButton = null;
    target.dishImage = null;
    target.dishName = null;
    target.dishType = null;
    target.dishAvailable = null;
    target.dishPrice = null;
    target.dishCount = null;
    target.orderCount = null;
    target.dishNote = null;
    target.dishNoteLabel = null;
  }
}
