// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PrivacyActivity$$ViewBinder<T extends com.obigarson.app.activity.PrivacyActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624272, "field 'privacyText'");
    target.privacyText = finder.castView(view, 2131624272, "field 'privacyText'");
    view = finder.findRequiredView(source, 2131624212, "field 'forgotBack'");
    target.forgotBack = finder.castView(view, 2131624212, "field 'forgotBack'");
  }

  @Override public void unbind(T target) {
    target.privacyText = null;
    target.forgotBack = null;
  }
}
