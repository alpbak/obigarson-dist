// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CardAddActivity$$ViewBinder<T extends com.obigarson.app.activity.CardAddActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624133, "field 'confirmButton'");
    target.confirmButton = finder.castView(view, 2131624133, "field 'confirmButton'");
    view = finder.findRequiredView(source, 2131624131, "field 'backButton'");
    target.backButton = finder.castView(view, 2131624131, "field 'backButton'");
    view = finder.findRequiredView(source, 2131624135, "field 'cardDescription'");
    target.cardDescription = finder.castView(view, 2131624135, "field 'cardDescription'");
    view = finder.findRequiredView(source, 2131624137, "field 'cardOwner'");
    target.cardOwner = finder.castView(view, 2131624137, "field 'cardOwner'");
    view = finder.findRequiredView(source, 2131624139, "field 'cardNumber'");
    target.cardNumber = finder.castView(view, 2131624139, "field 'cardNumber'");
    view = finder.findRequiredView(source, 2131624145, "field 'cardCCV'");
    target.cardCCV = finder.castView(view, 2131624145, "field 'cardCCV'");
    view = finder.findRequiredView(source, 2131624143, "field 'yearSpinner'");
    target.yearSpinner = finder.castView(view, 2131624143, "field 'yearSpinner'");
    view = finder.findRequiredView(source, 2131624141, "field 'monthSpinner'");
    target.monthSpinner = finder.castView(view, 2131624141, "field 'monthSpinner'");
  }

  @Override public void unbind(T target) {
    target.confirmButton = null;
    target.backButton = null;
    target.cardDescription = null;
    target.cardOwner = null;
    target.cardNumber = null;
    target.cardCCV = null;
    target.yearSpinner = null;
    target.monthSpinner = null;
  }
}
