// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ReservationActivity$$ViewBinder<T extends com.obigarson.app.activity.ReservationActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624287, "field 'minusButton'");
    target.minusButton = finder.castView(view, 2131624287, "field 'minusButton'");
    view = finder.findRequiredView(source, 2131624289, "field 'plusButton'");
    target.plusButton = finder.castView(view, 2131624289, "field 'plusButton'");
    view = finder.findRequiredView(source, 2131624252, "field 'backButton'");
    target.backButton = finder.castView(view, 2131624252, "field 'backButton'");
    view = finder.findRequiredView(source, 2131624292, "field 'dateLater'");
    target.dateLater = finder.castView(view, 2131624292, "field 'dateLater'");
    view = finder.findRequiredView(source, 2131624291, "field 'dateToday'");
    target.dateToday = finder.castView(view, 2131624291, "field 'dateToday'");
    view = finder.findRequiredView(source, 2131624285, "field 'confirm'");
    target.confirm = finder.castView(view, 2131624285, "field 'confirm'");
    view = finder.findRequiredView(source, 2131624288, "field 'countText'");
    target.countText = finder.castView(view, 2131624288, "field 'countText'");
    view = finder.findRequiredView(source, 2131624294, "field 'dateVal'");
    target.dateVal = finder.castView(view, 2131624294, "field 'dateVal'");
    view = finder.findRequiredView(source, 2131624298, "field 'noteVal'");
    target.noteVal = finder.castView(view, 2131624298, "field 'noteVal'");
    view = finder.findRequiredView(source, 2131624293, "field 'date'");
    target.date = finder.castView(view, 2131624293, "field 'date'");
    view = finder.findRequiredView(source, 2131624297, "field 'note'");
    target.note = finder.castView(view, 2131624297, "field 'note'");
    view = finder.findRequiredView(source, 2131624295, "field 'tel'");
    target.tel = finder.castView(view, 2131624295, "field 'tel'");
    view = finder.findRequiredView(source, 2131624296, "field 'telVal'");
    target.telVal = finder.castView(view, 2131624296, "field 'telVal'");
  }

  @Override public void unbind(T target) {
    target.minusButton = null;
    target.plusButton = null;
    target.backButton = null;
    target.dateLater = null;
    target.dateToday = null;
    target.confirm = null;
    target.countText = null;
    target.dateVal = null;
    target.noteVal = null;
    target.date = null;
    target.note = null;
    target.tel = null;
    target.telVal = null;
  }
}
