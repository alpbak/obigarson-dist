// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ForgotPassActivity$$ViewBinder<T extends com.obigarson.app.activity.ForgotPassActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624212, "field 'forgotBack'");
    target.forgotBack = finder.castView(view, 2131624212, "field 'forgotBack'");
    view = finder.findRequiredView(source, 2131624214, "field 'send'");
    target.send = finder.castView(view, 2131624214, "field 'send'");
    view = finder.findRequiredView(source, 2131624213, "field 'phone'");
    target.phone = finder.castView(view, 2131624213, "field 'phone'");
  }

  @Override public void unbind(T target) {
    target.forgotBack = null;
    target.send = null;
    target.phone = null;
  }
}
