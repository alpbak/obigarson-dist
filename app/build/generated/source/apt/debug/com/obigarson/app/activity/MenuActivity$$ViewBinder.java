// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MenuActivity$$ViewBinder<T extends com.obigarson.app.activity.MenuActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624232, "field 'back'");
    target.back = finder.castView(view, 2131624232, "field 'back'");
    view = finder.findRequiredView(source, 2131624235, "field 'basket'");
    target.basket = finder.castView(view, 2131624235, "field 'basket'");
    view = finder.findRequiredView(source, 2131624237, "field 'restName'");
    target.restName = finder.castView(view, 2131624237, "field 'restName'");
    view = finder.findRequiredView(source, 2131624238, "field 'restType'");
    target.restType = finder.castView(view, 2131624238, "field 'restType'");
    view = finder.findRequiredView(source, 2131624236, "field 'restImg'");
    target.restImg = finder.castView(view, 2131624236, "field 'restImg'");
    view = finder.findRequiredView(source, 2131624196, "field 'orderCount'");
    target.orderCount = finder.castView(view, 2131624196, "field 'orderCount'");
  }

  @Override public void unbind(T target) {
    target.back = null;
    target.basket = null;
    target.restName = null;
    target.restType = null;
    target.restImg = null;
    target.orderCount = null;
  }
}
