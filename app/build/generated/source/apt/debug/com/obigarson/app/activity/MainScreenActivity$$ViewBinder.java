// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MainScreenActivity$$ViewBinder<T extends com.obigarson.app.activity.MainScreenActivity> extends com.obigarson.app.activity.BaseActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131624168, "field 'search'");
    target.search = finder.castView(view, 2131624168, "field 'search'");
    view = finder.findRequiredView(source, 2131624216, "field 'discover'");
    target.discover = finder.castView(view, 2131624216, "field 'discover'");
    view = finder.findRequiredView(source, 2131624217, "field 'scan'");
    target.scan = finder.castView(view, 2131624217, "field 'scan'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.search = null;
    target.discover = null;
    target.scan = null;
  }
}
