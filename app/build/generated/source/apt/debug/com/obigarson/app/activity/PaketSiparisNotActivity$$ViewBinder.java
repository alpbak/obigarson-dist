// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PaketSiparisNotActivity$$ViewBinder<T extends com.obigarson.app.activity.PaketSiparisNotActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624255, "field 'back'");
    target.back = finder.castView(view, 2131624255, "field 'back'");
    view = finder.findRequiredView(source, 2131624257, "field 'payButton'");
    target.payButton = finder.castView(view, 2131624257, "field 'payButton'");
    view = finder.findRequiredView(source, 2131624259, "field 'note_text'");
    target.note_text = finder.castView(view, 2131624259, "field 'note_text'");
  }

  @Override public void unbind(T target) {
    target.back = null;
    target.payButton = null;
    target.note_text = null;
  }
}
