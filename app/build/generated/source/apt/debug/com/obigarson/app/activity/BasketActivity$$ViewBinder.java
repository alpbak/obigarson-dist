// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BasketActivity$$ViewBinder<T extends com.obigarson.app.activity.BasketActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624149, "field 'back'");
    target.back = finder.castView(view, 2131624149, "field 'back'");
    view = finder.findRequiredView(source, 2131624154, "field 'payButton'");
    target.payButton = finder.castView(view, 2131624154, "field 'payButton'");
    view = finder.findRequiredView(source, 2131624157, "field 'restType'");
    target.restType = finder.castView(view, 2131624157, "field 'restType'");
  }

  @Override public void unbind(T target) {
    target.back = null;
    target.payButton = null;
    target.restType = null;
  }
}
