// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ScannerActivity$$ViewBinder<T extends com.obigarson.app.activity.ScannerActivity> extends com.obigarson.app.activity.BaseActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131624165, "field 'flashButton'");
    target.flashButton = finder.castView(view, 2131624165, "field 'flashButton'");
    view = finder.findRequiredView(source, 2131624167, "field 'discoverButton'");
    target.discoverButton = finder.castView(view, 2131624167, "field 'discoverButton'");
    view = finder.findRequiredView(source, 2131624168, "field 'search'");
    target.search = finder.castView(view, 2131624168, "field 'search'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.flashButton = null;
    target.discoverButton = null;
    target.search = null;
  }
}
