// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AddressAddActivity$$ViewBinder<T extends com.obigarson.app.activity.AddressAddActivity> extends com.obigarson.app.activity.BaseActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131624116, "field 'backButton'");
    target.backButton = finder.castView(view, 2131624116, "field 'backButton'");
    view = finder.findRequiredView(source, 2131624119, "field 'addButton'");
    target.addButton = finder.castView(view, 2131624119, "field 'addButton'");
    view = finder.findRequiredView(source, 2131624130, "field 'addrVal'");
    target.addrVal = finder.castView(view, 2131624130, "field 'addrVal'");
    view = finder.findRequiredView(source, 2131624124, "field 'telVal'");
    target.telVal = finder.castView(view, 2131624124, "field 'telVal'");
    view = finder.findRequiredView(source, 2131624122, "field 'titleVal'");
    target.titleVal = finder.castView(view, 2131624122, "field 'titleVal'");
    view = finder.findRequiredView(source, 2131624125, "field 'countySpinner'");
    target.countySpinner = finder.castView(view, 2131624125, "field 'countySpinner'");
    view = finder.findRequiredView(source, 2131624126, "field 'townSpinner'");
    target.townSpinner = finder.castView(view, 2131624126, "field 'townSpinner'");
    view = finder.findRequiredView(source, 2131624128, "field 'districtSpinner'");
    target.districtSpinner = finder.castView(view, 2131624128, "field 'districtSpinner'");
    view = finder.findRequiredView(source, 2131624127, "field 'neighbourSpinner'");
    target.neighbourSpinner = finder.castView(view, 2131624127, "field 'neighbourSpinner'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.backButton = null;
    target.addButton = null;
    target.addrVal = null;
    target.telVal = null;
    target.titleVal = null;
    target.countySpinner = null;
    target.townSpinner = null;
    target.districtSpinner = null;
    target.neighbourSpinner = null;
  }
}
