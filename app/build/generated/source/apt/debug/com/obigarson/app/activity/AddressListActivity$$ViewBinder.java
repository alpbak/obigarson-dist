// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AddressListActivity$$ViewBinder<T extends com.obigarson.app.activity.AddressListActivity> extends com.obigarson.app.activity.BaseActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131624116, "field 'backButton'");
    target.backButton = finder.castView(view, 2131624116, "field 'backButton'");
    view = finder.findRequiredView(source, 2131624119, "field 'addButton'");
    target.addButton = finder.castView(view, 2131624119, "field 'addButton'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.backButton = null;
    target.addButton = null;
  }
}
