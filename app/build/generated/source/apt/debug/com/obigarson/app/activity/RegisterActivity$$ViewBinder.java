// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterActivity$$ViewBinder<T extends com.obigarson.app.activity.RegisterActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624280, "field 'email'");
    target.email = finder.castView(view, 2131624280, "field 'email'");
    view = finder.findRequiredView(source, 2131624275, "field 'name'");
    target.name = finder.castView(view, 2131624275, "field 'name'");
    view = finder.findRequiredView(source, 2131624277, "field 'pass'");
    target.pass = finder.castView(view, 2131624277, "field 'pass'");
    view = finder.findRequiredView(source, 2131624279, "field 'repass'");
    target.repass = finder.castView(view, 2131624279, "field 'repass'");
    view = finder.findRequiredView(source, 2131624282, "field 'phone'");
    target.phone = finder.castView(view, 2131624282, "field 'phone'");
    view = finder.findRequiredView(source, 2131624273, "field 'register'");
    target.register = finder.castView(view, 2131624273, "field 'register'");
    view = finder.findRequiredView(source, 2131624171, "field 'loginBack'");
    target.loginBack = finder.castView(view, 2131624171, "field 'loginBack'");
    view = finder.findRequiredView(source, 2131624284, "field 'registerText'");
    target.registerText = finder.castView(view, 2131624284, "field 'registerText'");
  }

  @Override public void unbind(T target) {
    target.email = null;
    target.name = null;
    target.pass = null;
    target.repass = null;
    target.phone = null;
    target.register = null;
    target.loginBack = null;
    target.registerText = null;
  }
}
