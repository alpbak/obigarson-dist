// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DiscoverActivity$$ViewBinder<T extends com.obigarson.app.activity.DiscoverActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624178, "field 'back'");
    target.back = finder.castView(view, 2131624178, "field 'back'");
    view = finder.findRequiredView(source, 2131624180, "field 'basket'");
    target.basket = finder.castView(view, 2131624180, "field 'basket'");
    view = finder.findRequiredView(source, 2131624186, "field 'orderBefore'");
    target.orderBefore = finder.castView(view, 2131624186, "field 'orderBefore'");
    view = finder.findRequiredView(source, 2131624187, "field 'reservation'");
    target.reservation = finder.castView(view, 2131624187, "field 'reservation'");
    view = finder.findRequiredView(source, 2131624188, "field 'takeaway'");
    target.takeaway = finder.castView(view, 2131624188, "field 'takeaway'");
    view = finder.findRequiredView(source, 2131624184, "field 'searchCancel'");
    target.searchCancel = finder.castView(view, 2131624184, "field 'searchCancel'");
    view = finder.findRequiredView(source, 2131624183, "field 'searchBox'");
    target.searchBox = finder.castView(view, 2131624183, "field 'searchBox'");
    view = finder.findRequiredView(source, 2131624191, "field 'fabLocationButton'");
    target.fabLocationButton = finder.castView(view, 2131624191, "field 'fabLocationButton'");
    view = finder.findRequiredView(source, 2131624190, "field 'fabShopButton'");
    target.fabShopButton = finder.castView(view, 2131624190, "field 'fabShopButton'");
    view = finder.findRequiredView(source, 2131624189, "field 'arcMenu'");
    target.arcMenu = finder.castView(view, 2131624189, "field 'arcMenu'");
  }

  @Override public void unbind(T target) {
    target.back = null;
    target.basket = null;
    target.orderBefore = null;
    target.reservation = null;
    target.takeaway = null;
    target.searchCancel = null;
    target.searchBox = null;
    target.fabLocationButton = null;
    target.fabShopButton = null;
    target.arcMenu = null;
  }
}
