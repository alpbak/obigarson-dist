// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RestaurantActivity$$ViewBinder<T extends com.obigarson.app.activity.RestaurantActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624302, "field 'restNameTop'");
    target.restNameTop = finder.castView(view, 2131624302, "field 'restNameTop'");
    view = finder.findRequiredView(source, 2131624305, "field 'restName'");
    target.restName = finder.castView(view, 2131624305, "field 'restName'");
    view = finder.findRequiredView(source, 2131624306, "field 'restType'");
    target.restType = finder.castView(view, 2131624306, "field 'restType'");
    view = finder.findRequiredView(source, 2131624316, "field 'restAddress'");
    target.restAddress = finder.castView(view, 2131624316, "field 'restAddress'");
    view = finder.findRequiredView(source, 2131624313, "field 'restWorkHours'");
    target.restWorkHours = finder.castView(view, 2131624313, "field 'restWorkHours'");
    view = finder.findRequiredView(source, 2131624196, "field 'orderCount'");
    target.orderCount = finder.castView(view, 2131624196, "field 'orderCount'");
    view = finder.findRequiredView(source, 2131624304, "field 'restaurantImg'");
    target.restaurantImg = finder.castView(view, 2131624304, "field 'restaurantImg'");
    view = finder.findRequiredView(source, 2131624310, "field 'orderBeforeImg'");
    target.orderBeforeImg = finder.castView(view, 2131624310, "field 'orderBeforeImg'");
    view = finder.findRequiredView(source, 2131624308, "field 'reserveImg'");
    target.reserveImg = finder.castView(view, 2131624308, "field 'reserveImg'");
    view = finder.findRequiredView(source, 2131624309, "field 'takeAwayImg'");
    target.takeAwayImg = finder.castView(view, 2131624309, "field 'takeAwayImg'");
    view = finder.findRequiredView(source, 2131624317, "field 'reserveButton'");
    target.reserveButton = finder.castView(view, 2131624317, "field 'reserveButton'");
    view = finder.findRequiredView(source, 2131624318, "field 'packageButton'");
    target.packageButton = finder.castView(view, 2131624318, "field 'packageButton'");
    view = finder.findRequiredView(source, 2131624303, "field 'menuButton'");
    target.menuButton = finder.castView(view, 2131624303, "field 'menuButton'");
    view = finder.findRequiredView(source, 2131624300, "field 'back'");
    target.back = finder.castView(view, 2131624300, "field 'back'");
    view = finder.findRequiredView(source, 2131624301, "field 'basket'");
    target.basket = finder.castView(view, 2131624301, "field 'basket'");
  }

  @Override public void unbind(T target) {
    target.restNameTop = null;
    target.restName = null;
    target.restType = null;
    target.restAddress = null;
    target.restWorkHours = null;
    target.orderCount = null;
    target.restaurantImg = null;
    target.orderBeforeImg = null;
    target.reserveImg = null;
    target.takeAwayImg = null;
    target.reserveButton = null;
    target.packageButton = null;
    target.menuButton = null;
    target.back = null;
    target.basket = null;
  }
}
