// Generated code from Butter Knife. Do not modify!
package com.obigarson.app.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LoginActivity$$ViewBinder<T extends com.obigarson.app.activity.LoginActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624224, "field 'phoneVal'");
    target.phoneVal = finder.castView(view, 2131624224, "field 'phoneVal'");
    view = finder.findRequiredView(source, 2131624225, "field 'passVal'");
    target.passVal = finder.castView(view, 2131624225, "field 'passVal'");
    view = finder.findRequiredView(source, 2131624218, "field 'login'");
    target.login = finder.castView(view, 2131624218, "field 'login'");
    view = finder.findRequiredView(source, 2131624219, "field 'loginRegButton'");
    target.loginRegButton = finder.castView(view, 2131624219, "field 'loginRegButton'");
    view = finder.findRequiredView(source, 2131624177, "field 'forgotPassButton'");
    target.forgotPassButton = finder.castView(view, 2131624177, "field 'forgotPassButton'");
    view = finder.findRequiredView(source, 2131624221, "field 'facebookLogin'");
    target.facebookLogin = finder.castView(view, 2131624221, "field 'facebookLogin'");
    view = finder.findRequiredView(source, 2131624120, "field 'contentLayout'");
    target.contentLayout = finder.castView(view, 2131624120, "field 'contentLayout'");
    view = finder.findRequiredView(source, 2131624227, "field 'entryLayout'");
    target.entryLayout = finder.castView(view, 2131624227, "field 'entryLayout'");
    view = finder.findRequiredView(source, 2131624118, "field 'footerLayout'");
    target.footerLayout = finder.castView(view, 2131624118, "field 'footerLayout'");
    view = finder.findRequiredView(source, 2131624230, "field 'uyeGirisButton'");
    target.uyeGirisButton = finder.castView(view, 2131624230, "field 'uyeGirisButton'");
    view = finder.findRequiredView(source, 2131624229, "field 'uyeOlButton'");
    target.uyeOlButton = finder.castView(view, 2131624229, "field 'uyeOlButton'");
  }

  @Override public void unbind(T target) {
    target.phoneVal = null;
    target.passVal = null;
    target.login = null;
    target.loginRegButton = null;
    target.forgotPassButton = null;
    target.facebookLogin = null;
    target.contentLayout = null;
    target.entryLayout = null;
    target.footerLayout = null;
    target.uyeGirisButton = null;
    target.uyeOlButton = null;
  }
}
