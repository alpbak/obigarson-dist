# view res/layout/design_navigation_item.xml #generated:17
-keep class android.support.design.internal.NavigationMenuItemView { <init>(...); }

# view res/layout/design_navigation_menu.xml #generated:17
-keep class android.support.design.internal.NavigationMenuView { <init>(...); }

# view res/layout/design_bottom_sheet_dialog.xml #generated:17
-keep class android.support.design.widget.CoordinatorLayout { <init>(...); }

# view res/layout/activity_drawer.xml #generated:31
# view res/layout/layout_add_address.xml #generated:212
# view res/layout/layout_camera.xml #generated:111
# view res/layout/layout_camera_yeni.xml #generated:149
# view res/layout/layout_home_page.xml #generated:128
# view res/layout/layout_my_addresses.xml #generated:107
# view res/layout/layout_my_cards.xml #generated:107
# view res/layout/layout_my_favorites.xml #generated:133
# view res/layout/layout_old_orders_list.xml #generated:100
# view res/layout/layout_past_orders.xml #generated:95
# view res/layout/layout_reservation_list.xml #generated:100
-keep class android.support.design.widget.NavigationView { <init>(...); }

# view res/layout/design_layout_snackbar.xml #generated:18
# view sw600dp-v13/res/layout-sw600dp-v13/design_layout_snackbar.xml #generated:18
-keep class android.support.design.widget.Snackbar$SnackbarLayout { <init>(...); }

# view res/layout/layout_discover.xml #generated:77
# view res/layout/layout_menu.xml #generated:182
-keep class android.support.design.widget.TabLayout { <init>(...); }

# view res/layout/intro_layout.xml #generated:5
# view res/layout/intro_layout2.xml #generated:6
# view res/layout/layout_discover.xml #generated:114
# view res/layout/layout_menu.xml #generated:194
# view v21/res/layout-v21/intro_layout.xml #generated:5
# view v21/res/layout-v21/intro_layout2.xml #generated:6
-keep class android.support.v4.view.ViewPager { <init>(...); }

# view res/layout/activity_drawer.xml #generated:2
# view res/layout/layout_add_address.xml #generated:2
# view res/layout/layout_camera.xml #generated:2
# view res/layout/layout_camera_yeni.xml #generated:2
# view res/layout/layout_home_page.xml #generated:2
# view res/layout/layout_my_addresses.xml #generated:2
# view res/layout/layout_my_cards.xml #generated:2
# view res/layout/layout_my_favorites.xml #generated:2
# view res/layout/layout_old_orders_list.xml #generated:2
# view res/layout/layout_past_orders.xml #generated:2
# view res/layout/layout_reservation_list.xml #generated:2
-keep class android.support.v4.widget.DrawerLayout { <init>(...); }

# view res/layout/abc_alert_dialog_material.xml #generated:75
-keep class android.support.v4.widget.NestedScrollView { <init>(...); }

# view res/layout/abc_alert_dialog_button_bar_material.xml #generated:40
-keep class android.support.v4.widget.Space { <init>(...); }

# view res/layout/abc_action_menu_item_layout.xml #generated:17
-keep class android.support.v7.view.menu.ActionMenuItemView { <init>(...); }

# view res/layout/abc_expanded_menu_layout.xml #generated:17
-keep class android.support.v7.view.menu.ExpandedMenuView { <init>(...); }

# view res/layout/abc_list_menu_item_layout.xml #generated:17
# view res/layout/abc_popup_menu_item_layout.xml #generated:17
-keep class android.support.v7.view.menu.ListMenuItemView { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:27
-keep class android.support.v7.widget.ActionBarContainer { <init>(...); }

# view res/layout/abc_action_mode_bar.xml #generated:19
# view res/layout/abc_screen_toolbar.xml #generated:43
-keep class android.support.v7.widget.ActionBarContextView { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:17
-keep class android.support.v7.widget.ActionBarOverlayLayout { <init>(...); }

# view res/layout/abc_action_menu_layout.xml #generated:17
-keep class android.support.v7.widget.ActionMenuView { <init>(...); }

# view res/layout/abc_activity_chooser_view.xml #generated:19
-keep class android.support.v7.widget.ActivityChooserView$InnerLayout { <init>(...); }

# view res/layout/abc_alert_dialog_button_bar_material.xml #generated:18
-keep class android.support.v7.widget.ButtonBarLayout { <init>(...); }

# view res/layout/abc_screen_content_include.xml #generated:19
-keep class android.support.v7.widget.ContentFrameLayout { <init>(...); }

# view res/layout/abc_alert_dialog_material.xml #generated:48
-keep class android.support.v7.widget.DialogTitle { <init>(...); }

# view res/layout/abc_screen_simple_overlay_action_mode.xml #generated:23
-keep class android.support.v7.widget.FitWindowsFrameLayout { <init>(...); }

# view res/layout/abc_dialog_title_material.xml #generated:22
# view res/layout/abc_screen_simple.xml #generated:17
-keep class android.support.v7.widget.FitWindowsLinearLayout { <init>(...); }

# view res/layout/fragment_tab.xml #generated:2
# view res/layout/layout_restaurant.xml #generated:270
-keep class android.support.v7.widget.RecyclerView { <init>(...); }

# view res/layout/abc_search_view.xml #generated:78
-keep class android.support.v7.widget.SearchView$SearchAutoComplete { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:36
# view res/layout/activity_drawer.xml #generated:16
# view res/layout/app_bar.xml #generated:3
# view res/layout/layout_add_address.xml #generated:199
# view res/layout/layout_camera.xml #generated:99
# view res/layout/layout_camera_yeni.xml #generated:48
# view res/layout/layout_home_page.xml #generated:115
# view res/layout/layout_my_addresses.xml #generated:94
# view res/layout/layout_my_cards.xml #generated:94
# view res/layout/layout_my_favorites.xml #generated:120
# view res/layout/layout_old_orders_list.xml #generated:87
# view res/layout/layout_past_orders.xml #generated:82
# view res/layout/layout_reservation_list.xml #generated:87
-keep class android.support.v7.widget.Toolbar { <init>(...); }

# view res/layout/abc_screen_simple.xml #generated:25
# view res/layout/abc_screen_simple_overlay_action_mode.xml #generated:32
-keep class android.support.v7.widget.ViewStubCompat { <init>(...); }

# view res/layout/alert_dialog.xml #generated:76
-keep class cn.pedant.SweetAlert.SuccessTickView { <init>(...); }

# view AndroidManifest.xml #generated:69
-keep class com.facebook.FacebookActivity { <init>(...); }

# view AndroidManifest.xml #generated:142
-keep class com.facebook.FacebookContentProvider { <init>(...); }

# view res/layout/layout_login_page.xml #generated:250
# view res/layout/layout_login_page.xml #generated:93
-keep class com.facebook.login.widget.LoginButton { <init>(...); }

# view AndroidManifest.xml #generated:204
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(...); }

# view AndroidManifest.xml #generated:102
-keep class com.obigarson.app.activity.AddressAddActivity { <init>(...); }

# view AndroidManifest.xml #generated:106
-keep class com.obigarson.app.activity.AddressListActivity { <init>(...); }

# view AndroidManifest.xml #generated:151
-keep class com.obigarson.app.activity.BKMExpressActivity { <init>(...); }

# view AndroidManifest.xml #generated:130
-keep class com.obigarson.app.activity.BasketActivity { <init>(...); }

# view AndroidManifest.xml #generated:122
-keep class com.obigarson.app.activity.CardAddActivity { <init>(...); }

# view AndroidManifest.xml #generated:118
-keep class com.obigarson.app.activity.CardListActivity { <init>(...); }

# view AndroidManifest.xml #generated:86
-keep class com.obigarson.app.activity.DiscoverActivity { <init>(...); }

# view AndroidManifest.xml #generated:126
-keep class com.obigarson.app.activity.DishActivity { <init>(...); }

# view AndroidManifest.xml #generated:150
-keep class com.obigarson.app.activity.FastFoodOrderOKActivity { <init>(...); }

# view AndroidManifest.xml #generated:82
-keep class com.obigarson.app.activity.ForgotPassActivity { <init>(...); }

# view AndroidManifest.xml #generated:59
-keep class com.obigarson.app.activity.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:78
-keep class com.obigarson.app.activity.MainScreenActivity { <init>(...); }

# view AndroidManifest.xml #generated:114
-keep class com.obigarson.app.activity.MenuActivity { <init>(...); }

# view AndroidManifest.xml #generated:149
-keep class com.obigarson.app.activity.OldOrdersListActivity { <init>(...); }

# view AndroidManifest.xml #generated:147
-keep class com.obigarson.app.activity.PaketSiparisNotActivity { <init>(...); }

# view AndroidManifest.xml #generated:148
-keep class com.obigarson.app.activity.PrivacyActivity { <init>(...); }

# view AndroidManifest.xml #generated:74
-keep class com.obigarson.app.activity.RegisterActivity { <init>(...); }

# view AndroidManifest.xml #generated:94
-keep class com.obigarson.app.activity.ReservationActivity { <init>(...); }

# view AndroidManifest.xml #generated:90
-keep class com.obigarson.app.activity.ReservationListActivity { <init>(...); }

# view AndroidManifest.xml #generated:98
-keep class com.obigarson.app.activity.RestaurantActivity { <init>(...); }

# view AndroidManifest.xml #generated:110
-keep class com.obigarson.app.activity.ScannerActivity { <init>(...); }

# view AndroidManifest.xml #generated:55
-keep class com.obigarson.app.intro.Intro { <init>(...); }

# view res/layout/layout_forgot_password.xml #generated:58
# view res/layout/layout_login_page.xml #generated:136
# view res/layout/layout_register_page.xml #generated:211
-keep class com.obigarson.app.util.IntlPhoneInput { <init>(...); }

# view AndroidManifest.xml #generated:43
-keep class com.obigarson.app.util.getappContext { <init>(...); }

# view AndroidManifest.xml #generated:192
-keep class com.onesignal.BootUpReceiver { <init>(...); }

# view AndroidManifest.xml #generated:170
-keep class com.onesignal.GcmBroadcastReceiver { <init>(...); }

# view AndroidManifest.xml #generated:181
-keep class com.onesignal.GcmIntentService { <init>(...); }

# view AndroidManifest.xml #generated:179
-keep class com.onesignal.NotificationOpenedReceiver { <init>(...); }

# view AndroidManifest.xml #generated:190
-keep class com.onesignal.NotificationRestoreService { <init>(...); }

# view AndroidManifest.xml #generated:186
-keep class com.onesignal.PermissionsActivity { <init>(...); }

# view AndroidManifest.xml #generated:182
-keep class com.onesignal.SyncService { <init>(...); }

# view AndroidManifest.xml #generated:198
-keep class com.onesignal.UpgradeReceiver { <init>(...); }

# view res/layout/alert_dialog.xml #generated:112
-keep class com.pnikosis.materialishprogress.ProgressWheel { <init>(...); }

# view res/layout/layout_discover.xml #generated:185
-keep class com.sa90.materialarcmenu.ArcMenu { <init>(...); }

# view res/layout/drawer_header.xml #generated:11
-keep class de.hdodenhof.circleimageview.CircleImageView { <init>(...); }

# view AndroidManifest.xml #generated:153
-keep class io.nlopez.smartlocation.activity.providers.ActivityGooglePlayServicesProvider$ActivityRecognitionService { <init>(...); }

# view AndroidManifest.xml #generated:159
-keep class io.nlopez.smartlocation.geocoding.providers.AndroidGeocodingProvider$AndroidGeocodingService { <init>(...); }

# view AndroidManifest.xml #generated:156
-keep class io.nlopez.smartlocation.geofencing.providers.GeofencingGooglePlayServicesProvider$GeofencingService { <init>(...); }

# view res/layout/merge_camera_preview_view_finder.xml #generated:3
-keep class me.dm7.barcodescannerview.CameraPreview { <init>(...); }

# view res/layout/merge_camera_preview_view_finder.xml #generated:7
-keep class me.dm7.barcodescannerview.ViewFinderView { <init>(...); }

